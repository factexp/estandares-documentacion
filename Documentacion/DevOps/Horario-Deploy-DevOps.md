**POLITICAS DE SUBIDAS A PRODUCCIÓN**

A continuación se encuentra detallado el Horario de subidas a producción:

| Tipo  |       Dias                |           Horas          |
|:-----:|:-------------------------:|:------------------------:|
|  Bug  |       Todos               |           Todos          |
| Nuevo | Martes , Jueves , Domingo |           23h00          |

La reunión de preparación de subidas a producción de los cambios Nuevos, será a las 17h00. 
