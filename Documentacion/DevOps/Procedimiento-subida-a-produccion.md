**PROCEDIMIENTO DE SUBIDAS A PRODUCCIÓN**

- Reunión de definición de prioridad de subida de Merge Request en estado 'Done'.
- Creación de tabla con los siguientes datos:

| Prioridad |# MR | RAMA | DESCRIPCION | TIENE BD ? | TIENE CONFIG ? | INT. OTRAS APPS ? | PROGR. RESPONSABLE | TIEMPO DE SUBIDA | HORA DE INICIO APROX. |
|:---------:|:---:|:----:|:-----------:|:----------:|:--------------:|:-----------------:|:------------------:|:----------------:|:---------------------:|

- Revisar posibles conflictos en el versionarmiento y notificar en caso de encontrarlos para su correción inmediata.
- Realizar consolidación de archivos .pks y ejecutarlos en la Base de Datos de Pre Producción, si existiesen problemas, notificar para su corrección inmediata.
- Crear tarea programada con los Merge Request coordinados a subir.
- Enviar el Correo de notificación de la subida a producción planificada a Sistemas.
- Enviar FOTO de la tabla elaborada sobre la Prioridad y horario de las subidas a producción.
- Iniciar la tarea programada de las subidas a producción en el horario establecido, caso contrario notificar algún inconveniente.
- Realizar el merge de la rama en el servidor de producción.
- Realizar las ejecuciones en Base de Datos, si fuese el caso.
- Colocar la evidencia de las diferentes ejecuciones en el Merge Request de la rama Master.
- Realizar rsync si fuese el caso.
- Realizar el push de la rama master y test a gitlab.telconet.ec.
- Notificar en el Grupo de Telegram y WhatsApp el: inicio , avance y culmincación de las subidas a producción.