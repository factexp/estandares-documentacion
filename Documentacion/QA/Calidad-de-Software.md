**INTRODUCCION**

El software es el objeto de análisis y estudio de una de las ramas más jóvenes de ingeniería. De hecho, podemos decir que la concepción misma del software, es decir, su naturaleza de construcción y desarrollo es lo que lo hace particularmente distinto. Entre las características fundamentales del software podemos mencionar que su naturaleza es abstracta debido a que su herramienta principal es el manejo de la información. 

Además el software se desarrolla, no se fabrica como cualquier producto en un sentido clásico; se crea mediante la transformación del poder intelectual y cerebral de los especialistas en el conocimiento. 

Consecuentemente, siendo la información uno de los recursos más valiosos de cualquier institución u organización, es de gran importancia contar con Sistemas que permitan su uso eficiente, en armonía con todas las áreas. 

Para garantizar que esto ocurra dentro de una institución o empresa es indispensable contar con políticas, normas y procedimientos que permitan homogeneizar la elaboración de sistemas de información.


----------


**CALIDAD DEL SOFTWARE**

La calidad del software es el conjunto de cualidades que lo caracterizan y que determinan su utilidad y existencia. La calidad es sinónimo de eficiencia, flexibilidad, corrección, confiabilidad, mantenibilidad, portabilidad, usabilidad, seguridad e integridad.

La calidad del software es medible y varía de un sistema a otro o de un programa a otro. Un producto de software para ser explotado durante un largo período (10 años o más), necesita ser confiable, mantenible y flexible para disminuir los costos de mantenimiento y perfeccionamiento durante el tiempo de explotación.

La calidad del software puede medirse después de elaborado el producto. Pero esto puede resultar muy costoso si se detectan problemas, deriva de imperfecciones en el diseño, por lo que es imprescindible tener en cuenta tanto la obtención de la calidad como su control durante todas las etapas del ciclo de vida del software.

La obtención de un software con calidad implica la utilización de metodologías o procedimientos estándares para el análisis, diseño, programación y prueba del software que permitan uniformar la filosofía de trabajo, en aras de lograr una mayor confiabilidad, mantenibilidad y facilidad de prueba, a la vez que eleven la productividad, tanto para la labor de desarrollo como para el control de la calidad del software.

Los estándares o metodologias definen un conjunto de criterios de desarrollo que guian la forma en que se aplica la ingenieria de software. Sino se sigue ninguna metodologia siempre habra falta de calidad.

Existen algunos requisitos implicitos o expectaticas que a menudo no se mencionan, o se mencionan de forma incompleta que tambien puede implicar una falta de calidad.

La adopcion de una buena politica constribuye en gran medida a lograr la calidad del software, pero no la asegura. Para asegurarse de un nivel de calidad es necesario su control y evaluacion.


----------

**PRUEBAS DEL SOFTWARE**

La definición de pruebas en el contexto del software tiene diferentes connotaciones que en algunos casos llevan a malas interpretaciones. La definición de pruebas de software de Myers es: “Las pruebas son el proceso de ejecución de un programa con la intensión de encontrar errores” ; además, ésta es una parte fundamental del aseguramiento de la calidad del software (SQA), ya que ayuda a asegurar que el producto cumpla con los requisitos ; sin embargo las pruebas de software no son QA, tan solo son una parte de ella.

Las pruebas del software (software testing) son parte del proceso de control de calidad del software (SQA) como ya lo mencionamos, donde especialistas en procesos de software y auditores están preocupados por el proceso de desarrollo de software en lugar de los artefactos, tales como documentación, código y sistemas. Ellos examinan y cambian el proceso de ingeniería del software en sí para reducir la cantidad de errores en el software entregado: denominada tasa de defectos.

Lo que constituye una “Aceptable tasa de defectos” depende de la naturaleza del software; un simulador de vuelo de un juego de video podría tener una tolerancia mucho más alta que un software para un avi&oacuten real.

Las pruebas de software son una tarea destinada a detectar los defectos en el software contrastando los resultados esperados de un programa informático con los resultados reales de un conjunto dado de entradas. Por el contrario, QA (control de calidad) es la implementación de las políticas y procedimientos destinados a prevenir los defectos que se produzcan en primer lugar.

La importancia de las pruebas de software se puede visualizar teniendo como referencia algunos autores:

  - Las pruebas de software permiten pasar de forma confiable del cómodo ambiente planteado por la ingeniería de software, es decir del controlado ambiente de análisis, diseño y construcción, al exigente mundo real en el cual los entornos de producción someten los productos a todo tipo de fatiga .
  - Las pruebas de software basadas en componentes permite la reutilización y por ende la reducción de los ciclos de pruebas, lo cual se ve reflejado en la disminución de costos y tiempos .

  - La necesidad de productos de software de alta calidad ha obligado a identificar y cuantificar factores de calidad como:
  
    * capacidad de uso,  
    * capacidad de prueba,
    * capacidad de mantenimiento,
    * capacidad de ser medible,
    * capacidad de ser confiable


----------


**MODELO EN V**

El modelo en V es producido justamente por esta correspondencia entre las fases de desarrollo y su verificacion con los diferentes tipos de pruebas.


![Con titulo](http://gitlab.telconet.ec/sistemas/imagenes/raw/master/modeloenv.jpg "ModeloenV")

El lado izquierdo de la V representa la descomposición de las necesidades, y la creación de las especificaciones del sistema. El lado derecho de la V representa la integración de las piezas y su verificación. V significa "Verificación y validación".

Es un modelo de ciclo de vida desde el punto de vista de aseguramiento de calidad.

**Objetivos**

  - Minimizacion de los riesgos del proyecto
  - Mejora y garantia de calidad
  - Reduccion de los gastos totales durante todo el proyecto y sistema de ciclo de vida
  - Mejora de la comunicacion entre todos los inversionistas

**Ventajas:**
  - Específica bien los roles de los distintos tipos de pruebas a realizar.
  - Hace explícito parte de la iteración y trabajo que hay que realizar.
  - Este método involucra chequeos de cada una de las etaopas del método Cascada.
  - Es un método más robusto y completo que el método cascada y produce software de mayor calidad que con el modelo cascada.
  - Es un modelo sencillo de y de fácil aprendizaje.
  - Involucra al usuario en las pruebas.

![Con titulo](http://gitlab.telconet.ec/sistemas/imagenes/raw/master/modeloenv2.jpg "ModeloenV2")

En los niveles lógicos del 1 al 4, para cada fase del desarrollo, existe una fase correspondiente o paralela de verificación o validación. Esta estructura obedece al principio de que para cada fase del desarrollo debe existir un resultado verificable.

En la misma estructura se advierte también que la proximidad entre una fase del desarrollo y su fase de verificación correspondiente va decreciendo a medida que aumenta el nivel dentro de la V. La longitud de esta separación intenta ser proporcional a la distancia en el tiempo entre una fase y su homóloga de verificación.

- El **nivel 1** está orientado al “cliente”. El inicio del proyecto y el fin del proyecto constituyen los dos extremos del ciclo. Se compone del análisis de requisitos y especificaciones, se traduce en un documento de requisitos y especificaciones.

- El **nivel 2** se dedica a las características funcionales del sistema propuesto. Puede considerarse el sistema como una caja negra, y caracterizarla únicamente con aquellas funciones que son directa o indirectamente visibles por el usuario final, se traduce en un documento de análisis funcional.

- El **nivel 3** define los componentes hardware y software del sistema final, a cuyo conjunto se denomina arquitectura del sistema.

- El **nivel 4** es la fase de implementación, en la que se desarrollan los elementos unitarios o módulos del programa.	


----------


  [1]: https://lh6.googleusercontent.com/-BWFjxiub5XI/UrN1Ro-UEXI/AAAAAAAAAE8/U3KvBrf3nxA/s0/modeloenv1.jpg "modeloenv1.jpg"
  [2]: https://lh4.googleusercontent.com/-8jXkjv3SYDY/UrN1-uRSSDI/AAAAAAAAAFU/5Ht2eUClqYU/s0/modeloenv2.jpg "modeloenv2.jpg"