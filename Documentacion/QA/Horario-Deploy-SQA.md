A continuación se encuentra detallado el Horario de subidas a producción:

| Tipo  |       Dias                |           Horas          |
|:-----:|:-------------------------:|:------------------------:|
|  Bug  |       Todos               |           Todos          |
| Nuevo | Martes , Jueves , Domingo |           23h00          |

Adicional , en los días establecidos para subidas a producción , se receptarán Merge Request en Done hasta las 17h00.

> **Nota:** Toda subida a producción tipo Nuevo que requiera ser subido fuera de los Horarios establecidos , deben ser autorizados por Jefatura de Sistemas.