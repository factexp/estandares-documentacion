## Control de Calidad

El control de Calidad se basará en los siguientes puntos:

- Documentación de Requerimiento.
- Documentación de Tarea.
- Documentación de Pruebas de Programador.
- Documentación de Aceptación de Usuario.
- Formato de Merge Request.
- Formato de Commit.
- 1 Commit por Cambio.
- Indentación de Variables y Código.
- Máximo de caracteres por línea.
- Redacción y Ortografía.
- Arquitectura.
- Documentación de Clases, Funciones y código.
- Nombres descriptivos de Clases, Funciones y Variables.
- Inicialización de Variables.
- Seguridad.
- Documentación de DML,DDL,DCL.
- SQL.
- Programación.
- Instructivo ó Noticia.
- Pruebas Funcionales.
