## Control de Funcionalidad

El control de Funcionalidad se basará en los siguientes puntos:

- Usabilidad
    - Interacción
    - Orden
    - Textos
    - Consistencia
- Validaciones de datos en Cliente y Servidor
    - Tipo
    - Longitud 
    - Obligatoriedad
- Cuadros de Diálogo
    - Información
    - Error
    - Confirmación
    - Procesamiento de datos
- Seguridad de acceso y ejecución
    - Enlaces
    - Pantallas
    - Acciones
