**Introducción**  
El objetivo del presente documento es el establecimiento de estándares o convenciones de programación que deben seguirse durante el desarrollo de un aplicativo móvil android en __TELCONET__, manteniendo los [Estándares de Programación para Java][1].

**1. Estructura del Proyecto**  
Los proyectos que se implementen en la herramienta de desarrollo 'Android Studio' deben contener la siguiente estructura y ficheros: 

	|-- app 
		|-- manifests 
		|-- java 
			|-- ec.telconet.project 
				|-- models  
				|-- enums 
				|-- exceptions 
				|-- interfaces 
				|-- network
					|-- sync 
				|-- helpers  
				|-- services 
				|-- ui  
					|-- activities 
					|-- fragments
					|-- adapters 
				|-- utils 
		|-- res 
	|-- Gradle Scripts 

- **Manifest**
Contiene un archivo que provee información esencial al sistema previo a que se ejecute la aplicación como: los permisos, versión mínima requerida del sistema operativo, entre otras cosas. 

- **Java**
Contiene el código fuente de la aplicación distribuido en los siguientes directorios.
  
 -  **models:** En este directorio se encuentran los objetos de negocio.  
 -  **enums:** En este directorio se encuentran los tipos de datos especiales.  
 -  **exceptions:**  En este directorio se encuentran las excepciones manejadas  por el programador.  
 -  **interfaces:** En este directorio se encuentran todas las interfaces java.  
 -  **sync:** En este directorio se encuentran las clases que interactúan con algún componente externo al dispositivo, ejemplo: el middleware.  
 -  **helpers:** En este directorio se encuentran archivos que contienen métodos que son usados en más de una clase, el más conocido es el DbHelper pero se pueden implementar otros como ConnectivityHelper, BitmapHelper, DateHelper, LocationHelper, ParcelHelper, RuntimePermissionHelper.  
 -  **services:** En este directorio se encuentran procesos que se ejecutan en segundo plano.  
 -  **ui:** En este directorio se encuentran las clases de tipo Activity, Fragment.  
 -  **utils:** En este directorio se encuentran: la clase Utility, el SharedPreferenceManager, y el AppData.

- **Res**
Contiene información sobre el diseño de las pantallas, imágenes, estilos, palabras e iconos de la aplicación. 

---
**2. Estándares para Nombres de los Recursos**  
Para nombrar los recursos se debe usar el formato **lowercase_uderscore**, adicionalmente se deben usar abreviaturas en lugar del nombre completo en los widgets. 

|      WIDGET      | ABREVIATURA |
|:----------------:|:-----------:|
| Icon             | ic          |
| Action Bar       | ab          |
| Tabs             | tb          |
| Button           | btn         |
| EditText         | et          |
| TextView         | tv          |
| CheckBox         | chk         |
| RadioButton      | rb          |
| ToggleButton     | tb          |
| Spinner          | spn         |
| Menu             | menu        |
| ListView         | lv          |
| GalleryView      | gv          |
| Linear Layout    | ll          |
| Relative Layout  | rl          |

Todos los nombres de los recursos deben llevar la siguiente convención: 

	[WHAT]_[WHERE]_[DESCRIPTION]_[SIZE]

**[WHAT]:** Objeto al que representa  
**[WHERE]:** Parte de la lógica a la que pertenece, por lo general a alguna vista en particular  
**[DESCRIPTION]:** Permite diferenciar entre los varios elementos de una pantalla  
**[SIZE]:** Tamaño al cual se aplica el recurso, all si se aplica sin importar el tamaño 

>Nota:  Si en algún caso una de los elementos está constituido por más de una palabra esta se la escribirá unida y en minúscula. Ejemplo: ic_notification_gpsaccuracy

- **Archivos de Layout**  
Un recurso de tipo layout define la arquitectura de la interfaz de usuario en una actividad o en un componente.

		Convención: [WHAT]_[WHERE]_[DESCRIPTION] 

	Ejemplo:

		activity_login_layout.xml
		fragmet_main_menu.xml

- **String ID's**  
Un recurso de tipo string provee del texto para la aplicación.

		Convención: [WHERE]_[DESCRIPTION] 

	Ejemplo:

		all_continue
		login_username

- **Archivos Drawable**  
Un recurso de tipo drawable es un concepto para acceder a una imagen o gráfico y esta pueda ser dibujada en la pantalla.

		Convención: [WHAT]_[WHERE]_[DESCRIPTION]_[SIZE] 

	Ejemplo:

		img_main_logo_all
		ic_main_person_small
		ic_main_history_mdpi

- **Tag ID's**  
Palabra única con la que se identifica los elementos dentro de un layout.

		Convención: [WHAT]_[WHERE]_[DESCRIPTION] 

	Ejemplo:

		btn_login_signup

- **Dimensions ID's**  
Recurso que contiene el valor de una dimensión.

		Convención: [WHAT]_[WHERE]_[DESCRIPTION]_[SIZE] 

	Ejemplo:

		btn_newtask_textsize_24sp

---
**3. Logs**  
En caso de que se necesite registrar un Log este seguirá el siguiente formato: 

	Fecha y hora | LatLng | Aplicación | Clase | Método | Acción | Estado | Mensaje | Descripción | Usuario | Imei | Modelo | Version Apk | Version Android | Tipo de Conexión | Señal | Entrada 

**LatLng:** Coordenadas Geográficas donde se registra el log  
**Aplicación:** Nombre de la aplicación  
**Clase:** Nombre canónico de la clase java  
**Método:** Nombre del método  
**Acción:** Descripción del evento, ejemplo: Envío de Imagen  
**Estado:** Exitoso / Fallido  
**Mensaje:** Mensaje que se le muestra al usuario posterior a la acción del evento, si el log es para registrar una excepción lo que se hace es mostrar la causa de la excepción  
**Descripción:** En caso de ser fallida se debe indicar una descripción de la causa, sin embargo, de ser una excepción se entenderá como descripción al stacktrace.  
**Usuario:** Login username  
**Imei:** Imei del dispositivo  
**Modelo:** Marca y Modelo del dispositivo  
**Version Apk:** Número de compilación  
**Versión Android:** Versión del sistema operativo del dispositivo  
**Tipo de Conexión:** Wifi / EDGE / 3g / LTE  
**Señal:** Intensidad de la Señal  
**Entrada:** Parámetros de entrada del método o parámetros que permitan replicar el escenario.  

Ejemplo:
```java
	Log.Build(currentLocation, TAG, "methodName", "Action", "Success","loginUsername", SystemHelper.deviceImei(context), SystemHelper.deviceModel(), SystemHelper.deviceBrand(), Utility.getAppVersionCode(), SystemHelper.currentAndroidVersion(), requestPayload).info();
```
Salida:

	2017-10-20 12:34:56 | -2.154178,-79.951283 | Móvil Técnico | ec.telconet.movil.tecnico.ui.LoginActivity | callPostLogin | Login | Exitoso | El login se realizó exitósamente. |  | breyes | 123456789012345 | Samsung SM-5667 | 45 | 4.4.2 | LTE | 31 | { username: breyes, password: dna8ERsd98rnaf8d23423neq }

Este log debe registrarse en el servidor a través de un servicio web.
>Nota: en caso de no aplicar alguno de los elementos del Log o de no tener forma de obtenerlo este campo se dejará en blanco.

---
**4. Ciclo de Vida de la Aplicación**  
En todo proyecto debe existir la clase Main que herede de Application, esta es la clase que se ejecuta previo a cualquier actividad al momento de iniciar la aplicación 

```java
public class Main extends Application { 
 
    /** 
     * Called when the application is starting, before any activity, service, 
     * or receiver objects (excluding content providers) have been created. 
     */ 
    @Override 
    public void onCreate() { 
            super.onCreate(); 
    } 

    /** 
     * Called when the operating system has determined that it is a good time 
     * for a process to trim unneeded memory from its process. 
     * @param level The context of the trim, giving a hint of the amount of 
     *              trimming the application may like to perform. 
     */ 
    @Override 
    public void onTrimMemory(int level) { 
            super.onTrimMemory(level); 
    }  
} 
```
**5. Ciclo de Vida de las Actividades**

![Android Activity Life Cycle Image](https://developer.android.com/images/activity_lifecycle.png "Android Activity Life Cycle")

- **OnCreate**
Este método es primero en ser ejecutado por el sistema al momento de crear la actividad, en este método se debe inicializar las variables de instancia, vincular los widget con las variables, cargar la data para las listas e inicializar procesos que se ejecutan en segundo plano. 

- **OnStart**
Este método es ejecutado por el sistema posterior a que la actividad se haya creado pero previo a que esta pase a primer plano. En este método se deben llenar los widget con la data correspondiente y registrar los BroadcastReceiver. 

- **OnResume**
Este método es ejecutado por el sistema una vez que ya la actividad esta visible para el usuario. En este método se recomienda recién iniciar animaciones, acceder a la cámara. 

- **OnPause**
Este método es ejecutado por el sistema cuando la actividad está a punto de pasar a segundo plano. En este método se debe detener todo aquello que fue iniciado en el método OnResume. 

- **OnStop**
Este método es ejecutado por el sistema cuando la actividad deja de ser visible para el usuario. En este método se debe detener todo aquello que fue iniciado en el método OnStart. 

- **OnDestroy**
Este método es ejecutado por el sistema cuando la actividad está a punto de ser destruida. En este método se debe detener cualquier proceso que esté vinculado a la actividad. 

- **OnSaveInstanceState**
Este método es ejecutado por el sistema previo ejecutar el método OnStop. En este método se debe guardar el contenido de la pantalla que sea variable, ejemplo: los datos de un formulario; esto permitirá que la próxima vez que se inicie la actividad se puedan recuperar dichos datos. 

- **OnRestoreInstanceState**
Este método es ejecutado por el sistema una vez finalizada la ejecución del método OnStart. En este método se debe verificar si existieron datos de la actividad que necesiten recuperarse y agregarlos a los elementos correspondientes. 

 
```java
public class HomeActivity extends AppCompatActivity { 
 
     @Override 
     protected void onCreate(Bundle savedInstanceState) { 
         super.onCreate(savedInstanceState); 
         setContentView(R.layout.activity_main); 
     } 
 
     @Override 
     protected void onStart() { 
          super.onStart(); 
     } 
 
     @Override 
     protected void onResume() { 
          super.onResume(); 
     } 
 
     @Override 
     protected void onPause() { 
          super.onPause(); 
     } 
 
     @Override 
     protected void onStop() { 
          super.onStop(); 
     } 
 
     @Override 
     protected void onDestroy() { 
          super.onDestroy(); 
     } 
 
     @Override 
     public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) { 
            super.onSaveInstanceState(outState, outPersistentState); 
     } 
 
     @Override 
     public void onRestoreInstanceState(Bundle savedInstanceState) { 
            super.onRestoreInstanceState(savedInstanceState); 
     } 
 
     @Override 
     public void onBackPressed() { 
         super.onBackPressed(); 
     } 
 
}  
```
---
**6. Parámetros de Ambiente**  
Las variables de conexión que dependen del ambiente al cual se desee apuntar (Desarrollo, Pruebas y Producción) deberán ir en el archivo gradle como un campo de configuración.

	buildConfigField '[Tipo de Dato]', '[IDENTIFICADOR]', '[VALOR]'
Ejemplo

	buildConfigField 'String', 'MIDDLEWARE_URL', '"http://middleware.telconet.ec/"'

Esta sentencia deberá incluirse en todos los buildType.

```gradle
buildTypes {
        desarrollo {
            applicationIdSuffix ".dev"
            buildConfigField 'String', 'MIDDLEWARE_URL', '"http://dev.example.com/"'
        }
        pruebas {
            applicationIdSuffix ".test"
            buildConfigField 'String', 'MIDDLEWARE_URL', '"http://test.example.com/"'
        }
        release {
            minifyEnabled false
            buildConfigField 'String', 'MIDDLEWARE_URL', '"http://example.com/"'
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
}
```
Para obtener los valores de estas configuraciones en el código java se usa la sentencia BuildConfig.[IDENTIFICADOR]
```java
import ec.telconet.projectname.BuildConfig;

...

final String baseUrl = BuildConfig.MIDDLEWARE_URL;

...
```
---
**7. Anotaciones**  
Cada que se pase un parámetro de valor a un método y este no sea de tipo primitivo se debe anteponer la anotación `@NonNull` para evitar que se pase un valor null. 

Si por alguna razón un método pudiera llegar a devolver null como respuesta se debe anteponer la anotación `@Nullable` a la declaración de dicho método. 
```java
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

...

@Nullable protected Object doSomething(@NonNull Object object, boolean flag){} 

...
```
---
**8. Variables de Aplicación**  
Son aquellas variables cuyo espacio en memoria permanece reservado mientras el proceso de la aplicación se esté ejecutando. Estas variables se implementan en una clase singleton. 
```java
public class AppData {

    private static final AppData instance = new AppData();

    /**
     * Obtiene la instancia de la clase
     * @return instance {@link AppData}
     */
    public static AppData getInstance() {
        return instance;
    }

    private long timeLastClick;

    /**
     * Tiempo del ultimo evento click que el usuario realizo
     * @return long
     */
    public long getTimeLastClick() {
        return timeLastClick;
    }

    /**
     * Actualiza el tiempo del ultimo evento click que el usuario realizo
     * @param timeLastClick  numero de milisegundos desde since January 1, 1970,
     *                       00:00:00 GMT
     */
    public void setTimeLastClick(long timeLastClick) {
        this.timeLastClick = timeLastClick;
    }
}
```
Para llamar a alguno de los métodos definidos en esta clase, primero se debe obtener la instancia, ejemplo:
```java
	AppData.getInstance().setTimeLastClick((new Date()).getTime());
```
---
**9. Variables de Sesión**  
Son aquellas variables que se almacenan en una base de datos en memoria conocida como `SharedPreferences`. 

> Nota: No se debe guardar imágenes en los Shared Preferences. 

Para acceder a estas variables se debe crear la clase `SharedPreferenceManager` la cual debe implementar métodos estáticos que permitan acceder a las variables de sesión.

```java
public class SharedPreferenceManager {

    /**
     * Obtiene los shared preferences de la aplicacion.
     * @param context el contexto de la aplicacion 'getApplicationContext()'
     * @return Unica {@link SharedPreferences} instancia que puede usarse para
     *         obtener y modificar el valor de alguna preferencia compartida.
     */
    private static SharedPreferences getInstance(@NonNull Context context){
        return context.getSharedPreferences(
                context.getPackageName(),
                Context.MODE_PRIVATE);
    }

    /**
     * Obtiene el Editor de las preferencias compartidas
     * @param context el contexto de la aplicacion 'getApplicationContext()'
     * @return Unica {@link android.content.SharedPreferences.Editor} instancia 
     *         que puede usarse para modificar el valor de alguna preferencia 
     *         compartida.
     */
    @Nullable
    private static SharedPreferences.Editor getEditor(@NonNull Context context){
        SharedPreferences sp = getInstance(context);
        return sp != null ? sp.edit() : null;
    }

    /**
     * Limpia todas las preferencias compartidas.
     * @param context el contexto de la aplicacion 'getApplicationContext()'
     */
    public static void clearSharedPreferences(Context context){
        SharedPreferences.Editor editor = getEditor(context);
        if(editor != null) {
            editor.clear();
            editor.apply();
        }
    }

    /**
     * Obtiene el nombre del usuario.
     * @param context el contexto de la aplicacion 'getApplicationContext()'
     */
    @Nullable public static String getUsername(Context context){
        SharedPreferences sp = getInstance(context);
        return sp.getString("username", null);
    }

    /**
     * Guarda el nombre del usuario en las preferencias compartidas
     * @param context el contexto de la aplicacion 'getApplicationContext()'
     * @param username nombre del usuario
     */
    public static void setUsername(@NonNull Context context, @NonNull String username){
        SharedPreferences.Editor editor = getEditor(context);
        if(editor != null) {
            editor.putString("username", username);
            editor.apply();
        }
    }

}
```
Ejemplo:
```java
String username = SharedPreferenceManager.getUsername(getApplicationContext());
```
---
**10. Comunicación con el Middleware**  
Con la finalidad de establecer una comunicación con el middleware se usará la librería Retrofit. Para esto es necesario implementar: una clase `RetrofitManager` que administre la conexión, una interfaz `ITelcosAPI` en la que se definen los metodos y servicios del middleware que se van a consumir, y finalmente los controladores que se encarguen tanto de mapear y ejecutar las peticiones como de parsear las respuestas o las fallas en caso que se presenten.
 
Se creará una clase controlador por cada servicio web, cada una de estas clases contará con métodos estáticos para consumir el servicio `exec`, para procesar la respuesta `onSuccess` y para manejar algún fallo `onFailure`. Cada una de estas clases tendrán el prefijo `Call`, seguido del nombre del método al que representa en el middleware. 

	root
	|-- app
		|-- java
			|-- interfaces
				|
				|-- ITelcosAPI.java
				|
			|-- network
				|
				|-- RetrofitManager.java
				|
				|-- sync
					|
					|-- CallGetTareasPendientes.java
					|


-  **Administrador de Conexión Retrofit (RetrofitManager.java)**

```java
import ec.telconet.projectname.BuildConfig;

import android.support.annotation.NonNull;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitManager {

private final static int DEFAULT_TIMEOUT = 60; // Seconds

    /**
     * Devuelve el objeto de la libreria de retrofit encargado de la conexion
     * @param host direccion del host
     * @param connectionTimeout limite de tiempo en segundos
     * @return {@link Retrofit}
     */
    @NonNull
    public static Retrofit getRetrofitManager(@NonNull String host, int connectionTimeout){

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .writeTimeout(0, TimeUnit.MILLISECONDS)
                .connectTimeout(connectionTimeout, TimeUnit.SECONDS)
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(host)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    /**
     * Devuelve el objeto de la libreria de retrofit encargado de la conexion
     * @param host direccion del host
     * @return {@link Retrofit}
     */
    @NonNull
    public static Retrofit getRetrofitManager(@NonNull String host){
        return getRetrofitManager(host, DEFAULT_TIMEOUT);
    }
}
```

-  **Interfaz Java del Middleware (ITelcosAPI.java)**

```java
import ec.telconet.projectname.network.sync.CallGetParameters;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ITelcosAPI {

    @GET("parameters")
    @Headers("Content-Type: application/json")
    Call<CallGetParameters.ResponseModel> getParameters();

    @POST("login")
    @Headers("Content-Type: application/json")
    Call<CallLogin.ResponseModel> login(
            @NonNull @Body CallLogin.RequestModel bodyRequest);

    @GET("user/{id}/profile")
    @Headers("Content-Type: application/json")
    Call<CallGetUserProfile.ResponseModel> getUserProfile(
            @NonNull @Path("id") String id);

    @GET("logout")
    @Headers("Content-Type: application/json")
    Call<CallLogout.ResponseModel> logout(
            @NonNull @Header("securityToken") String token);

    @GET("contracts")
    @Headers("Content-Type: application/json")
    Call<CallGetContracts.ResponseModel> getContracts(
            @Query("after") String initialDate);

}
```
- **Controlador (CallGetParameters.java)**

```java
import ec.telconet.projectname.interfaces.ITelcosAPI;
import ec.telconet.projectname.models.Parametros;
import ec.telconet.projectname.network.RetrofitManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.support.annotation.Nullable;


public class CallGetParameters {

	private final static String API_URL = "http://telconet.ec/";

    /**
     *
     * @param callback
     * @return
     */
    @Nullable public static String exec (Callback<ResponseModel> callback){
        Call<ResponseModel> call    = null;
        ITelcosAPI telcosAPI        = null;
        String failureMessage       = "";

        try {
            telcosAPI = RetrofitManager
                    .getRetrofitManager(API_URL).create(ITelcosAPI.class);
            call = telcosAPI.getParameters();
            call.enqueue(callback);
        }catch (Exception e){
            failureMessage = "Error: "
                    + e.getLocalizedMessage()
                    + (e.getCause() == null
                        ? ""
                        : "\t\nCause: "
                            + e.getCause().getLocalizedMessage()
                            + "\r\nStacktrace: "
                            + Arrays.toString(e.getStackTrace())
                    );
            if(call != null && call.request() != null){
                failureMessage = failureMessage
                        + "\t\nOn Request: ";
                if(call.request().url() != null) {
                    failureMessage = failureMessage
                            + "\t\n\tUrl: "
                            + call.request().url().toString();
                }
                if(call.request().headers() != null) {
                    failureMessage = failureMessage
                            + "\t\n\tHeaders: "
                            + call.request().headers().toString();
                }
                if(call.request().body() != null) {
                    failureMessage = failureMessage
                            + "\t\n\tBody: "
                            + call.request().body().toString();
                }
            }
        }
        return failureMessage;
    }

    /**
     *
     * @param call
     * @param response
     * @return
     */
    public static List<Object> onSuccess(Call<ResponseModel> call
            , Response<ResponseModel> response){
        List<Object> result     = new ArrayList<>();
        String err              = "";

        try {
            err = response.errorBody().toString();
        } catch(Exception e){
            err="";
        }

        if(err.equalsIgnoreCase("")){
            if(response.body() != null){
                if(response.isSuccessful()){
                    ResponseModel resp = response.body();
                    if(resp.getStatus() == "200"){
                        result = resp.getParametros();
                    }else{
                        // TODO: Log - Fail
                    }
                }else{
                    // TODO: Log - Http Status not in 200 - 300
                }
            }else{
                // TODO: Log - Body is empty
            }
        }else{
            // TODO: Log - Connection Error
        }
        return result;
    }

    /**
     *
     * @param call
     * @param t
     * @return
     */
    @Nullable public static String onFailure(Call<ResponseModel> call
            , Throwable t){
        String failureMessage = "";
        if(t != null){
            failureMessage = "Error: "
                    + t.getLocalizedMessage()
                    + (t.getCause() == null
                    ? ""
                    : "\t\nCause: "
                        + t.getCause().getLocalizedMessage()
                        + "\r\nStacktrace: "
                        + Arrays.toString(t.getStackTrace())
                    );
            if(call != null && call.request() != null) {
                failureMessage = failureMessage
                        + "\t\nOn Request: ";
                if(call.request().url() != null) {
                    failureMessage = failureMessage
                            + "\t\n\tUrl: "
                            + call.request().url().toString();
                }
                if(call.request().headers() != null) {
                    failureMessage = failureMessage
                            + "\t\n\tHeaders: "
                            + call.request().headers().toString();
                }
                if(call.request().body() != null) {
                    failureMessage = failureMessage
                            + "\t\n\tBody: "
                            + call.request().body().toString();
                }
            }
        }
        return failureMessage;
    }


    /**
     *
     */
    public class ResponseModel {
        private String mensaje;
        private String status;
        private List<Object> parametros;

        public String getMensaje() {
            return mensaje;
        }

        public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Object> getParametros() {
            return parametros;
        }

        public void setParametros(List<Object> parametros) {
            this.parametros = parametros;
        }
    }
}
```

Ejemplo

```java
import ec.telconet.projectname.network.sync.CallGetParameters;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.util.Log;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

...

    private void callGetParameters(){
        String failureMessage = CallGetParameters.exec(
                new Callback<CallGetParameters.ResponseModel>() {

            @Override
            public void onResponse(Call<CallGetParameters.ResponseModel> call,
                                   Response<CallGetParameters.ResponseModel> response) {
                List<Object> result = CallGetParameters.onSuccess(call, response);
                Log.d(TAG, "CallGetParameters retrieve " + result.size() + " results.");
                // TODO: Save parameters as App Variables
            }

            @Override
            public void onFailure(Call<CallGetParameters.ResponseModel> call, Throwable t) {
                String failureMessage = CallGetParameters.onFailure(call, t);
                Log.i(TAG, failureMessage);
                // TODO: Show failure message to the user
            }
        });

        if(!failureMessage.isEmpty()){
            Log.i(TAG, failureMessage);
            // TODO: Show failure message to the user
        }
    }
    
...

}
```
---

  
 
[1]: /Estandares/Java/Estandares-de-Programacion-Java-Version-1.0.md




