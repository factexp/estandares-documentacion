# Services en Symfony

Un **Service** es un objeto PHP que realiza una tarea específica. Un service usualmente es utilizado globalmente, como un objeto de conexión a la base de datos o un objeto que envía mensajes de correo. En Symfony2, los servicios se suelen configurar y obtener del *service container*. Cada service es utilizado en la aplicación en cualquier momento que se necesite la funcionalidad específica que provee. Se suele decir que una aplicación que tiene muchos servicios desacoplados sigue una *[arquitectura orientada a servicios][1]*.

El **Service Container**, también conocido como **Dependency Injection Container**, es un objeto especial que administra la instanciación de servicios dentro de una aplicación. En lugar de crear servicios directamente, el desarrollador configura el service container para que sepa cómo crearlos. El service container se ocupa de instanciar e inyectar servicios dependientes.

>Se recomienda la lectura de la [documentación de Symfony2 sobre Service Container][2].


A continuación se explica los pasos para crear y consumir un service:

## 1. Creación de un Service

#### 1.1. Configuración en services.yml

Se debe definir el service en el archivo **services.yml** ubicado en la ruta `Resources/config` del bundle correspondiente. Cada bundle puede tener services propios.

El **nombre** del service debe ser el nombre del bundle, seguido por un punto, seguido por el nombre del service en UpperCamelCase.

Se debe indicar el nombre completo de la **clase** que implementa el service, que deberá estar ubicada en la ruta `Service` del Bundle. El nombre de la clase debe terminar con el sufijo `Service`.

Finalmente se especifica que una vez instanciado el service, se llamará a su método **setDependencies**, enviándole como parámetro al service container, para más adelante poder especificar las dependencias del service en la implementación de la clase. Un service puede utilizar otros services del mismo bundle o de otro bundle de la aplicación.

Por ejemplo, los services de un `fooBundle` estarían configurados en el archivo `telconet\fooBundle\Resources\config\services.yml`:

```yaml
parameters:
#    foo.example.class: telconet\fooBundle\Example

services:

    foo.SomeFoo:
        class: telconet\fooBundle\Service\SomeFooService
        calls:
            - [setDependencies, [@service_container]]

    foo.OtherFoo:
        class: telconet\fooBundle\Service\OtherFooService
        calls:
            - [setDependencies, [@service_container]]
```

Además otro bundle de la misma aplicación, por ejemplo `barBundle` podría tener más services configurados, en el archivo `telconet\barBundle\Resources\config\services.yml`. También se puede definir parameters propios del bundle, similares a los parameters generales definidos en `app\config\parameters.yml`.

```yaml
parameters:
    bar.hello: 'Hello World!'

services:

    bar.Validator:
        class: telconet\barBundle\Service\ValidatorService
        calls:
            - [setDependencies, [@service_container]]

    bar.Mailer:
        class: telconet\barBundle\Service\MailerService
        calls:
            - [setDependencies, [@service_container]]

    bar.PlusUltra:
        class: telconet\barBundle\Service\PlusUltraService
        calls:
            - [setDependencies, [@service_container]]

```

#### 1.2. Implementación de la clase

Se debe crear la clase que implementa el service dentro de la carpeta `Service` del Bundle. Esta clase no hereda de ninguna otra, ni implementa ninguna interfaz.

Al inicio de la clase, se indica como propiedades privadas las **dependencias** que requiere el service para su correcto funcionamiento, ya sean entity managers, parameters u otros services. Se recomienda especificar el tipo de dato de cada dependencia en el comentario, para facilitar su uso con ayuda del IDE.

A continuación se define el método **setDependencies**, que recibe el service container. Dentro de este método se obtiene las dependencias del service container, y se las almacena en las propiedades privadas. El service no debe conservar referencia alguna al service container. 

Finalmente se define los **métodos** con lógica del negocio. Por lo general deben recibir arrays con los parámetros necesarios para trabajar, los cuales podrían venir desde un form de un controller web, o desde un método de web service.

```php
<?php
namespace telconet\fooBundle\Service;
        
class SomeFooService
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $emDefault;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $emAlternate;
    /**
     * @var \telconet\barBundle\Service\ValidatorService
     */
    private $validator;
    /**
     * @var \telconet\fooBundle\Service\OtherFooService
     */
    private $serviceOtherFoo;
    /**
     * @var string
     */
    private $hello;

    public function setDependencies(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        // entity managers
        $this->emDefault = $container->get('doctrine.orm.telconet_entity_manager');
        $this->emAlternate = $container->get('doctrine.orm.telconet_alternate_entity_manager');
        // otros services
        $this->validator = $container->get('bar.Validator');
        $this->serviceOtherFoo = $container->get('foo.OtherFoo');
        // parameters (validando que este definido)
        $this->hello = ($container->hasParameter('bar.hello') ? $container->getParameter('bar.hello') : 'Hello');
    }

    /**
     * Guarda el archivo, crea el registro en estado Pendiente,
     * define el mensaje inicial, devuelve el registro creado.
     * @param string $empresaCod
     * @param string $usrCreacion
     * @param string $ipCreacion
     * @param array $datos_form_files
     * @return InfoRegistro
     */
    public function guardarRegistro($empresaCod, $usrCreacion, $ipCreacion, $datos_form_files)
    {
        $entityRegistro = new InfoRegistro();
        $entityRegistro->setFile($datos_form_files['file']);
        if ($entityRegistro->getFile())
        {
            $entityRegistro->preUpload();
            $entityRegistro->upload();
        }
        $entityRegistro->setEstado('Pendiente');
        $entityRegistro->setFeCreacion(new \DateTime('now'));
        $entityRegistro->setEmpresaCod($empresaCod);
        $entityRegistro->setUsrCreacion($usrCreacion);
        $entityRegistro->setIpCreacion($ipCreacion);
        // uso de parameter
        $entityRegistro->setMensaje($this->hello);
        // uso de otro service
        $this->validator->validateAndThrowException($entityRegistro);
        // uso de entity manager
        $this->emDefault->persist($entityRegistro);
        $this->emDefault->flush();
        return $entityRegistro;
    }
```

## 2. Consumo de un Service

Una vez definido el service, se lo debe invocar desde los métodos de controllers web o desde los métodos de web service controllers.

Como un controller específico hereda de la clase `Controller`, internamente tiene acceso al service container, así que se puede obtener referencias a los services configurados en la aplicación mediante el método heredado `get()`. 

Se recomienda especificar el tipo de dato del service en el comentario, para facilitar su uso con ayuda del IDE.


```php
<?php

class SomeController extends Controller  
{
    public function guardarRegistroAction()
    {
        // obtener parametros web
        $request = $this->getRequest();
        $session = $request->getSession();
        $empresaCod = $session->get('idEmpresa');
        $usrCreacion = $session->get('user');
        $clientIp = $request->getClientIp();
        $datos_form_files = $request->files->get('inforegistrotype');

        // obtener service
        /* @var $serviceOtherFoo \telconet\fooBundle\Service\OtherFooService */
        $serviceOtherFoo = $this->get('foo.OtherFoo');

        // invocar metodo de service
        $entityInfoRegistro = $serviceOtherFoo->guardarRegistro($empresaCod, $usrCreacion, $clientIp, $datos_form_files);

        // respuesta web
        $session->getFlashBag()->add('exito', 'Registro guardado correctamente');
        return $this->redirect($this->generateUrl('inforegistro_list', array('idReg' => $entityInfoRegistro->getId())));

    }
}
```

  [1]: http://en.wikipedia.org/wiki/Service-oriented_architecture
  [2]: http://symfony.com/doc/current/book/service_container.html