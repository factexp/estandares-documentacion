# RESTful Web Services con Tokens de Seguridad en Symfony

## Configuración YML

Para la creación de Web Services REST en Symfony, el módulo debe contar con una entrada en el archivo `app/config/restfulws.yml`, apuntando a un archivo `restfulws.yml` que debe existir dentro de la configuración del módulo. La entrada debe iniciar con el prefijo `rs_`, seguida del nombre del módulo, y debe definir como prefijo el nombre del módulo. Actualmente están configurados los módulos comercial, financiero y seguridad:

```yaml
# RESTful Web Services: anidados en /rs

rs_comercial:
  resource: @comercialBundle/Resources/config/restfulws.yml
  prefix:   /comercial

rs_financiero:
  resource: @financieroBundle/Resources/config/restfulws.yml
  prefix:   /financiero

rs_seguridad:
  resource: @seguridadBundle/Resources/config/restfulws.yml
  prefix:   /seguridad
```


Dentro del archivo `restfulws.yml` del módulo, se debe indicar las distintas rutas de web services que se van a anidar, indicando el prefijo para cada una, apuntando al archivo yml correspondiente por carpeta, dentro de una carpeta `restfulws`. Las entradas deben comenzar con el nombre de la ruta del módulo, más uno único correspondiente al web service. Se indica el formato generado y requerido (de preferencia JSON), así como el método usado (de preferencia POST). Por ejemplo, para el módulo de seguridad, en el archivo `src/telconet/seguridadBundle/Resources/config/restfulws.yml` se ha configurado dos web services, detallados en dos archivos yml independientes, ambos dentro de la carpeta `src/telconet/seguridadBundle/Resources/config/restfulws`.

```yaml
# RESTful Web Services: anidados en /rs/seguridad

rs_seguridad_notoken:
  resource: restfulws/notoken.yml
  defaults: { _format: json }
  requirements: { _method: post, _format: json }
  prefix: /notoken

rs_seguridad_testtoken:
  resource: restfulws/testtoken.yml
  defaults: { _format: json }
  requirements: { _method: post, _format: json }
  prefix: /testtoken
```

Dentro de un archivo yml correspondiente a un web service, se define las rutas a los métodos web, que corresponden a acciones en controladores de web services. Las entradas deben comenzar con el prefijo del web service, más uno específico para el método. Se debe indicar la ruta completa de la clase y el nombre completo del método. No sirve la sintaxis resumida que suele usarse para controladores normales. Por ejemplo, se ha configurado el web service de pruebas en el archivo `src/telconet/seguridadBundle/Resources/config/restfulws/testtoken.yml`:

```yaml
# RESTful Web Services: anidados en /rs/seguridad/testtoken

rs_seguridad_testtoken_testlogin:
  pattern:  /testlogin
  defaults: { _controller: telconet\seguridadBundle\WebService\TestTokenWSController::testLoginAction }

rs_seguridad_testtoken_testprocess:
  pattern:  /testprocess
  defaults: { _controller: telconet\seguridadBundle\WebService\TestTokenWSController::testProcessAction }

rs_seguridad_testtoken_testprocessgenerate:
  pattern:  /testprocessgenerate
  defaults: { _controller: telconet\seguridadBundle\WebService\TestTokenWSController::testProcessGenerateAction }
```

## Programación del WSController PHP

Los controladores de web services deben ser creados dentro de la carpeta WebService del módulo. Deben ser nombrados con un sufijo `WSController.php`. Deben heredar de `BaseWSController` (`telconet\schemaBundle\DependencyInjection\BaseWSController`). Ver más abajo la definición de la clase `TestTokenWSController` (`src/telconet/seguridadBundle/WebService/TestTokenWSController`).

Los métodos deben recibir la data de trabajo en formato `application/json`, como parte del contenido directo del request (no como campos de un formulario), por lo que siempre se va a usar `$request->getContent()` y luego `json_decode(.., true)` (para decodificar el string como arreglo asociativo y no como `stdClass`). Se recomienda validar los campos de la data obtenida, para no generar errores *PHP Notice*.

Todos los formatos de respuesta de ls métodos del web service, deben obedecer al requerimiento de la aplicación remota. Los métodos mostrados a continuación son solo ejemplos sencillos del uso del servicio de generación y validación de tokens, accesible mediante los métodos explicados, gracias a que se hereda de `BaseWSController`.

Los métodos que requieran **generar tokens en base al login exitoso del usuario**, deben invocar el método `generateToken`, enviando el nombre de la aplicación remota para la cual se está generando el token y el `username`. Si se pudo generar el token, se retornará como `string`, caso contrario se retornará `false`, lo cual debe validarse para devolver una respuesta adecuada. Ver más abajo método de ejemplo `testLogin`.

Los métodos que requieran **realizar procesos luego de validar tokens**, deben invocar el método `validateToken`, enviando el token recibido. Si el token es válido, se retornará `true`, caso contrario se retornará `false`, en base a lo cual se deberá ejecutar o no el proceso y luego devolver una respuesta adecuada. Ver más abajo método de ejemplo `testProcess`.

Los métodos que requieran **realizar procesos luego de validar tokens y finalmente devolver nuevos tokens**, deben invocar el método `validateGenerateToken`, enviando el token recibido, el nombre de la aplicación remota para la cual se está generando el token y el `username`. Si el token es válido y se pudo generar uno nuevo, se retornará como `string`, caso contrario se retornará `false`, en base a lo cual se deberá ejecutar o no el proceso y luego devolver una respuesta adecuada incluyendo el token posiblemente generado. Ver más abajo método de ejemplo `testProcessGenerate`.


```php
<?php

namespace telconet\seguridadBundle\WebService;

use telconet\schemaBundle\DependencyInjection\BaseWSController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * RESTful Web Service Controller para prueba de procesos con tokens de seguridad
 * @author ltama
 */
class TestTokenWSController extends BaseWSController
{

    /**
     * Metodo de ejemplo que simula un login y devuelve respuesta incluyendo un token generado
     * @param Request $request
     * @return Response
     */
    public function testLoginAction(Request $request)
    {
        // simular validacion de usuario y clave para luego generar un token
        $data = json_decode($request->getContent(), true);
        if (!empty($data['username']) && !empty($data['password']) &&
            (($data['username'] == 'SomeUser' && $data['password'] == 'SomePass') ||
            ($data['username'] == 'AnotherUser' && $data['password'] == 'AnotherPass'))
        )
        {
    	    // luego de login exitoso, generar token desde un source especifico
    	    $token = $this->generateToken('SomeApp', $data['username']);
    	    if (!$token)
    	    {
    	        // si la generacion de token devuelve false, devolver response con error segun estructura del metodo
    	        return new Response(json_encode(array(
                    'msg' => 'Error al iniciar sesion',
                    'token' => null
    	        )));
    	    }
	        // devolver response incluyendo token segun estructura del metodo
	        return new Response(json_encode(array(
                'msg' => 'Sesion iniciada exitosamente',
                'token' => $token
	        )));
        }
        else
        {
    	    // si el login falla, devolver response con error segun estructura del metodo
    	    return new Response(json_encode(array(
                'msg' => 'Credenciales incorrectas',
                'token' => null
	        )));
        }
    }

    /**
     * Metodo de ejemplo que valida el token, ejecuta un proceso y devuelve el resultado, sin nuevo token
     * @param Request $request
     * @return Response
     */
    public function testProcessAction(Request $request)
    {
        // obtener data del request como array asociativo
        $data = json_decode($request->getContent(), true);
        // primero validar el token sin generar siguiente token
        $token = $this->validateToken($data['token']);
        if (!$token)
        {
            // si la validacion devuelve false, devolver response con error segun estructura del metodo
            return new Response(json_encode(array(
                'msg' => 'No se pudo elevar el valor al cuadrado',
                'result' => null
            )));
        }
        // una vez validado el token, ejecutar proceso (elevar al cuadrado el value dado)
        $square = pow($data['value'], 2);
        // devolver response segun estructura del metodo
        return new Response(json_encode(array(
            'msg' => 'Valor elevado al cuadrado exitosamente',
            'result' => $square
        )));
    }

    /**
     * Metodo de ejemplo que valida el token, genera un token nuevo, ejecuta un proceso y devuelve el token generado
     * @param Request $request
     * @return Response
     */
    public function testProcessGenerateAction(Request $request)
    {
        // obtener data del request como array asociativo
        $data = json_decode($request->getContent(), true);
        // primero validar el token y generar el siguiente
        $token = $this->validateGenerateToken($data['token'], 'SomeApp', $data['user']);
        if (!$token)
        {
            // si la validacion devuelve false, devolver response con error segun estructura del metodo
            return new Response(json_encode(array(
                'msg' => 'No se pudo elevar el valor al cuadrado',
                'result' => null,
                'token' => null
            )));
        }
        // una vez validado el token, ejecutar proceso (elevar al cuadrado el value dado)
        $square = pow($data['value'], 2);
        // devolver response y nuevo token segun estructura del metodo
        return new Response(json_encode(array(
            'msg' => 'Valor elevado al cuadrado exitosamente',
            'result' => $square,
            'token' => $token
        )));
    }

}
```

## Servidor de Tokens

En el archivo `app/config/parameters.yml` se debe configurar las rutas del servidor de generación/validación de tokens, los correos a los cuales notificar en caso de error de comunicación con el servidor de tokens, y si se debe verificar o no el certificado SSL del servidor de tokens.

```yaml
# Tokens de Seguridad
    # rutas de generacion/validacion locales de prueba, se debe indicar rutas validas
    seguridad.token_generate_url: https://dev-telcos-developer.telconet.ec/rs/seguridad/notoken/nogenerate
    seguridad.token_validate_url: https://dev-telcos-developer.telconet.ec/rs/seguridad/notoken/novalidate
    seguridad.token_validate_generate_url: https://dev-telcos-developer.telconet.ec/rs/seguridad/notoken/novalidategenerate
    # emails para notificar errores, se muestra valores por defecto
    seguridad.token_mail_error:
       - notificaciones_telcos@telconet.ec
       - telcos@telconet.ec
    # true por defecto, usar false solo si se desea probar validacion/generacion de tokens contra un servidor sin certificado SSL valido
    seguridad.token_ssl_verify: false
```
