
**Estándares de Programación en Symfony**
----------------------------------------------
_________________________________________________________________________


Los siguientes criterios serán evaluados y tomados en consideracion en las revisiones de calidad, además de los estándares establecidos.


**Criterio # 1**
---------------


_Si se modifica un **JS**, se debe modificar los **TWIG** que lo invocan, aumentando la numeración en el URL, por ejemplo:_

```
{{ asset('./bundles/tecnico/js/InfoServicio/botonesIndex.js?37') }}
```
_Asegurando que todos los TWIG tengan la misma numeración (la más alta). Se puede buscar en todo el código fuente del proyecto (carpeta src) con grep, por ejemplo:_

```
grep "InfoServicio/indexBotones.js" src -irn
```

**Criterio # 2**
----------------

_Validar que un objeto no sea nulo antes de acceder a sus métodos o atributos. Se debe usar `is_object`, por ejemplo:_

```
if(is_object($objVariable))
{
   ...
}
```

**Criterio # 3**
----------------

_Las consultas siempre deben ser por un código, no por el id de la tabla._

**Criterio # 4**
----------------

_Manejo de excepciones en métodos de repositorios._

- _Si es una consulta simple no debería producir excepciones._
- _Para métodos más complejos, se puede capturar excepciones para dar un tratamiento específico como parte de la lógica del método, por ejemplo al intentar obtener un valor único, o si se quiere retornar resultado vacío, nulo, u otro valor controlado en caso de excepciones._
- _Debe documentarse este comportamiento en el método. Nunca debe capturarse excepciones para simplemente relanzarlas sin dar ningún tratamiento._
- _Se puede manejar posibles excepciones del repositorio en el Controller o Service con `insertError` o `error_log` para no mostrar al usuario el mensaje de la excepción._

**Criterio # 5**
----------------
_Si se va a retornar una respuesta en formato JSON desde un método de Controller, en lugar de usar:_

```
$objRespuesta = new Response();
$objRespuesta->headers->set('Content-Type', 'text/json'); 
```

_Usar en su lugar:_

```
$objRespuesta = new JsonResponse();
```

**Criterio # 6**
----------------
_Manejo de excepciones en transacciones. Las transacciones que involucren más de un registro deben estar contenidas en un bloque try-catch. Debe iniciarse la transacción antes de iniciar el bloque **try**, por ejemplo_

```
$emComercial->beginTransaction()

try
{
   ...
}
catch(\Exception)
{
   ...
}
```

_El commit debe hacerse al final del bloque **try**, antes del **catch**. No debe hacerse más operaciones luego del commit, solo se puede definir código/mensaje de éxito, por ejemplo:_

```
try
{
   ...
   $emComercial->commit();
}
catch(\Exception)
{
   ...
}
```

_En el bloque catch debe capturarse todo tipo de \Exception, hacer rollback validando existencia de transaccion, luego cerrarse el entityManager, guardar registro del error y finalmente se puede definir código/mensaje de error, por ejemplo:_

```
catch(\Exception)
{
    if($emComercial->getConnection()->isTransactionActive())
    {
        $emComercial->rollback();
        $serviceUtil->insertError(...)
    }
    $emComercial->close(); 
}

$respuesta->setContent("Se elimino la entidad");
return $respuesta;

```

**Criterio # 7**
----------------
_Se debe hacer **flush** solamente cuando sea necesario que los registros persistidos estén disponibles para las siguientes operaciones, por ejemplo cuando luego de guardar una cabecera se necesita guardar sus detalles. No se debe hacer flush luego de cada persist, porque esto hace más lentas las transacciones._


**Criterio # 8**
----------------
_No se debe enviar un entity manager como parámetro a un método de repository. El entity manager con el que se invoca el método de repository debe ser capaz de consultar las tablas que necesite. Para eso debe configurarse grants y sinónimos según sea necesario._

**Criterio # 9**
----------------
_No se debe enviar un entity manager como parámetro a un método de service. El service debe tener una referencia al entity manager como campo privado, inicializado en el método setDependencies._

**Criterio # 10**
----------------
_No se debe usar el método `__construct` para inicializar un service._
_Debe usarse el método setDependencies con el container como un único parámetro, por ejemplo:_

```
public function setDependencies(\Symfony\Component\DependencyInjection\ContainerInterface $container). 
```

_En el método **setDependencies** debe obtenerse del container todos los services, entity manager y parámetros necesarios, y conservarlos como variables privadas del service, por ejemplo:_

```
$this->serviceInfoPago                 = $container->get('financiero.InfoPago');
$this->emGeneral                       = $container->get('doctrine.orm.telconet_general_entity_manager'); 
$this->strURLWifiCambiarEstadoUsuarios = $container->getParameter('wifi.ws_cambiarestadousuarios_url');
```
_No debe conservarse variable privada con referencia al container como tal. En el archivo services.yml solo debe definirse el service con un bloque calls: indicando que invoca al método setDependencies con el container como único parámetro, por ejemplo:_
```
calls:
    - [setDependencies, [@service_container]]
```


**Criterio # 11**
----------------

_No es `$intIdEmpresa`, debe ser `$strCodEmpresa` , porque es string._


**Criterio # 12**
----------------
_Al declarar constantes se lo debe hacer con la frase const, ejemplo:_

```
const CONSTANTE = "valor"; 
```

_Y para acceder a la constante dentro de la clase se debe usar la frase self, ejemplo:_

```
self::CONSTANTE
```

**Criterio # 13**
----------------

_Todas las variables deben ser inicializadas según su tipo, ejemplo:_

```
$intCount           = 0;
$floatTotal         = 0;
$strNombre          = '';
$arrayParametros    = array();
```

**Criterio # 14**
----------------

_Se debe validar si el elemento de un array no es vacío usando las funciones **isset** y **empty** en la misma validación, ejemplo:_

```
if(isset($arrayParametro['strKey']) && !empty($arrayParametro['strKey']))
{
    ...   
}
```

**Criterio # 15**
----------------

_Los keys de los array también deben cumplir con el estándar de nombres de variables con prefijo por tipo de dato._

**Criterio # 16**
----------------

_Manejo de valores por defecto para arreglos de parametros:_

```
$strOrigen    = isset($arrayParams['strOrigen']) ? $arrayParams['strOrigen'] : 'WEB';
$strDocumento = isset($arrayParams['strDocumento']) ? $arrayParams['strDocumento'] : null;

```



**Criterio # 17**
----------------

_En los js para validar si una variable está vacia se puede usar la función del platform de_ `EXTjs -> Ext.isEmpty()` _, ejemplo:_

```
var strVariable  = '';
Ext.isEmpty(strVariable)    //[Returns true]
var objVariable = null;
Ext.isEmpty(objVariable)    //[Returns true]
var objVariable;
Ext.isEmpty(objVariable)    //[Returns true]
var arrayVariable = {};
Ext.isEmpty(arrayVariable)  //[Returns true]
```

**Criterio # 18**
----------------

_Evitar caer en el problema de las N+1 consultas a la BD._


**Criterio # 19**
----------------

_Todos los métodos deben recibir un **array** de está manera controlamos la afectación en los procesos donde el método es invocado cuando se requiere enviar un nuevo parámetro._

