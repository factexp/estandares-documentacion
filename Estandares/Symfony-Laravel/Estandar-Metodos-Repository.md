## Estándar para métodos de un Repository

#### Estándar para el nombre del método

El nombre de los métodos para obtener Resultados de Queries deberá empezar con:
    
    public function getResultadoNombreFuncion()
    {
        try
        {
            //codigo con Query Builder
            //codigo con Result Mapping
            //codigo con Native Query
        }
        catch(\Exception $e)
        {
            error_log($e->getMessage());
        }
    }

El nombre de los métodos para obtener Resultados de JSON deberá empezar con:

    public function getJsonNombreFuncion()
    {
        //codigo 
    }

#### Estándar para recibir parámetros en el método

Para recibir parámetros en el método, existen dos formas:

1. Si reciben hasta un máximo de 4 parámetros sería de esta forma:

    ```php
    /*
     * Descripción del método
     *
     * @author
     * @version
     * @param tipoVariable $intParametro1
     * @param tipoVariable $strParametro2
     * @param tipoVariable $intParametro3
     * @param tipoVariable $strParametro4
     * @return tipoVariable $arrayResultado
     * @costoQuery el costo del query que se va a utilizar
     */
    public function getResultadoNombreFuncion($intParametro1, $strParametro2, $intParametro3, $strParametro4)
    {
        try
        {
            //codigo con Query Builder
            //codigo con Result Mapping
            //codigo con Native Query
        }
        catch(\Exception $e)
        {
            error_log($e->getMessage());
        }
    }

2. Si reciben más de 4 parámetros sería de esta forma:
     
    ```php
    /*
     * Descripción del método
     *
     * @author
     * @version
     * @param array $arrayParametros [intParametro1, strParametro2, intParametro3, strParametro4, intParametro5, strParametro6]
     * @return array $arrayResultado[total, resultado]
     * @costoQuery el costo del query que se va a utilizar
     */
    public function getResultadoNombreFuncion($arrayParametros)
    {
        //codigo con Query Builder
        //codigo con Result Mapping
        //codigo con Native Query
    }

Nota: Favor no olvidarse de escribir el comentario de la función.
      Favor solicitar al DBA el costo del query.

#### Estándar para return Query resultados del método

Para obtener los resultados y el total al momento de ejecutar un query, se deberá devolver un array con resultado y total. Ejemplo:

    public function getResultadoNombreFuncion($arrayParametros)
    {
        $arrayRespuesta['total']     = 0;
        $arrayRespuesta['resultado'] = "";
        try
        {
            //codigo con Query Builder
            //codigo con Result Mapping
            //codigo con Native Query

            $intTotal       = $objQueryCont->getSingleScalarResult();
            $arrayResultado = $objQuery->getResult();
            //ó
            $arrayResultado = $objQuery->getArrayResult();

            $arrayRespuesta['total']     = $intTotal;
            $arrayRespuesta['resultado'] = $arrayResultado;
        }
        catch(\Exception $e)
        {
            error_log($e->getMessage());
        }

        return $arrayRespuesta;
    }

#### Estándar para return JSON resultados del método

Para obtener el resultado con formato JSON, se deberá realizar lo siguiente:

    public function getJSONNombreFuncion($arrayParametros)
    {
        $arrayEncontrados = array();
        $arrayResultado   = $this->getResultadoNombreFuncion($arrayParametros);
        $tipoData         = $arrayResultado['resultado'];
        $intTotal         = $arrayResultado['total'];

        //codigo y se llena variable $arrayEncontrados
	
        $arrayRespuesta = array('total' => $total, 'encontrados' => $arrayEncontrados);
        $jsonData       = json_encode($arrayRespuesta);

        return $jsonData;
    }
