# Inyección SQL en PHP y Doctrine #

> ![](http://imgs.xkcd.com/comics/exploits_of_a_mom.png)
> Exploits of a Mom - http://xkcd.com/327/

Muchos desarrolladores web son desprevenidos acerca de cómo las consultas SQL pueden ser manipuladas, y asumen que una consulta SQL es un comando confiable. Esto significa que las consultas SQL pueden saltar controles de acceso, y por lo tanto, sobrepasar las revisiones de autenticación y autorización, y que algunas veces las consultas SQL aún podrían permitir el acceso de comandos a nivel de sistema operativo del servidor.

La Inyección Directa de Comandos SQL es una técnica donde un atacante crea o altera comandos SQL existentes para exponer datos ocultos, reemplazar los que son importantes, o peor aún, ejecutar comandos peligrosos a nivel de sistema en el equipo donde se encuentra la base de datos. Esto se logra a través de la aplicación, tomando la entrada del usuario y combinándola con parámetros estáticos para elaborar una consuta SQL.

> Se recomienda la lectura de la [documentación de PHP sobre inyección SQL][1].

Por su parte, **Doctrine** no puede prevenir los ataques de inyección SQL si el desarrollador es descuidado. Solamente unos pocos APIs de Doctrine están diseñados para ser seguros frente a inyecciones SQL, pero se debe considerar que los siguientes APIs **NO son seguros**:

- Los métodos de **Query** sobre Connection.
- El API **QueryBuilder**.
- Los APIs Platform y SchemaManager para generer y ejeceutar sentencias SQL de DML/DDL.

> Se recomienda la lectura de la [documentación de Doctrine sobre seguridad][2].

A continuación se presenta algunos ejemplos de la manera incorrecta y correcta de manejar las entradas del usuario.

## Incorrecto: Concatenación de Strings ##

Nunca jamás se debe construir consultas dinámicamente y concatenar la entrada del usuario en el SQL o DQL. Por ejemplo :

```php
<?php
// Muy incorrecto!
$sql = "SELECT * FROM users WHERE name = '" . $username . "'";
```
Un atacante podría inyectar cualquier valor en la variable $username para modificar la consulta según su antojo.

A pesar de que DQL es una envoltura alrededor de SQL que puede prevenir algunas implicaciones de seguridad, el ejemplo anterior también es una amenaza contra consutlas DQL.

```php
<?php
// DQL tampoco es seguro contra la entrada del usuario:
$dql = "SELECT u FROM User u WHERE u.username = '" . $username . "'";
```

En este escenario un atacante podría todavía definir un valor de `"'OR 1=1"` y crear una consulta DQL válida.

A pesar de que DQL hará uso de funciones de control de comillas al usar literales en una sentencia DQL, permitir al atacante modificar la sentencia DQL con literales válidos no puede ser detectado por el analizador DQL, esto es **responsabilidad del desarrollador**.

## Correcto: Sentencias preparadas ##

Siempre se debe usar sentencias preparadas para ejecutar las consultas. Una sentencia preparada es un procedimiento de dos pasos, que separa la consulta SQL de los parámetros.

En lugar de usar concatenación de strings para insertar la entrada del usuario en las sentencias SQL/DQL, solo se especifica en su lugar marcadores posicionales (usando signos de interrogación) o nombrados (`:param1`, `:foo`, `:bar`), y luego se indica qué variable debe asociarse a qué marcador.

```php
<?php
// SQL Prepared Statements: Positional
$sql = "SELECT * FROM users WHERE username = ?";
$stmt = $connection->prepare($sql);
$stmt->bindValue(1, $username);
$stmt->execute();

// SQL Prepared Statements: Named
$sql = "SELECT * FROM users WHERE username = :user";
$stmt = $connection->prepare($sql);
$stmt->bindValue("user", $username);
$stmt->execute();

// DQL Prepared Statements: Positional
$dql = "SELECT u FROM User u WHERE u.username = ?1";
$query = $em->createQuery($dql);
$query->setParameter(1, $username);
$data = $query->getResult();

// DQL Prepared Statements: Named
$dql = "SELECT u FROM User u WHERE u.username = :name";
$query = $em->createQuery($dql);
$query->setParameter("name", $username);
$data = $query->getResult();
```

Esto puede ser un poco más tedioso de escribir, pero es la única manera de escribir consultas seguras.

### API QueryBuilder ###

En cuanto al API **QueryBuilder**, es importante entender que como se permite expresiones SQL en casi todas las cláusulas y posiciones, solo se puede prevenir inyecciones SQL en los métodos `setFirstResult()` y `setMaxResults()`.

Todos los demás métodos son incapaces de distinguir entre entrada del usuario y entrada del desarrollador, y por lo tanto están sujetos a la posibilidad de inyección SQL.

Para trabajar con seguridad con el API QueryBuilder, **NUNCA** se debe pasar entradas del usuario a ninguno de los métodos de QueryBuilder. Al contrario, se debe usar la sintaxis de marcadores posicionales `?` o de `:nombre` en combinación con `$queryBuilder->setParameter($marcador, $valor)`.

```php
<?php
// Positional
$queryBuilder
    ->select('u.id', 'u.name')
    ->from('users', 'u')
    ->where('u.email = ?')
    ->setParameter(0, $userInputEmail)
;
// Named
$queryBuilder
    ->select('u.id', 'u.name')
    ->from('users', 'u')
    ->where('u.email = :email')
    ->setParameter('email', $userInputEmail)
;
```

> Se recomienda la lectura de la [documentación de Doctrine sobre QueryBuilder][3].

  [1]: http://www.php.net/manual/en/security.database.sql-injection.php
  [2]: http://doctrine-dbal.readthedocs.org/en/latest/reference/security.html
  [3]: http://doctrine-dbal.readthedocs.org/en/latest/reference/query-builder.html