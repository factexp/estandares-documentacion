# Queries Dinámicos en Symfony #

En ciertas opciones de las aplicaciones es necesario construir queries dinámicamente, en base a los filtros proporcionados por el usuario. Como de costumbre, los queries se definen en métodos dentro de los repositorios.

A continuación se presenta la manera recomendada de construir estos queries DQL/SQL, evitando inyecciones SQL.


## createQuery ##

**Inyección SQL - Incorrecto**

Si ya se ha revisado el manual sobre [Inyección SQL](../blob/master/Symfony/Inyeccion-SQL.md), se esperaría que el desarrollador no caiga en el siguiente **error de inyección SQL** al utilizar el método createQuery del entity manager:

```php
<?php

// INYECCION SQL - MUY INCORRECTO!
public function findByFilters($code, $category, $dateStart, $dateEnd)
{
    // definir string dql basico
    $dql = "SELECT rec
            FROM schemaBundle:InfoRecord rec
	        WHERE rec.code = $code ";

    // agregar condiciones opcionales
    if ($category != "")
    {
        $dql .= " AND rec.category = $category";
    }
    if ($dateStart != "" && $dateEnd != "")
    {
        $dql .= " AND rec.creation BETWEEN $dateStart AND $dateEnd";
    }

    // crear y ejecutar query con string dql dinamico - INYECTADO!
    $query = $this->_em->createQuery($dql);
    $datos = $query->getResult();
    return $datos;
}
```

**Doble trabajo**

La siguiente es una opción **correcta**, que evita la inyección SQL, pero que obliga al desarrollador a verificar los filtros dos veces, una para construir el string SQL y otra para definir los parámetros, lo cual hace que el código sea **menos entendible y mantenible**:

```php
<?php

// DOBLE TRABAJO!
public function findByFilters($code, $category, $dateStart, $dateEnd)
{
    // definir string dql basico
    $dql = "SELECT rec
            FROM schemaBundle:InfoRecord rec
	        WHERE rec.code = :code ";

    // agregar condiciones opcionales
    if ($category != "")
    {
        $dql .= " AND rec.category = :category";
    }
    if ($dateStart != "" && $dateEnd != "")
    {
        $dql .= " AND rec.creation BETWEEN :dateStart AND :dateEnd";
    }

    // crear query con string dql dinamico
    $query = $this->_em->createQuery($dql);

    // agregar parametros basicos al query
    $query->setParameter("code", $code);

    // agregar parametros para las condiciones opcionales
    if ($category != "")
    {
        $query->setParameter("category", $category);
    }
    if ($dateStart != "" && $dateEnd != "")
    {
        $query->setParameter("dateStart", $dateStart);
        $query->setParameter("dateEnd", $dateEnd);
    }

    // ejecutar el query
    $datos = $query->getResult();
    return $datos;
}
```

**Correcto**

Lo **recomendado** es crear el objeto `$query` sin DQL, contruir el string DQL y definir los parámetros según sea necesario, aplicar el DQL y ejecutar:

```php
<?php

public function findByCodeAndCategoryAndCreation($code, $category, $dateStart, $dateEnd)
{
    // crear query sin string dql
    $query = $this->_em->createQuery();

    // definir string dql basico
    $dql = "SELECT rec
            FROM schemaBundle:InfoRecord rec
	        WHERE rec.code = :code ";

    // agregar parametros basicos al query
    $query->setParameter("code", $code);

    // agregar condiciones opcionales - con sus parametros
    if ($category != "")
    {
        $dql .= " AND rec.category = :category";
        $query->setParameter("category", $category);
    }
    if ($dateStart != "" && $dateEnd != "")
    {
        $dql .= " AND rec.creation BETWEEN :dateStart AND :dateEnd";
        $query->setParameter("dateStart", $dateStart);
        $query->setParameter("dateEnd", $dateEnd);
    }

    // aplicar string dql al query y ejecutar
    $query->setDQL($dql);
    $datos = $query->getResult();
    return $datos;
}
```


## createQueryBuilder ##

    // TODO


## createNativeQuery ##

    // TODO

