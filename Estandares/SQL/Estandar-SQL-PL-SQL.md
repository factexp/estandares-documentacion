# Estándares SQL y PL SQL

> **Introducción** 
> El objetivo del presente documento es el establecimiento de estándares o convenciones de programación y
> creación de objetos con _SQL_ y _PL/SQL_, empleados en el desarrollo
> de software en __TELCONET__.

## 1 SQL
SQL (Structured Query Language) es un lenguaje declarativo de acceso a bases de datos relacionales que permite especificar diversos tipos de operaciones en ellas. Una de sus características es el manejo del álgebra y el cálculo relacional que permiten efectuar consultas con el fin de recuperar, de forma sencilla, información de bases de datos, así como hacer cambios en ellas. ([SQL](https://es.wikipedia.org/wiki/SQL)).
### 1.1 DCL
DLC (Data control language) es una sintaxis similar a un lenguaje de programación utilizado para controlar el acceso a los datos almacenados en una base de datos (autorización). En particular, se trata de un componente de _SQL_. ([DCL](https://en.wikipedia.org/wiki/Data_control_language)).

**_Versionamiento para scripts con sentencias DCL_**   
Agregar en un solo archivo sql todas las sentencias DCL que se deban ejecutar por desarrollo, respetando el orden de ejecución de las mismas y anteponiendo los esquemas dueños de objetos utilizados en el script.

Versionar los cambios realizados en la ubicación:

    src/telconet/[MODULO]/Resources/sql/[DCL]/

ejemplo:

    src/telconet/financieroBundle/Resources/sql/DCL/

**_Estándares para  nombre de archivos_**   

Anteponer prefijo DCL al nombre del archivo, seguido del número de solicitud, login del programador y la extensión debe ser .SQL.

    [DCL]_[#Solicitud]_[login].SQL

En caso de existir un orden de ejecución específico, agregar al final del nombre del script el número que indique su orden de ejecución.

    [DCL]_[#Solicitud]_[login]_[Orden].SQL

ejemplo:
     
    DCL_999_acarias_1.SQL
    DCL_999_acarias_2.SQL

**_Estándares para sentencias DCL_**   
Anteponer el esquema dueño del objeto a todos los objetos enviados en el archivo.
    
    GRANT PRIVILEGES ON [ESQUEMA.]OBJECTO TO [ESQUEMA];
    REVOKE PRIVILEGES ON [ESQUEMA.]OBJECTO FROM [ESQUEMA];
    
 ejemplo: 
 
    GRANT SELECT ON DB_GENERAL.ADMI_PARAMETRO_CAB TO DB_INFRAESTRUCTURA;
    GRANT EXECUTE ON DB_COMERCIAL.COMEK_CONSULTAS TO DB_GENERAL;
    REVOKE EXECUTE ON DB_COMERCIAL.COMEK_CONSULTAS TO DB_GENERAL;


### 1.2 DDL
DDL (Data Definition Language) se encarga de la modificación de la estructura de los objetos de la base de datos. Oracle implícitamente confirma la transacción actual antes y después de cada instrucción DDL.

**_Versionamiento para scripts con sentencias DDL_**   
Agregar en un solo archivo sql todas las sentencias DDL que se deban ejecutar por desarrollo, respetando el orden de ejecución de las mismas y anteponiendo los esquemas dueños de objetos utilizados en el script.

Versionar los cambios realizados en la ubicación:

    src/telconet/[MODULO]/Resources/sql/[DDL]

ejemplo:

    src/telconet/financieroBundle/Resources/sql/DDL


**_Estándares para nombre de archivos_**   
Anteponer prefijo DDL al nombre del archivo, seguido del número de solicitud, login del programador y la extensión debe ser .SQL.

    [DDL]_[#Solicitud]_[login].SQL

En caso de existir un orden de ejecución específico, agregar al final del nombre del script el número que indique su orden de ejecución.

    [DDL]_[#Solicitud]_[login]_[Orden].SQL

ejemplo:
     
    DDL_999_acarias.SQL
    DDL_999_acarias_1.SQL


**_Estándares para  sentencias DDL_**   
Anteponer el esquema dueño del objeto a todos los objetos enviados en el archivo.
    
    ALTER TABLE [ESQUEMA.]OBJECTO ADD (...);
    CREATE [OR REPLACE] SYNONYM [ESQUEMA.]NOMBRE_SINONIMO FOR [ESQUEMA.]OBJETO;

    
 ejemplo: 
 
    ALTER TABLE DB_INFRAESTRUCTURA.INFO_PROCESO_MASIVO_CAB ADD MENSAJE VARCHAR2(500);
    CREATE SYNONYM DB_GENERAL.INFO_PUNTO FOR DB_COMERCIAL.INFO_PUNTO;

### 1.3 DML
DML (Data Manipulation Language) permite manipular los datos en objetos de esquema existentes. Estas declaraciones no confirman la transacción de forma implícitan en la transacción actual.

**_Versionamiento para scripts con sentencias DML_**   
Agregar en un solo archivo sql todas las sentencias DML que se deban ejecutar por desarrollo, respetando el orden de ejecución de las mismas y anteponiendo los esquemas dueños de objetos utilizados en el script.
Versionar los cambios realizados en la ubicación:

    src/telconet/[MODULO]/Resources/sql/[DML]

ejemplo:

    src/telconet/financieroBundle/Resources/sql/DML



**_Estándares para nombre de archivos_**   
Anteponer prefijo DML al nombre del archivo, seguido del número de solicitud, login del programador y la extensión debe ser .SQL.

    [DML]_[#Solicitud]_[login].SQL

En caso de existir un orden de ejecución específico, agregar al final del nombre del script el número que indique su orden de ejecución.

    [DML]_[#Solicitud]_[login]_[Orden].SQL

ejemplo:
     
    DML_999_acarias.SQL
    DML_999_acarias_1.SQL
---

**_Estándares para sentencias DML_**   
Anteponer el esquema dueño del objeto a todos los objetos enviados en el archivo.
    
    INSERT INTO [ESQUEMA.]OBJETO (...) VALUES (...) ;
    DELETE [ESQUEMA.]OBJETO WHERE (...);
 ejemplo: 
    
    INSERT INTO DB_GENERAL.ADMI_PARAMETRO_CAB(...) VALUES (...);
    DELETE DB_INFRAESTRUCTURA.INFO_PROCESO_MASIVO_DET WHERE (...);


---

### 1.4 Estándares para objetos de bases de datos
Se debe escribir el nombre del objeto de base de datos en singular, en mayúsculas y las palabras deben ser separadas por "_"<smal>(guión bajo)</small>.

> **Formato:** [PREFIJO_][PALABRA1_][PALABRA2]

**TABLAS, VISTAS, INDICES, SECUENCIAS, PK, FK.**

|        OBJETO       |   PREFIJO   |               EJEMPLO               |
|:-------------------:|:-----------:|:-----------------------------------:|
| TABLA               | ADMI / INFO | ADMI_FORMATO / INFO_DETALLE_FORMATO |
| TRIGGER             |    AFTER    | AFTER_ADMI_FORMATO                  |
| TRIGGER             |    BEFORE   | BEFORE_ADMI_FORMATO                 |
| VISTA               |      V      | V_HILO_DISPONIBLE                   |
| VISTA MATERIALIZADA |      VM     | VM_SCOPE                            |
| PRIMARY KEY         |      PK     | PK_ID_FORMATO                       |
| FOREIGN KEY         |      FK     | FK_FORMATO_ID                       |
| INDEX               |     IDX     | IDX_FORMATO_ID                      |
| SECUENCIA           |     SEQ     | SEQ_ADMI_FORMATO                    |


**PAQUETES**


|  OBJETO |     ESQUEMA     | PREFIJO |    EJEMPLO    |
|:-------:|:---------------:|:-------:|:-------------:|
| PACKAGE | INFRAESTRUCTURA |   INKG  | INKG_CONSULTA |
| PACKAGE |    FINANCIERO   |   FNKG  | FNKG_CONSULTA |
| PACKAGE |    COMERCIAL    |   CMKG  | CMKG_CONSULTA |
| PACKAGE |     SOPORTE     |   SPKG  | SPKG_CONSULTA |
| PACKAGE |    SEGURIDAD    |   SGKG  | SGKG_CONSULTA |
| PACKAGE |     GENERAL     |   GNKG  | GNKG_CONSULTA |
| PACKAGE |   COMUNICACION  |   CUKG  | CUKG_CONSULTA |

**FUNCIONES, PROCEDIMIENTOS Y CURSORES**

|   OBJETO  | PREFIJO |          EJEMPLO         |
|:---------:|:-------:|:------------------------:|
| PROCEDURE |    P    | P_PROCESA_PAGO           |
| FUNCTION  |    F    | F_MAX_HISTORIAL_SERVICIO |
| CURSOR    |    C    | C_GetServicio            |

### 1.5 Documentación y comentarios

**PACKAGE**   
La documentación para paquetes sólo se la hará en la especificación del mismo, en el body no es necesario.   
Tanto la declaracion como el body es necesarios terminar con un (/).   

```sql
PACKAGE INKG_CONSULTA
AS
/**
 * Documentación para INKG_CONSULTA
 * Paquete que contiene procesos y funciones de 
 * tipo consulta
 * 
 * @author Nombre Apellido <correo@mail.com>
 * @version 1.0 DD/MM/YYYY
 */
END INKG_CONSULTA;
/
```

**PROCEDURE y FUNCTION**   
La documentación para procedimientos y funciones solo se la hará en la especificación del paquete, en el body no es necesario.   
Tanto la declaracion como el body es necesarios terminar con un (/).   

```sql
PACKAGE INKG_CONSULTA
AS
/**
 * (...)
 */

/**
 * Documentación para F_GET_CORREO_BY_PUNTO
 * Función que obtiene los correos de un punto
 * 
 * @author Nombre Apellido <correo@mail.com>
 * @version 1.0 DD/MM/YYYY
 * 
 * @param Fn_IdPunto IN DATATYPE Recibe el ID del punto
 * @return DATATYPE Rertorna correos concatenados por ";"
 */
FUNCTION F_GET_CORREO_BY_PUNTO(Fn_IdPunto IN DATATYPE) RETURN DATATYPE;

/**
 * Documentación para P_CALCULA_METRAJE
 * Procedimiento que realiza el cálculo de metraje
 * 
 * @author Nombre Apellido <correo@mail.com>
 * @version 1.0 DD/MM/YYYY
 * 
 * @param Pf_CoordenadaPunto    IN FLOAT Recibe la coordenada del punto
 * @param Pf_CoordenadaElemento IN FLOAT Recibe la coordenada del elemento
 * @param Pf_Metraje           OUT FLOAT Devuelve el metraje
 */
PROCEDURE P_CALCULA_METRAJE(Pf_CoordenadaPunto    IN FLOAT, 
			                Pf_CoordenadaElemento IN FLOAT,
			                Pf_Metraje           OUT FLOAT);

END INKG_CONSULTA;
/

PACKAGE BODY INKG_CONSULTA
AS
(...)
END INKG_CONSULTA;
/
```
**TABLAS Y VISTAS**

```sql
COMMENT ON TABLE [esquema.]tabla IS 'comentario';
COMMENT ON TABLE [esquema.]vista IS 'comentario';
COMMENT ON TABLE [esquema.]vista_materilizada IS 'comentario';
```

**COLUMNAS**

```sql
COMMENT ON COLUMN [esquema.]tabla.columna IS 'comentario';
COMMENT ON COLUMN [esquema.]vista.columna IS 'comentario';
COMMENT ON COLUMN [esquema.]vista_materilizada.columna IS 'comentario';
```

Ejemplo de tabla y columna:

```sql
CREATE TABLE DB_SOPORTE.INFO_TAREA_CARACTERISTICA (
    ID_TAREA_CARACTERISTICA NUMBER,
    TAREA_ID                NUMBER,
    DETALLE_ID              NUMBER,
    CARACTERISTICA_ID       NUMBER,
    VALOR                   VARCHAR2(200),
    FE_CREACION             TIMESTAMP,
    USR_CREACION            VARCHAR2(20),
    IP_CREACION             VARCHAR2(30),
    FE_MODIFICACION         TIMESTAMP,
    USR_MODIFICACION        VARCHAR2(20),
    IP_MODIFICACION         VARCHAR2(30),
    ESTADO                  VARCHAR2(20)
);

--COMENTARIO TABLA
COMMENT ON TABLE DB_SOPORTE.DB_SOPORTE.INFO_TAREA_CARACTERISTICA IS 'TABLA PARA ALMACENAR DE FORMA DINAMICA LOS NUEVOS REGISTROS DE CAMPOS DE LAS TAREAS';
--COMENTARIOS COLUMNAS
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.ID_TAREA_CARACTERISTICA	IS 'ID DE LA TABLA';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.TAREA_ID                 IS 'ID DE LA TAREA';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.DETALLE_ID               IS 'ID DEL DETALLE DE LA TAREA';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.CARACTERISTICA_ID        IS 'ID DE LA CARACTERISTICA';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.VALOR                    IS 'VALOR DE LA CARACTERISTICA';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.FE_CREACION              IS 'FECHA DE CREACION';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.USR_CREACION             IS 'USUARIO DE CREACION';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.IP_CREACION              IS 'IP DE CREACION';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.FE_MODIFICACION          IS 'FECHA DE MODIFICACION';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.USR_MODIFICACION         IS 'USUARIO DE MODIFICACION';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.IP_MODIFICACION          IS 'IP DE MODIFICACION';
COMMENT ON COLUMN DB_SOPORTE.INFO_TAREA_CARACTERISTICA.ESTADO                   IS 'ESTADO DE LA CARACTERISTICA DE LA TAREA';
```
**DDL**
Todos los DDL son necesarios terminar con un (/).   

```sql
-- CREACION DEL SECUENCIAL PARA LA TABLA INFO_TAREA_CARACTERISTICA
CREATE SEQUENCE DB_SOPORTE.SEQ_INFO_TAREA_CARACTERISTICA MINVALUE 1 MAXVALUE 9999999999999999999999999999
INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  CYCLE;
```

**DML**
Todos los DML son necesarios terminar con un (/).   

```sql
--CREACION DE LA CARACTERISTICA ATENDER ANTES PARA EL SISTEMA HAL
INSERT INTO DB_COMERCIAL.ADMI_CARACTERISTICA (
    ID_CARACTERISTICA,
    DESCRIPCION_CARACTERISTICA,
    TIPO_INGRESO,
    FE_CREACION,
    USR_CREACION,
    TIPO,
    ESTADO
) VALUES (
    DB_COMERCIAL.SEQ_ADMI_CARACTERISTICA.NEXTVAL,
    'ATENDER_ANTES',
    'T',
     SYSDATE,
    'gvalenzuela',
    'SOPORTE',
    'Activo'
);

COMMIT;
```

**DCL**
Todos los DCL son necesarios terminar con un (/).   

```sql
--PERMISOS PARA CONSULTAR INFORMACION DEL LOS MATERIALES EN EL NAF
GRANT SELECT ON  NAF47_TNET.ARIN_CARACTERISTICA TO DB_SOPORTE;
GRANT SELECT ON  NAF47_TNET.ARIN_CARACTERISTICA_ARTICULO TO DB_SOPORTE;
GRANT SELECT ON  NAF47_TNET.ARINDA TO DB_SOPORTE;
GRANT SELECT ON  NAF47_TNET.ARAF_CONTROL_CUSTODIO TO DB_SOPORTE; 
```

### 1.6 Longitud máxima de la línea
La línea de código no puede superar las 150 caracteres de longitud.

### 1.7 Indentación
Espacio de identación entre niveles debe ser de 2 espacios.

```sql
DECLARE
  --
  CURSOR C_GetParametro(Cv_..., Cn_...)
  IS
    SELECT APD.VALOR1,
      APD.VALOR2,
      APD.VALOR3,
      APD.VALOR4
    FROM DB_GENERAL.ADMI_PARAMETRO_CAB APC,
      DB_GENERAL.ADMI_PARAMETRO_DET APD
    WHERE APC.ID_PARAMETRO   = APD.PARAMETRO_ID
    AND APC.ESTADO           = NVL(Cv_EstadoParametroCab, APC.ESTADO )
    AND APD.ESTADO           = NVL(Cv_EstadoParametroDet, APD.ESTADO )
    AND APC.NOMBRE_PARAMETRO = NVL(Cv_NombreParamCab, APC.NOMBRE_PARAMETRO )
    AND APD.VALOR1           = NVL(Cv_Valor1, APD.VALOR1 )
    AND APD.VALOR2           = NVL(Cv_Valor2, APD.VALOR2 )
    AND APD.VALOR3           = NVL(Cv_Valor3, APD.VALOR3)
    AND APD.VALOR4           = NVL(Cv_Valor4, APD.VALOR4);
  --
  Ln_IdElemento NUMBER := 0;
  --
BEGIN
  --
  IF 0 = 0 THEN
    --
    NULL;
    --
  END IF;
  --
  NULL;
  --
EXCEPTION
WHEN OTHERS THEN
  --
  SQLERRM;
  --
END;
```

### 1.8 Alineación
Espacio de alineación entre niveles debe ser de 2 espacios, ejemplo:   

```sql
SELECT APD.VALOR1,
  APD.VALOR2,
  APD.VALOR3,
  APD.VALOR4
FROM DB_GENERAL.ADMI_PARAMETRO_CAB APC,
  DB_GENERAL.ADMI_PARAMETRO_DET APD
WHERE APC.ID_PARAMETRO   = APD.PARAMETRO_ID
AND APC.ESTADO           = NVL(Fv_EstadoParametroCab, APC.ESTADO )
AND APD.ESTADO           = NVL(Fv_EstadoParametroDet, APD.ESTADO )
AND APC.NOMBRE_PARAMETRO = NVL(Fv_NombreParameteroCab, APC.NOMBRE_PARAMETRO )
AND APD.VALOR1           = NVL(Fv_Valor1, APD.VALOR1 )
AND APD.VALOR2           = NVL(Fv_Valor2, APD.VALOR2 )
AND APD.VALOR3           = NVL(Fv_Valor3, APD.VALOR3)
AND APD.VALOR4           = NVL(Fv_Valor4, APD.VALOR4);
```

## 2 PL/SQL  
PL / SQL (Procedural Language/Structured Query Language) es un lenguaje de procedimientos diseñados específicamente para abarcar las sentencias SQL dentro de su sintaxis . PL / SQL unidades de programa son compilados por el servidor de base de datos Oracle y se almacenan en la base de datos . Y en tiempo de ejecución , tanto de PL / SQL y SQL se ejecutan dentro del mismo proceso de servidor , con lo que la eficiencia óptima . PL / SQL hereda automáticamente la solidez , seguridad y portabilidad de la base de datos Oracle ([PL/SQL](http://www.oracle.com/technetwork/database/features/plsql/index.html)).

### 2.1 Formato de variables
El nombre de la variable debe empezar con un prefijo según el contexto donde será usada y la primera letra del tipo de variable en minúscula.
La primera letra o las letras despues del simbolo "_"<small>(guión bajo)</small> iran en mayúscula.

**Declaración de variables en bloques anónimos, procedimientos y funciones**

| PREFJIO |        TIPO       |         EJEMPLO        |
|:-------:|:-----------------:|:----------------------:|
|    L    |      VARCHAR2     | Lv_NombreElemeneto     |
|    L    |       NUMBER      | Ln_IdElemento          |
|    L    |  ROWTYPE / RECORD | Lr_Elemento            |
|    L    |     EXCEPTION     | Le_FalloInsertElemento |
|    L    |        DATE       | Ld_FeCreacion          |
|    L    |     TIMESTAMP     | Lt_FeCreacion          |
|    L    | CONSTANT VARCHAR2 | LCv_Estado             |
|    L    |  CONSTANT NUMBER  | LCn_LimiteIteracion    |
|    L    |   SYS_REFCURSOR   | Lrf_Elemento           |
|    L    |        CLOB       | Lcl_Observacion        |
|    L    |      XMLTYPE      | Lxml_Documento         |
|    L    |       ROWID       | Lrw_InfoElemento       |
|    L    |      BOOLEAN      | Lb_EsEdificio          |
|    L    |       FLOAT       | Lf_ValorPago           |
|    Lc   |      CURSOR       | Lc_Nombre              |

**Parametros de un procedimiento**

| PREFJIO |        TIPO       |         EJEMPLO        |
|:-------:|:-----------------:|:----------------------:|
|     P   |      VARCHAR2     | Pv_NombreElemeneto     |
|     P   |       NUMBER      | Pn_IdElemento          |
|     P   |  ROWTYPE / RECORD | Pr_Elemento            |
|     P   |        DATE       | Pd_FeCreacion          |
|     P   |     TIMESTAMP     | Pt_FeCreacion          |
|     P   |   SYS_REFCURSOR   | Prf_Elemento           |
|     P   |        CLOB       | Pcl_Observacion        |
|     P   |      XMLTYPE      | Pxml_Documento         |
|     P   |       ROWID       | Prw_InfoElemento       |
|     P   |      BOOLEAN      | Pb_EsEdificio          |
|     P   |       FLOAT       | Pf_ValorPago           |

**Parametros de una función**

| PREFJIO |        TIPO       |         EJEMPLO        |
|:-------:|:-----------------:|:----------------------:|
|    F    |      VARCHAR2     | Fv_NombreElemeneto     |
|    F    |       NUMBER      | Fn_IdElemento          |
|    F    |  ROWTYPE / RECORD | Fr_Elemento            |
|    F    |        DATE       | Fd_FeCreacion          |
|    F    |     TIMESTAMP     | Ft_FeCreacion          |
|    F    |   SYS_REFCURSOR   | Frf_Elemento           |
|    F    |        CLOB       | Fcl_Observacion        |
|    F    |      XMLTYPE      | Fxml_Documento         |
|    F    |       ROWID       | Frw_InfoElemento       |
|    F    |      BOOLEAN      | Fb_EsEdificio          |
|    F    |       FLOAT       | Ff_ValorPago           |

**Declaración de variables dentro de un paquete**

| PREFJIO |        TIPO       |         EJEMPLO        |
|:-------:|:-----------------:|:----------------------:|
|    L    |      DATATYPE     | L[]_Nombre             |

**Parametro de cursor**

| PREFJIO |        TIPO       |         EJEMPLO        |
|:-------:|:-----------------:|:----------------------:|
|    C    |      DATATYPE     | C[]_Nombre             |


---

### 2.2 Cursores 
Palabras reservadas deben ir en mayúscula.   

```sql
DECLARE
(...)
  --
  CURSOR C_GetInfoElemento(Cn_IdElemento INFO_ELEMENTO.ID_ELEMENTO%TYPE)
  IS
    SELECT IE.ID_ELEMENTO
    FROM INFO_ELEMENTO IE
    WHERE IE.ID_ELEMENTO = Cn_IdElemento;
  --
(...)
BEGIN
  --
  IF C_GetInfoElemento%ISOPEN THEN
    CLOSE C_GetInfoElemento;
  END IF;
  --
  OPEN C_GetInfoElemento(...);
  --
  (...)
END;
```

### 2.0.3 Estructura de bloque anónimo
Palabras reservadas deben ir en mayúscula.   

```sql
SET SERVEROUTPUT ON --Debe ser incluido cuando se ejecute en el servidor
SPOOL NOMBRE_ARCHIVO.log; --Incluir cuando se requiera escribir log
[DECLARE]
/* Aquí define objetos que serán utilizados dentro del mismo bloque. */
BEGIN
/* Sentencias ejecutables */
[EXCEPTION]
/* Captura de errores */
[DBMS_OUTPUT.PUT_LINE(SQLERRM);]
END;
/
SPOOL OFF; --Incluir si declara SPOOL
/
```

### 2.4 Estructura de un procedimiento
Palabras reservadas deben ir en mayúscula.   

```sql
PROCEDURE P_PROCESA_PAGO
IS
  (...)
BEGIN
  (...)
EXCEPTION
WHEN OTHERS THEN
  (...)
  DB_GENERAL.GNRLPCK_UTIL.INSERT_ERROR(Pv_Aplicacion, 
				       Pv_Proceso, 
				       Pv_DetalleError, 
				       Pv_UsrCreacion, 
				       Pd_FeCreacion, 
				       Pv_IpCreacion);
END P_PROCESA_PAGO;
```

---

### 2.5 Estructura de una función**   
Palabras reservadas deben ir en mayúscula.   

```sql
FUNCTION F_GET_VALOR_PAGO
  RETURN DATATYPE
IS
  (...)
BEGIN
  (...)
EXCEPTION
WHEN OTHERS THEN
  (...)
  DB_GENERAL.GNRLPCK_UTIL.INSERT_ERROR(Pv_Aplicacion, 
				       Pv_Proceso, 
				       Pv_DetalleError, 
				       Pv_UsrCreacion, 
				       Pd_FeCreacion, 
				       Pv_IpCreacion);
END F_GET_VALOR_PAGO;
```

## 3. Políticas:
1. Se deben declarar las variables con los atributos **%TYPE** y **%ROWTYPE**, ejemplo:   
2. Ln_IdElemento **INFO_ELEMENTO.ID_ELEMENTO%TYPE;**   
3. Lr_Elemento    **INFO_ELEMENTO%ROWTYPE;**
4. Todo procedimiento o función debe ir dentro de un **PACKAGE.**
5. Todo procedimiento o función debe realizar el **control de excepciones.**
6. Se debe segmentar un paquete para contener los tipos de variables y puedan ser reusadas. 
Ejemplo: [PACKAGE DB_FINANCIERO.FNKG_TYPES](http://gitlab.telconet.ec/telcos/telcos/blob/master/src/telconet/financieroBundle/Resources/sql/FNKG_TYPES.sql)
7.  Se debe segmentar un paquete para realizar consultas y un paquete para realizar transacciones.
