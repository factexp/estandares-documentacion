## Comentarios de documentación

Los comentarios de documentación son comentarios multilínea con un formato especial y con palabras clave interpretadas por el generador de la documentación.

Estos comentarios se utilizan para la generación de la documentación del API del código fuente, tanto pública (hacia otros programadores) como privada (hacia los programadores de la aplicación).

Los comentarios de documentación comienzan por la apertura de un comentario multilínea seguido de un asterisco. Cada nueva línea del comentario debe ser precedida por un asterisco y su indentación debe ser la misma que el primer asterisco (no la barra oblícua) de la línea de apertura del comentario. El final de la línea debe estar indentada a la misma altura que las líneas intermedias.

Este tipo de comentario es igual que el comentario multilínea mencionado anteriormente añadiéndo un asterisco a la primera línea:

```php
<?php

/**
 * Comentario de documentación de clases y funciones.
 *
 * Estos comentarios pueden llevar etiquetas especiales
 * con significado dentro de la documentación general:
 *
 * @author Kenneth Jiménez <kjimenez@telconet.ec>
 * @version 1.0 15-05-2014
 */
```

Los comentarios de documentación deben comentar las clases, los métodos y las funciones. Además, es recomendable comentar las variables de las clases.

Dentro de los comentarios de documentación, como se ha podido comprobar en el ejemplo anterior, pueden (y deben) existir etiquetas especiales que son interpretadas por el generador de documentación. Todas estas etiquetas comienzan por @ (arroba) y pueden ser las siguientes:

**@author:** Indica el autor de la clase, función, etc.
**@copyright:** Indica la información del copyright de un archivo o clase.
**@deprecated:** Indica que un elemento es antiguo y no se debe usar.
**@example:** Indica un archivo de ejemplo.
**@global:** Documenta una variable global o su uso.
**@internal:** Indica que es documentación interna.
**@license:** Indica la licencia del archivo o clase.
**@link:** Indica un enlace a otro elemento de documentación.
**@method:** Indica la implementación de un método “mágico” de la clase (PHP).
**@param:** En una función indica el tipo de parámetro y sus posibles valores.
**@property:** Indica la implementación de una propiedad “mágica” de una clase (PHP).
**@return:** En una función indica el tipo de retorno de una función y sus posibles valores.
**@see:** Indica que se debe ver además otro elemento de la documentación.
**@since:** Indica desde qué versión está disponible este elemento.
**@throws:** En una función indica el tipo de excepciones que puede lanzar.
**@todo:** Indica cambios futuros en el código.
**@var:** Documenta una variable de una clase.
**@version:** Indica la versión de la clase, archivo o función.

### Terminos usados

- **Camel Case**

    Es la práctica de escribir frases o palabras compuestas eliminando los espacios y poniendo en mayúscula la primera letra de cada palabra.

- **Lower Camel Case**

    Similar al Camel Case sólo que la primera letra de la primera palabra es también en minúscula. Por ejemplo, la frase "generar datos aleatorios" pasada a lowerCamelCase sería "generarDatosAleatorios".

- **Upper Camel Case**

    Similar al Camel Case siendo la primera letra de la primera palabra en mayúscula. Por ejemplo, la frase "generar datos aleatorios" pasada a UpperCamelCase sería "GenerarDatosAleatorios".

### Clases

Los nombres de las clases deben estar escritos en **UpperCamelCase**. La indentación de las mismas debe ser de 4 espacios con respecto al margen izquierdo mientras que los campos y los métodos deben estar indentados de la misma forma con respecto a la clase.

La implementación de los métodos deben estar indentadas con lo ya establecido respecto al nombre del método. El resto de estilos es similar al de las funciones normales.

Dentro de las clases, los métodos deben estar separados por un retorno de carro entre sí y por un retorno de carro tanto del inicio y final de la clase como de las propiedades.

Las propiedades similares pueden estar agrupadas (sin separar con retornos de carro) pero no en la misma línea, mientras que las propiedades que no tengan relación deben estar separadas por un retorno de carro igual que los métodos.

Si un método o propiedad tiene documentación, se aplica la misma normal de separación.

Tanto la clase como los métodos y propiedades deben estar documentados. Así mismo, en la clase deben existir los elementos **@author** y **@version** de la documentación aunque se pueden incluir más como @internal, @since, etc.

En caso de que un clase extienda otra o implemente una interfaz, la extensión debe estar en la misma línea que el nombre de la clase.

Los modificadores de clase o de método, como abstract, static, etc., también deben estar en la misma línea que el nombre que modifican.

Los nombres de las interfaces, por convención, deben comenzar por "I" mayúscula, por ejemplo: IConfig.

```php
<?php

	/**
	 * Documentación para la clase 'MiClaseDeEjemplo'.
	 *
	 * Esta es la descripcion de lo que es 'MiClaseDeEjemplo'
	 *
	 * @author Kenneth Jimenez <kjimenez@telconet.ec>
	 * @version 1.0 15-05-2014
	 */
	class MiClaseDeEjemplo extends MiClaseBase implements MiInterface {

		private static $campo1;
		private $campo2;

		static function miMetodo1() {
		}

		/**
		 * Documentación para el método 'miMetodoDos'.
		 *
		 * Esta es una descripcion de lo que realiza 'miMetodoDos'
		 *
		 * @param mixed $param1 El primer parámetro.
		 * @param mixed $param2 El segundo parámetro.
		 *
		 * @return mixed Valor de retorno.
		 *
		 * @author Kenneth Jimenez <kjimenez@telconet.ec>
	     * @version 1.0 15-05-2014
		 */
		function miMetodoDos($param1,$param2) {
		}

	}
?>
```

### Funciones

Los nombres de las funciones serán en **lowerCamelCase**. Los delimitadores del cuerpo de la función, en caso de que sean llaves ({ y }) estarán: el de apertura en la misma línea que la declaración de la función separado por 4 espacios. La llave de cierre debe estar en una línea a parte y no debe haber más código que la propia llave de cierre. Por ejemplo:

```php
<?php

function getUserName($parametros) {
    // código
}
```

En caso de que haya muchos parámetros, estos estarán en múltiples líneas indentados con tabulaciones y espacios hasta coincidir en la misma columna que el primer parámetro. Por ejemplo:

```php
<?php

function getDataFromDB($param1, 
	                   $param2,
	                   $param3,
	                   $param4) {
    // código
}
```

Sólo se permitirán hasta 4 parámetros en las funciones, si estos pasan de este número se deberá pasar un arreglo de parámetros. Por ejemplo:

```php
<?php

function getDataFromDB($arrayParams) {
    $param1 = $arrayParams['param1'];
    $param2 = $arrayParams['param2'];
    $param3 = $arrayParams['param3'];
    $param4 = $arrayParams['param4'];
    .
    .
    $paramN = $arrayParams['paramN'];
    // código
}
```

Los nombres de los parámetros, igual que los nombres de las variables, deben ser nombres concretos del tipo de parámetro en cuestión.

Dentro de la implementación de la función sólo debería haber un return aunque esta regla que se puede omitir siempre y cuando se mejore la legibilidad del código así como el rendimiento.

### Métodos

Los métodos de las clases, al igual que las funciones normales, deben estar escritos en **lowerCamelCase**. La forma de escribir los parámetros y el cuerpo es la misma que para las funciones normales.

Además, a las funciones hay que incorporarles su modificador de acceso aunque, en el caso de que sean públicas, no es necesario incluirlo. Por ejemplo:

```php
<?php

private function getRealName() {
	// código
}
```
