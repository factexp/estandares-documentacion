## Variables

El nombre de las variables serán en **lowerCamelCase**. Las variables usadas en cualquier aplicación deben tener nombres que las identifiquen, es decir, nombres como "a", "b" o "c" no se deberían usar sino que deberían tener un nombre coherente con lo que contienen. En el siguiente ejemplo los nombres de las variables son cortos por lo que no se identifica su significado:

```php
<?php

$a = 0, $b = 0, $c = 0;

if($a > 10 && $b > 20 && $c > 30)
{
	return true;
}
else
{
	return false;
}
```

Este ejemplo debería ser:

```php
<?php

$hora = 0, $minuto = 0, $segundo = 0;

if($hora > 10 && $minuto > 20 && $segundo > 30)
{
	return true;
}
else
{
	return false;
}
```

Sólo en el caso de que la variable sea lo demasiado genérica para no tener un nombre concreto (por ejemplo el índice de un bucle) ésta se declarará con el nombre de "i" siguiendo el orden alfabético en caso de varios bucles anidados ("i", "j", "k",...).

### Prefijo de Variables

Debido a la no existencia de tipos de datos en PHP se seguirán las siguientes reglas para el nombre de las variables.

- Para cuando sea un **string**:

    ```php
    $strMensaje = "Hola mundo";
    ```

- Para cuando sea un **int**:

    ```php
    $intNumero = 1;
    ```

- Para cuando sea un **float**:

    ```php
    $fltNumero = 1;
    ```
        
- Para cuando sea un **boolean**:

    ```php
    $boolBandera = true;
    ```

- Para cuando sea un **Objeto**:

    ```php
    $objEntidad = new Entidad();
    ```
        
- Para cuando sea un **array**:

    ```php
    $arrayParametros = array();
    ```

