## ¿Que es la indentación?

Por indentación se entiende mover un bloque de texto hacia la derecha insertando espacios o tabuladores para separarlo del texto adyacente.

Para dar un ejemplo, veamos un código no indentado, y uno correctamente indentado:

```php
<?php

if( a == b ){
echo "a y b son iguales";
	}
```

El mismo código, pero aplicando indentación:

```php
<?php

if( a == b )
{
    echo "a y b son iguales";
}
```

### Estilo Allman  

Se trata de crear una nueva línea para las llaves, e indentar el código debajo de ellas. La llave de cierre tiene el mismo indentado que la de inicio.

```php
<?php

function saludar( $val )
{
    if( $val == 1 )
    {
        echo "Hola!";
    }
    else
    {
        echo "Chau!";
    }
}
```

Este estilo mantiene un código limpio y claro. Las llaves de inicio y fin coinciden en la misma columna, haciendo más fácil la identificación de cada bloque. Además, es más difícil olvidarse cerrar una llave cuando se identifica claramente la llave inicial.

### ¿Indentar seteo de variables?
Esto también se aplicará para la declaración o asignación de variables.

Antes:

```php
<?php

$nombre_temporal = $_FILES['Filedata']['tmp_name'];
$tamano_de_archivo = $_FILES['Filedata']['size'];
$realname = $_FILES['Filedata']['name'];
```

Despues [Indentado]:

```php
<?php

$nombre_temporal    = $_FILES['Filedata']['tmp_name'];
$tamano_de_archivo  = $_FILES['Filedata']['size'];
$realname           = $_FILES['Filedata']['name'];
```

Sin duda se lee mejor de la segunda manera, queda más limpio, y se distinguen mejor las variables.
También podemos hacer lo mismo con los parametros de funciones, cuando las lineas se repiten:

```php
<?php

$site->set( "descripcion" , $descripcion );
$site->set( "titulo"      , $titulo );
$site->set( "enabled"     , $enabled );
$site->set( "notenabled"  , $notenabled );
```
