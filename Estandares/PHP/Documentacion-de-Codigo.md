## Comentarios

Los comentarios son la forma de aclarar el funcionamiento del código desarrollado así como generar la documentación específica de la API por lo que dichos comentarios son necesarios a lo largo de todo el desarrollo.

Existen dos tipos de comentarios: los comentarios de aclaración del código y los comentarios de documentación.

**1.- Comentarios de aclaración del código**

Este tipo de comentarios se escribe en el medio del código para aclarar su funcionamiento. El comentario en cuestión se debe hacer en la línea inmediatamente superior a la línea de código fuente que se desea aclarar y se deben utilizar los comentarios de doble barra (//) de la siguiente forma:

```php
<?php

// se comprueba si la instancia es null
if( $instancia === null )
{
    $error = "La instancia es NULL.";
}
```

En caso de que la aclaración sea lo suficientemente corta, se puede incluir en la misma línea de código. El punto y coma debe estar separado dos espacios de la primera barra invertida y el texto del comentario debe estar separado un espacio de la segunda barra:

```php
<?php

$i++;  // incrementamos la variable
```
    
En caso de que el comentario aclaratorio sea bastante largo, se deben usar los comentarios multilínea con apertura (/*) y cierre (*/). El uso de asteriscos en cada inicio de línea es obligatorio teniendo en cuenta que muchos editores los añaden automáticamente:

```php
<?php

/*
 * Este es un comentario multilínea para aclarar el 
 * funcionamiento de una o varias líneas de código que 
 * son difíciles de entender por la complejidad del
 * tratamiento de los datos.
 */
foreach( $datos as $indice => $dato )
{
    if(validarDato( $dato ))
    {
	    enviarDato( $indice,$dato );
    }
}
```

En ocasiones tenemos estructuras de control muy grandes (verticalmente) tanto es asi que al leer el final de ellas, perdemos de vista el principio obligándonos a tener que subir y bajar la página para poder leer el código.
Por ello una buena costumbre es la de comentar al final de cada bloque, escribiendo en el comentario la misma declaración del inicio.

```php
<?php

function saludar($val)
{
    if($val == 1)
    {
        echo "Hola!";
    }
    else
    {
        echo "Chau!";
    }//($val == 1)
}//function saludar($val)
```

Usar bloques de código para identificar rápidamente el significado de unas lineas de proceso. En el siguiente ejemplo, se verifica si el usuario tiene los permisos para acceder a la pagina. Nótese como se han encerrado las lineas entre llaves `{}` sin pertenecer estas, a un condicional.

```php
<?php

//Login Check
if(!$login->logged() && !$login->isadmin())
{
    $site->template('login');
    exit(0);
    // ...
}
//End Login Check
```

El nivel de indentación de los comentarios debe ser el mismo que el de la línea que comentan y no debería haber ninguna línea adicional entre la última línea del comentario y la línea de código fuente que comentan. Además, entre los asteriscos y el texto debe haber un espacio.

**2.- Máximo de caracteres por línea**

El número máximo de caracteres por línea debe ser de 150.