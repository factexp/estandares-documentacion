# Configuración de instancias Virgo en ambiente Linux

En este manual se detalla la configuración necesaria para levantar varias instancias Virgo en ambiente Linux.

Se supone que el usuario conectado tiene acceso a ejecutar comandos privilegiados mediante `sudo`, para lo cual deberá proporcionar su contraseña cada vez que el servidor lo requiera.

## Modificación de bin/dmk.sh

El archivo dmk.sh debe ser modificado para que soporte configuraciones manuales para debug, memoria, garbage collector y conexión remota JMX. Debe quedar con el siguiente contenido:

```
#!/bin/bash

SCRIPT="$0"

# SCRIPT may be an arbitrarily deep series of symlinks. Loop until we have the concrete path.
while [ -h "$SCRIPT" ] ; do
  ls=`ls -ld "$SCRIPT"`
  # Drop everything prior to ->
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    SCRIPT="$link"
  else
    SCRIPT=`dirname "$SCRIPT"`/"$link"
  fi
done

# determine kernel home
KERNEL_HOME=`dirname "$SCRIPT"`/..

# make KERNEL_HOME absolute
KERNEL_HOME=`cd "$KERNEL_HOME"; pwd`

# setup classpath and java environment
. "$KERNEL_HOME/bin/setupClasspath.sh"

# execute user setenv script if needed
if [ -r "$KERNEL_HOME/bin/setenv.sh" ]
then
        . $KERNEL_HOME/bin/setenv.sh
fi


# Run java version check with the discovered java jvm.
. "$KERNEL_HOME/bin/checkJava.sh"

shopt -s extglob

# parse the command we executing
COMMAND=$1
shift;

if [ "$COMMAND" = "start" ]
then

        # parse the standard arguments
        CONFIG_DIR=$KERNEL_HOME/configuration
        CLEAN_FLAG=
        NO_START_FLAG=

        SHELL_FLAG=

        DEBUG_FLAG=
# Usar DEBUG_PORT definido en setenv.sh - Cambio realizado por SQA el 20150414
#       DEBUG_PORT=8000
        if [ -z "$DEBUG_PORT" ]
        then
                DEBUG_PORT=8000
        fi
        SUSPEND=n
        if [ -z "$JMX_PORT" ]
        then
                JMX_PORT=9875
        fi

        if [ -z "$KEYSTORE_PASSWORD" ]
        then
                KEYSTORE_PASSWORD=changeit
        fi

        ADDITIONAL_ARGS=

        while (($# > 0))
                do
                case $1 in
                -debug)
                                DEBUG_FLAG=1
                                if [[ "$2" == +([0-9]) ]]
                                then
                                        DEBUG_PORT=$2
                                        shift;
                                fi
                                ;;
                -clean)
                                CLEAN_FLAG=1
                                ;;
                -configDir)
                                CONFIG_DIR=$2
                                shift;
                                ;;
                -jmxport)
                                JMX_PORT=$2
                                shift;
                                ;;
                -keystore)
                                KEYSTORE_PATH=$2
                                shift;
                                ;;
                -keystorePassword)
                                KEYSTORE_PASSWORD=$2
                                shift;
                                ;;
                -noStart)
                                NO_START_FLAG=1
                                ;;

                -suspend)
                                SUSPEND=y
                                ;;
                -shell)
                                SHELL_FLAG=1
                                ;;
                *)
                                ADDITIONAL_ARGS="$ADDITIONAL_ARGS $1"
                                ;;
                esac
                shift
        done

        # start the kernel
        if [[ "$CONFIG_DIR" != /* ]]
        then
            CONFIG_DIR=$KERNEL_HOME/$CONFIG_DIR
        fi

        if [ -z "$KEYSTORE_PATH" ]
        then
            KEYSTORE_PATH=$CONFIG_DIR/keystore
        fi

# Debug con agentlib optimizado para Java 5+ - Cambio realizado por SQA el 20150331
        if [ "$DEBUG_FLAG" ]
        then
#               DEBUG_OPTS=" \
#                       -Xdebug \
#                       -Xrunjdwp:transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=$SUSPEND"
                DEBUG_OPTS=" \
                        -agentlib:jdwp=transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=$SUSPEND"
        fi

        if [ "$CLEAN_FLAG" ]
        then
        rm -rf $KERNEL_HOME/work
        rm -rf $KERNEL_HOME/serviceability

        LAUNCH_OPTS="$LAUNCH_OPTS -clean" #equivalent to setting osgi.clean to "true"
        fi

        if [ "$SHELL_FLAG" ]
        then
            echo "Warning: Kernel shell not supported; -shell option ignored."
                # LAUNCH_OPTS="$LAUNCH_OPTS -Forg.eclipse.virgo.kernel.shell.local=true"
        fi

    ACCESS_PROPERTIES=$CONFIG_DIR/org.eclipse.virgo.kernel.jmxremote.access.properties
    AUTH_LOGIN=$CONFIG_DIR/org.eclipse.virgo.kernel.authentication.config
    AUTH_FILE=$CONFIG_DIR/org.eclipse.virgo.kernel.users.properties
    CONFIG_AREA=$KERNEL_HOME/work
    JAVA_PROFILE=$KERNEL_HOME/configuration/java6-server.profile

    if $cygwin; then
        ACCESS_PROPERTIES=$(cygpath -wp $ACCESS_PROPERTIES)
        AUTH_LOGIN=$(cygpath -wp $AUTH_LOGIN)
        AUTH_FILE=$(cygpath -wp $AUTH_FILE)
        KERNEL_HOME=$(cygpath -wp $KERNEL_HOME)
        CONFIG_DIR=$(cygpath -wp $CONFIG_DIR)
        CONFIG_AREA=$(cygpath -wp $CONFIG_AREA)
        JAVA_PROFILE=$(cygpath -wp $JAVA_PROFILE)
    fi

        # Set the required permissions on the JMX configuration files
        chmod 600 $ACCESS_PROPERTIES

# Se deshabilita jmx.remote.ssl - Cambio realizado por SQA el 20141002
        JMX_OPTS=" \
                $JMX_OPTS \
                -Dcom.sun.management.jmxremote.port=$JMX_PORT \
                -Dcom.sun.management.jmxremote.authenticate=true \
                -Dcom.sun.management.jmxremote.login.config=virgo-kernel \
                -Dcom.sun.management.jmxremote.access.file="$ACCESS_PROPERTIES" \
                -Djavax.net.ssl.keyStore=$KEYSTORE_PATH \
                -Djavax.net.ssl.keyStorePassword=$KEYSTORE_PASSWORD \
                -Dcom.sun.management.jmxremote.ssl=false \
                -Dcom.sun.management.jmxremote.ssl.need.client.auth=false"

        if [ -z "$JAVA_HOME" ]
    then
        JAVA_EXECUTABLE=java
    else
        JAVA_EXECUTABLE=$JAVA_HOME/bin/java
    fi

        # If we get here we have the correct Java version.

        if [ -z "$NO_START_FLAG" ]
        then
                TMP_DIR=$KERNEL_HOME/work/tmp
                # Ensure that the tmp directory exists
                mkdir -p $TMP_DIR

# Se quita asignacion de memoria, se debe especificar en bin/setenv.sh mediante JAVA_OPTS - Cambio realizado por SQA el 0141002
#        JAVA_OPTS="$JAVA_OPTS \
#                    -Xmx512m \
#                    -XX:MaxPermSize=512m"

                cd $KERNEL_HOME; exec $JAVA_EXECUTABLE \
                        $JAVA_OPTS \
                        $DEBUG_OPTS \
                        $JMX_OPTS \
                        -XX:+HeapDumpOnOutOfMemoryError \
                        -XX:ErrorFile=$KERNEL_HOME/serviceability/error.log \
                        -XX:HeapDumpPath=$KERNEL_HOME/serviceability/heap_dump.hprof \
                        -Djava.security.auth.login.config=$AUTH_LOGIN \
                        -Dorg.eclipse.virgo.kernel.authentication.file=$AUTH_FILE \
                        -Djava.io.tmpdir=$TMP_DIR \
                        -Dorg.eclipse.virgo.kernel.home=$KERNEL_HOME \
                        -Dorg.eclipse.virgo.kernel.config=$CONFIG_DIR \
                        -Dosgi.sharedConfiguration.area=$CONFIG_DIR \
                        -Dosgi.java.profile="file:$JAVA_PROFILE" \
            -Declipse.ignoreApp=true \
            -Dosgi.install.area=$KERNEL_HOME \
            -Dosgi.configuration.area=$CONFIG_AREA \
            -Dssh.server.keystore="$CONFIG_DIR/hostkey.ser" \
            -Dosgi.frameworkClassPath=$FWCLASSPATH \
            -Djava.endorsed.dirs="$KERNEL_HOME/lib/endorsed" \
            -classpath $CLASSPATH \
                        org.eclipse.equinox.launcher.Main \
            -noExit \
                        $LAUNCH_OPTS \
                        $ADDITIONAL_ARGS
        fi
elif [ "$COMMAND" = "stop" ]
then

        CONFIG_DIR=$KERNEL_HOME/configuration

        #parse args for the script
        if [ -z "$TRUSTSTORE_PATH" ]
        then
                TRUSTSTORE_PATH=$CONFIG_DIR/keystore
        fi

        if [ -z "$TRUSTSTORE_PASSWORD" ]
        then
                TRUSTSTORE_PASSWORD=changeit
        fi

        if [ -z "$JMX_PORT" ]
        then
                JMX_PORT=9875
        fi

        shopt -s extglob

        while (($# > 0))
                do
                case $1 in
                -truststore)
                                TRUSTSTORE_PATH=$2
                                shift;
                                ;;
                -truststorePassword)
                                TRUSTSTORE_PASSWORD=$2
                                shift;
                                ;;
                -configDir)
                                CONFIG_DIR=$2
                                shift;
                                ;;
                -jmxport)
                                JMX_PORT=$2
                                shift;
                                ;;
                *)
                        OTHER_ARGS+=" $1"
                        ;;
                esac
                shift
        done

        JMX_OPTS=" \
                $JMX_OPTS \
                -Djavax.net.ssl.trustStore=${TRUSTSTORE_PATH} \
                -Djavax.net.ssl.trustStorePassword=${TRUSTSTORE_PASSWORD}"

        OTHER_ARGS+=" -jmxport $JMX_PORT"

    if $cygwin; then
        KERNEL_HOME=$(cygpath -wp $KERNEL_HOME)
        CONFIG_DIR=$(cygpath -wp $CONFIG_DIR)
    fi

        exec $JAVA_EXECUTABLE $JAVA_OPTS $JMX_OPTS \
                -classpath $CLASSPATH \
                -Dorg.eclipse.virgo.kernel.home=$KERNEL_HOME \
                -Dorg.eclipse.virgo.kernel.authentication.file=$CONFIG_DIR/org.eclipse.virgo.kernel.users.properties \
                org.eclipse.virgo.nano.shutdown.ShutdownClient $OTHER_ARGS

else
        echo "Unknown command: ${COMMAND}"
fi

```

## Creación de bin/setenv.sh

Se debe crear el archivo setenv.sh, que contendrá las configuraciones manuales para debug, memoria, garbage collector y conexión remota JMX. Se debe especificar la memoria requerida en las variables Xms y Xmx, la dirección IP del servidor en la variable `java.rmi.server.hostname` y los puertos según la tabla al final de este manual. En general debe quedar con un contenido similar al siguiente:

```
export JAVA_OPTS="$JAVA_OPTS \
-Xms2g -Xmx2g \
-XX:MaxPermSize=256m \
-XX:ReservedCodeCacheSize=128m \
-XX:+UseConcMarkSweepGC \
-XX:CMSInitiatingOccupancyFraction=80 \
-XX:NewRatio=2 -XX:SurvivorRatio=3 \
-verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps \
-Xloggc:$KERNEL_HOME/serviceability/logs/gc.log"

export DEBUG_PORT="8000"

export JMX_PORT="9875"

export JMX_OPTS="-Djava.rmi.server.hostname=192.168.1.1 \
-Dcom.sun.management.jmxremote.rmi.port=9876 \
-Dcom.sun.management.jmxremote.local.only=false \
-Dcom.sun.management.jmxremote.ssl=false"
```

## Modificación de configuration/tomcat-server.xml

Se debe modificar la configuración del Tomcat para que utilice NIO y una mejor distribución de hilos. Se debe especificar los puertos según la tabla al final de este manual. Para una aplicación de web services debería quedar con un contenido similar al siguiente:

```
<?xml version='1.0' encoding='utf-8'?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<Server>
  <!--APR library loader. Documentation at /docs/apr.html -->
  <Listener className="org.apache.catalina.core.AprLifecycleListener" SSLEngine="on" />
  <!--Initialize Jasper prior to webapps are loaded. Documentation at /docs/jasper-howto.html -->
  <Listener className="org.apache.catalina.core.JasperListener" />
  <!-- Prevent memory leaks due to use of particular java/javax APIs-->
  <Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener" />
  <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
  <Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener" />
  <Listener className="org.eclipse.virgo.web.tomcat.support.ServerLifecycleLoggingListener"/>

  <Service name="Catalina">
<!-- Usar HTTP NIO para non-blocking threads - Cambio realizado por SQA el 20141003 -->
<!--    <Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />-->
    <Connector port="8080" protocol="org.apache.coyote.http11.Http11NioProtocol"
               connectionTimeout="20000"
               redirectPort="8443"
               acceptorThreadCount="2"
               maxConnections="10000"
               maxThreads="500"
               minSpareThreads="10" />

<!-- Usar HTTP NIO para non-blocking threads - Cambio realizado por SQA el 20141209 -->
<!--    <Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true"
               maxThreads="150" scheme="https" secure="true"
               clientAuth="false" sslProtocol="TLS"
               keystoreFile="configuration/keystore"
               keystorePass="changeit"/>-->
    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" SSLEnabled="true"
               scheme="https" secure="true"
               clientAuth="false" sslProtocol="TLS"
               keystoreFile="configuration/keystore"
               keystorePass="changeit"
               connectionTimeout="20000"
               acceptorThreadCount="2"
               maxConnections="10000"
               maxThreads="500"
               minSpareThreads="10" />

<!-- Usar AJP NIO para non-blocking threads - Cambio realizado por SQA el 20141002 -->
<!--    <Connector port="8009" protocol="AJP/1.3" redirectPort="8443" />-->
    <Connector port="8009" protocol="org.apache.coyote.ajp.AjpNioProtocol" redirectPort="8443"
               connectionTimeout="600000"
               keepAliveTimeout="600000"
               acceptorThreadCount="2"
               maxConnections="10000"
               maxThreads="500"
               minSpareThreads="10" />

    <Engine name="Catalina" defaultHost="localhost">
      <Realm className="org.apache.catalina.realm.JAASRealm" appName="virgo-kernel"
             userClassNames="org.eclipse.virgo.nano.authentication.User"
             roleClassNames="org.eclipse.virgo.nano.authentication.Role"/>

      <Host name="localhost" appBase=""
            unpackWARs="false" autoDeploy="false"
            deployOnStartup="false" createDirs="false">

        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="serviceability/logs/access"
               prefix="localhost_access_log." suffix=".txt" pattern="common" resolveHosts="false"/>

        <Valve className="org.eclipse.virgo.web.tomcat.support.ApplicationNameTrackingValve"/>
      </Host>
    </Engine>
  </Service>
</Server>
```

## Modificación de configuration/serviceability.xml

Si se desea mantener mayor cantidad de información en los logs rotativos, se puede aumentar el MaxIndex y el MaxFileSize como se indica a continuación:

```
<configuration>

        <jmxConfigurator />

        <contextListener class="ch.qos.logback.classic.jul.LevelChangePropagator"/>

        <appender name="SIFTED_LOG_FILE" class="ch.qos.logback.classic.sift.SiftingAppender">
                <discriminator>
                        <Key>applicationName</Key>
                        <DefaultValue>virgo-server</DefaultValue>
                </discriminator>
                <sift>
                        <appender name="${applicationName}_LOG_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
                                <file>serviceability/logs/${applicationName}/log.log</file>
                                <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
                                        <FileNamePattern>serviceability/logs/${applicationName}/log_%i.log</FileNamePattern>
                                        <MinIndex>1</MinIndex>
                                        <!-- MaxIndex - Cambio realizado por SQA el 20150608 -->
                                        <MaxIndex>10</MaxIndex>
                                </rollingPolicy>
                                <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
                                        <!-- MaxFileSize - Cambio realizado por SQA el 20150608 -->
                                        <MaxFileSize>40MB</MaxFileSize>
                                </triggeringPolicy>
                                <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
                                        <Pattern>[%d{yyyy-MM-dd HH:mm:ss.SSS}] %-5level %-28.28thread %-64.64logger{64} %X{medic.eventCode} %msg %ex%n</Pattern>
                                </encoder>
                        </appender>
                </sift>
        </appender>

        <appender name="LOG_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
                <file>serviceability/logs/log.log</file>
                <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
                        <FileNamePattern>serviceability/logs/log_%i.log</FileNamePattern>
                        <MinIndex>1</MinIndex>
                        <!-- MaxIndex - Cambio realizado por SQA el 20150608 -->
                        <MaxIndex>10</MaxIndex>
                </rollingPolicy>
                <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
                        <!-- MaxFileSize - Cambio realizado por SQA el 20150608 -->
                        <MaxFileSize>40MB</MaxFileSize>
                </triggeringPolicy>
                <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
                        <Pattern>[%d{yyyy-MM-dd HH:mm:ss.SSS}] %-5level %-28.28thread %-64.64logger{64} %X{medic.eventCode} %msg %ex%n</Pattern>
                </encoder>
        </appender>

        <appender name="EVENT_LOG_STDOUT" class="org.eclipse.virgo.medic.log.logback.ReroutingAwareConsoleAppender">
                <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
                        <Pattern>[%d{yyyy-MM-dd HH:mm:ss.SSS}] %-28.28thread &lt;%X{medic.eventCode}&gt; %msg %ex%n</Pattern>
                </encoder>
        </appender>

        <appender name="EVENT_LOG_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
                <file>serviceability/eventlogs/eventlog.log</file>
                <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
                        <FileNamePattern>serviceability/eventlogs/eventlog_%i.log</FileNamePattern>
                        <MinIndex>1</MinIndex>
                        <!-- MaxIndex - Cambio realizado por SQA el 20150608 -->
                        <MaxIndex>10</MaxIndex>
                </rollingPolicy>
                <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
                        <!-- MaxFileSize - Cambio realizado por SQA el 20150608 -->
                        <MaxFileSize>40MB</MaxFileSize>
                </triggeringPolicy>
                <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
                        <Pattern>[%d{yyyy-MM-dd HH:mm:ss.SSS}] %-28.28thread &lt;%X{medic.eventCode}&gt; %msg %ex%n</Pattern>
                </encoder>
        </appender>

        <logger level="INFO" additivity="false" name="org.eclipse.virgo.medic.eventlog.localized">
                <appender-ref ref="EVENT_LOG_STDOUT" />
                <appender-ref ref="EVENT_LOG_FILE" />
        </logger>

        <logger level="INFO" additivity="false" name="org.eclipse.virgo.medic.eventlog.default">
                <appender-ref ref="SIFTED_LOG_FILE" />
                <appender-ref ref="LOG_FILE" />
        </logger>

        <root level="INFO">
                <appender-ref ref="SIFTED_LOG_FILE" />
                <appender-ref ref="LOG_FILE" />
        </root>

</configuration>
```


## Configuración como servicio

Si se desea configurar las instancias Virgo como servicios, se podrá crear los archivos correspondientes en la carpeta /etc/init.d/, con permisos 755, y configurarlos en el chkconfig según se desee. Se debe especificar la ruta del JDK en la variable `JAVA_HOME`, la ruta de la instancia Virgo en la variable `VIRGO_HOME` y su nombre en la variable `VIRGO_NAME`. El contenido de un archivo /etc/init.d/virgo1 debe ser similar al siguiente:

```
#!/bin/bash
# description: Tomcat Start Stop Restart
# processname: virgo1
# chkconfig: 234 20 80
JAVA_HOME=/opt/server-jre1.7.0_60/jdk1.7.0_60/jre
export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH
export PATH
VIRGO_HOME=/opt/web-virgo-tomcat-server-3.6.2.RELEASE-18080/bin
VIRGO_NAME="# 1 - WEB : 18080"

echo $VIRGO_HOME

[ -d "$VIRGO_HOME" ] || { echo "Virgo Tomcat Server requires $VIRGO_HOME."; exit 1; }

case $1 in
start)
    echo "Starting Virgo Tomcat Server $VIRGO_NAME"
    sh $VIRGO_HOME/startup.sh -debug &
;;
stop)
    echo "Stoping Virgo Tomcat Server $VIRGO_NAME"
    sh $VIRGO_HOME/shutdown.sh
;;
restart)
    echo "Restarting Virgo Tomcat Server $VIRGO_NAME"
    $0 stop
    $0 start
;;
cleanstart)
    echo "Starting Virgo Tomcat Server with clean $VIRGO_NAME"
    sh $VIRGO_HOME/startup.sh -clean -debug &
;;
esac
exit 0
```


## Tabla de Puertos

Según la cantidad de instancias que se desee manejar, se deberá configurar los puertos según se indica a continuación:

**bin/setenv.sh**

|              Puerto              | virgo0 | virgo1 | virgo2 | virgo3 | virgo4 | virgo5 |
|:--------------------------------:|:------:|:------:|:------:|:------:|:------:|:------:|
|            DEBUG_PORT            |  8000  |  18000 |  28000 |  38000 |  48000 |  58000 |
|             JMX_PORT             |  9875  |  19875 |  29875 |  39875 |  49875 |  59875 |
|        jmxremote.rmi.port        |  9876  |  19876 |  29876 |  39876 |  49876 |  59876 |

**configuration/tomcat-server.xml**

|              Puerto              | virgo0 | virgo1 | virgo2 | virgo3 | virgo4 | virgo5 |
|:--------------------------------:|:------:|:------:|:------:|:------:|:------:|:------:|
|     Http11NioProtocol - port     |  8080  |  18080 |  28080 |  38080 |  48080 |  58080 |
| Http11NioProtocol - redirectPort |  8443  |  18443 |  28443 |  38443 |  48443 |  58443 |
|  Http11NioProtocol (SSL) - port  |  8443  |  18443 |  28443 |  38443 |  48443 |  58443 |
|       AjpNioProtocol - port      |  8009  |  18009 |  28009 |  38009 |  48009 |  58009 |
|   AjpNioProtocol - redirectPort  |  8443  |  18443 |  28443 |  38443 |  48443 |  58443 |
