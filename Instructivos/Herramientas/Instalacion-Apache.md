### Instalación de APACHE

## Instalación en Ubuntu

Herramienta que utilizaremos en Ubuntu como Servidor web.  

- Instalamos desde consola :  

        sudo apt-get install apache2  

- Habilitamos módulo rewrite :  

        sudo a2enmod rewrite  
        
- Habilitamos módulo ssl : 

        sudo a2enmod ssl  
        
- Habilitamos la configuración ssl por defecto :  

        sudo a2ensite default-ssl  


- Le ponemos nombre al servidor y lo reiniciamos :  

        sudo sh -c 'echo "ServerName localhost" >> /etc/apache2/apache2.conf' && sudo service apache2 restart  
        
- Creamos el certificado de seguridad para el servidor Apache para esto ingresamos a :

        cd /etc/apache2  
        
- Generamos la llave del certificado ejecutando desde consola :  

        sudo openssl genrsa -des3 -out server.key 1024  

    > **Nota:** Nos pedirá una clave a la cual ingresaremos 'serverkey'  
        
- Generamos la clave ejecutando desde consola : 

        sudo openssl req -new -key server.key -out server.csr  

    Nos pedirán los siguientes datos :  
    
        Country Name (2 letter code) [AU] : EC
        State or Province Name (full name) [Some-State]: GUAYAS
        Locality Name (eg, city) []: GUAYAQUIL
        Organization Name (eg, company) [Internet Widgits Pty Ltd]: TELCONET
        Organizational Unit Name (eg, section) []: SISTEMAS
        Common Name (e.g. server FQDN or YOUR name) []: dev-telcos-developer.telconet.ec
        Email Address []: < login programador >@telconet.ec

        Please enter the following 'extra' attributes
        to be sent with your certificate request

        A challenge password []:serverkey      
        An optional company name []:tn
    
- Generamos el certificado ejecutando desde consola :  

        sudo openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

    > **Nota:** Nos pedirá una clave a la cual ingresaremos 'serverkey'  
        
- Movemos la llave y el certificado generados a :

        sudo cp server.crt /etc/ssl/certs/  
        sudo cp server.key /etc/ssl/private/  
        
- Reiniciamos el servidor Apache :  

        sudo service apache2 restart

