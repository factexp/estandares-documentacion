### Instalación de Oracle Instant Client

## Instalación en Ubuntu


Librerías necesarias para conexión a Bases de Datos Oracle.

- Instalamos unas librerias necesarias desde consola:

        sudo apt-get install build-essential  
        sudo apt-get install php5-dev php-pear libaio1  

- Solicitar a SQA instaladores :

    >**Version 11.2.X.X** 
    
    > - instantclient-basic  
    > - instantclient-sdk  
    > - instantclient-sqlplus  
    > - instantclient-jdbc

- Descomprimimos los archivos zip desde consola :  

        sudo unzip /<path-of-instant-client-basic>/instantclient-basic-linux.x64-11.2.0.4.0.zip  
        sudo unzip /<path-of-instant-client-sdk>/instantclient-sdk-linux.x64-11.2.0.4.0.zip  
        sudo unzip /<path-of-instant-client-sqlplus>/instantclient-sqlplus-linux.x64-11.2.0.4.0.zip  
        sudo unzip /<path-of-instant-client-jdbc>/instantclient-jdbc-linux.x64-11.2.0.4.0.zip  
    
- Movemos la carpeta donde descomprimimos los instant client a la de librerias desde consola :  

        sudo mv /<path-to-instantclient>/instantclient_11_2 /usr/local/lib/  

- Ingresamos a la carpeta de librerías y creamos el enlace simbólico desde consola :  

        cd /usr/local/lib/instantclient_11_2  
        sudo ln -s libclntsh.so.11.1 libclntsh.so  

- Declaramos las variables de entorno necesarias editando el archivo /etc/environment desde consola :  

        sudo nano /etc/environment  
        

    y al final del archivo escribimos :  
    
        LD_LIBRARY_PATH="/usr/local/lib/instantclient_11_2"  
        TNS_ADMIN="/usr/local/lib/instantclient_11_2"  
        ORACLE_BASE="/usr/local/lib/instantclient_11_2"  
        ORACLE_HOME=$ORACLE_BASE  

- Instalamos desde consola la extensión de php para conexiones a Base de Datos Oracle :  

        sudo pecl install oci8-1.4.10  
        
    > **Nota:** nos solicitará la ruta de los instant client instalados para lo cual ingresaremos:  
    
    > instantclient,/usr/local/lib/instantclient_11_2  
    
    
- Una vez instalada necesitamos configurarla en el PHP, para esto creamos el siguiente archivo desde consola :  

        sudo nano /etc/php5/mods-available/oci8.ini  
        
    Y escribimos lo siguiente : 
    
        extension=oci8.so
    
    creamos tambien los enlaces simbolicos necesarios :  
    
        cd /etc/php5/apache2/conf.d/  
        sudo ln -s ../../mods-available/oci8.ini 30-oci8.ini  
        cd /etc/php5/cli/conf.d/  
        sudo ln -s ../../mods-available/oci8.ini 30-oci8.ini
     
- Reiniciamos el servidor Apache :  

         sudo service apache2 restart  



## Instalación en Windows

Librerías necesarias para conexión a Bases de Datos Oracle.

- Descargar los zip siguientes desde : [Pagina Oficial de Descarga Oracle][1]

    >**Version 11.2.X.X** 
    
    > - instantclient-basic  
    > - instantclient-sdk  
    > - instantclient-sqlplus  
    > - instantclient-jdbc

- Descomprimir todos los zip y mover la carpeta que generó la descompresión a C:\ :

    > move C:\path\to\instant_cliente_11_2 C:\

- Agregar en la variable de entorno **PATH** la ruta de nuestro instant_client:

    > C:\instantclient_11_2  
    
- Reiniciar Windows


[1]: http://www.oracle.com/technetwork/database/features/instant-client/index-097480.html
