### Instalación de GIT

Herramienta que utilizaremos para el control de versiones.

## Instalacion en Ubuntu

- Instalamos desde consola:

        sudo apt-get install git

## Instalacion en Windows

- Descargar e Instalar : [Página Oficial de Descarga Git][1]

[1]: http://git-scm.com/download/win
