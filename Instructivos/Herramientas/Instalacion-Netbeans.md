### Instalación Netbeans

## Instalación en Ubuntu

- Solicitar a SQA el instalador.  

- Dar permisos de ejecucion al archivo :  

        chmod 777 netbeans-7.4-linux.sh  

- Ejecutar sh desde consola :  

        ./netbeans-7.4-linux.sh    


## Instalación en Windows

- Descargar desde : [Página Oficial Descarga Netbeans][1] 

-  Instalamos Netbeans


[1]: https://netbeans.org/downloads/
