### Instalación de PHP

Lenguaje en el cual esta desarrollado el Telcos +.

- Instalar desde consola :  

        sudo apt-get install php5
    
- Instalar extensión Curl desde consola :  

        sudo apt-get install curl libcurl3 libcurl3-dev php5-curl php5-gd

- Instalar extensión XDebug desde consola :  

        sudo apt-get install php5-xdebug

- Configurar extensión XDebug editando el archivo `/etc/php5/mods-available/xdebug.ini` desde consola :  

        sudo gedit /etc/php5/mods-available/xdebug.ini

    y agregar lo siguiente a la primera línea `zend_extension=xdebug.so` :  

        xdebug.max_nesting_level=200
        xdebug.remote_enable=1

- Reiniciamos el servidor Apache :  

        sudo service apache2 restart

