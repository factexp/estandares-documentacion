**Manual de Configuración de IDE Spring-Tool-Suite en Linux**

---

**Introducción**

Este manual indica la configuración del IDE STS en ambiente Linux.

---


- **Crear directorio "IDE"**
Crear directorio IDE y entrar en el.

   mkdir ~/IDE; cd ~/IDE


```bash
user@user:~$ mkdir ~/IDE; cd ~/IDE
user@user:~/IDE$
```

- **Descargar STS**

32 bits:

    wget http://dist.springsource.com/release/STS/3.5.1/dist/e4.3/spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk.tar.gz

64 bits:

    wget http://dist.springsource.com/release/STS/3.5.1/dist/e4.3/spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk-x86_64.tar.gz

```bash
user@user:~/IDE$ wget http://dist.springsource.com/release/STS/3.5.1/dist/e4.3/spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk-x86_64.tar.gz
—2016-08-23 19:01:44—  http://dist.springsource.com/release/STS/3.5.1/dist/e4.3/spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk-x86_64.tar.gz
Resolving dist.springsource.com (dist.springsource.com)... 104.16.65.137, 104.16.68.137, 104.16.69.137, ...
Connecting to dist.springsource.com (dist.springsource.com)|104.16.65.137|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 374764665 (357M) [application/octet-stream]
Saving to: ‘spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk-x86_64.tar.gz’

spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk 100%[=========================================================================================================>] 357,40M   681KB/s   in 9m 6s  

2016-08-23 19:10:51 (670 KB/s) - ‘spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk-x86_64.tar.gz’ saved [374764665/374764665]

user@user:~/IDE$
```


- **Descomprimir el archivo descargado**

    cd ~/IDE; tar -zxvf spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk-x86_64.tar.gz;

```bash
user@user:~/IDE$cd ~/IDE; tar -zxvf spring-tool-suite-3.5.1.RELEASE-e4.3.2-linux-gtk-x86_64.tar.gz;
sts-bundle/sts-3.5.1.RELEASE/plugins/org.springsource.ide.eclipse.commons.gettingstarted_3.5.1.201404300702-RELEASE/resources/welcome/common/css/
sts-bundle/sts-3.5.1.RELEASE/plugins/org.springsource.ide.eclipse.commons.gettingstarted_3.5.1.201404300702-RELEASE/resources/welcome/common/css/application.css
.....
sts-bundle/legal/Spring_Tool_Suite_Bundle_Oct_01_2013.txt
sts-bundle/legal/vfabric-tc-server-developer-open-source-licenses-2.9.5.SR1.txt
user@user:~/IDE$
```


- **Referenciar librería lombok.jar**
Solicitar librería lombok.jar a QA y agregarlo al directorio del STS.

    cp {path}/lombok.jar ~/IDE/sts-bundle/sts-3.5.1.RELEASE

```console
user@user:~/IDE$ cp ~/Downloads/lombok.jar ~/IDE/sts-bundle/sts-3.5.1.RELEASE
user@user:~/IDE$
```

Entrar la directorio del STS.

```bash
user@user:~/IDE$ cd ~/IDE/sts-bundle/sts-3.5.1.RELEASE/; ll | grep ".jar"
-rw-rw-r—   1 user user 1355970 ago 23 22:17 lombok.jar
user@user:~/IDE/sts-bundle/sts-3.5.1.RELEASE$
```

Editar archivo STS.ini

    sudo nano STS.ini

Agregar las siguientes lineas:

    -javaagent:/{path}/IDE/sts-bundle/sts-3.5.1.RELEASE/lombok.jar
    -Xbootclasspath/a:/{path}/IDE/sts-bundle/sts-3.5.1.RELEASE/lombok.jar


- **Configuración de IDE STS**

Abrir STS, elegir _workspace_ de un directorio que se encuentre en el _truecrypt_.
Ir a la barra de menú del STS -> Windows -> Preferences, buscar el item **Maven** y abrir el menú.

Item **Installations**
Quitar el check de **Embedded**.
Click en el boton **add**.
Buscar el directorio Maven, en /{path}/Frameworks/apache-maven-3.3.3, confirmar con el boton **OK**.
Dejar en check la carpeta Maven seleccionada.

Item **User Interface**
Dejar en check solo **Open XML page in the POM editor by default**.

Item **User Settings**
Verificar que User Settings tenga el archivo settings.xml de la carpeta .m2, _/{path}/.m2/settings.xml/_.

Click en Update Settings
Click en el boton **Apply**
Verificar el archivo settings.xml con el link (open file), cerrar ventana Preferences y validar información del archivo.
