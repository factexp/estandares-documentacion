**Manual de Configuración de Maven 3.3.9 en Linux**

---

**Introducción**

Este manual detalla la configuración del framework maven en ambiente linux.

**Maven**

Maven es una herramienta de gestión de proyectos de software y comprensión. Basado en el concepto de un modelo de objeto de proyecto (POM), Maven puede gestionar acumulación, generación de informes y documentación de un proyecto a partir de una pieza central de la información. [Maven](https://maven.apache.org/what-is-maven.html)


> **Requisito:** Se Debe tener instalado oracle-java7

- **Crear directorio _Frameworks_**

Crear directorio con nombre **Frameworks** y entrar al directorio.

    mkdir ~/Frameworks/; cd ~/Frameworks/;

```bash
user@user:~$ mkdir ~/Frameworks/; cd ~/Frameworks/;
user@user:~/Frameworks$
```

- **Descargar Maven**

Descargar Maven 3.3.9:

    wget http://www-us.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.zip

```bash
user@user:~/Frameworks$ wget http://www-us.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.zip
—2016-08-23 18:09:03—  http://www-us.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.zip
Resolving www-us.apache.org (www-us.apache.org)... 140.211.11.105
Connecting to www-us.apache.org (www-us.apache.org)|140.211.11.105|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 8617253 (8,2M) [application/zip]
Saving to: ‘apache-maven-3.3.9-bin.zip’

apache-maven-3.3.9-bin.zip                       100%[=========================================================================================================>]   8,22M   803KB/s   in 11s    

2016-08-23 18:09:44 (786 KB/s) - ‘apache-maven-3.3.9-bin.zip’ saved [8617253/8617253]

user@user:~/Frameworks$
```

- **Descomprimir archivo descargado**

    unzip apache-maven-3.3.9-bin.zip

```bash
user@user:~/Frameworks$ unzip apache-maven-3.3.9-bin.zip
Archive:  apache-maven-3.3.9-bin.zip
   creating: apache-maven-3.3.9/
   creating: apache-maven-3.3.9/boot/
  inflating: apache-maven-3.3.9/boot/plexus-classworlds-2.5.2.jar  
   creating: apache-maven-3.3.9/lib/
  inflating: apache-maven-3.3.9/lib/maven-embedder-3.3.9.jar  
  inflating: apache-maven-3.3.9/lib/maven-settings-3.3.9.jar  
  inflating: apache-maven-3.3.9/lib/plexus-utils-3.0.22.jar  
  inflating: apache-maven-3.3.9/lib/maven-core-3.3.9.jar
...
user@user:~/Frameworks$
```

- **Listar archivos del directorio**

    ls -l

```bash
user@user:~/Frameworks$ ls -l
total 8428
drwxrwxr-x  3 user user    4096 ago 23 18:12 ./
drwxr-xr-x 35 user user    4096 ago 23 18:07 ../
drwxr-xr-x  6 user user    4096 nov 10  2015 apache-maven-3.3.9/
-rw-rw-r—  1 user user 8617253 nov 18  2015 apache-maven-3.3.9-bin.zip
user@user:~/Frameworks$
```

- **Entrar y mostrar path del directorio apache-maven-3.3.9**
El path será usado en la configuración del archivo _settings.xml_ y para setear la variable de entorno _M2_HOME_.

    cd apache-maven-3.3.9; pwd;

```bash
user@user:~/Frameworks$ cd apache-maven-3.3.9; pwd;
/home/user/Frameworks/apache-maven-3.3.9
user@user:~/Frameworks/apache-maven-3.3.9$
```


- **Configurar variables de entorno _JAVA_HOME_ y _$M2_HOME_ en archivo _.profile_**

    sudo nano ~/.profile

user@user:~$ sudo nano ~/.profile

Al final del archivo agregar las siguientes lineas, reemplazar _{path}_ por el directorio respectivo.


    export JAVA_HOME=/{path}/java-7-oracle
    export PATH=$JAVA_HOME/bin:$PATH
    
    export M2_HOME=/{path}/apache-maven-3.3.3
    export PATH=$M2_HOME/bin:$PATH

Guardar cambios al archivo _.profile_

Mostrar valor de variables de entorno.

    echo $JAVA_HOME; echo $M2;

```bash
user@user:~$ echo $JAVA_HOME; echo $M2;


user@user:~$
```

- **Ejecutar el archivo .profile**
Ejecutar el archivo _.profile_ para aplicar los cambios.

    source ~/.profile

```bash
user@user:~$ source ~/.profile
user@user:~$
```

Cerrar y volver abrir la consola.

Verificar que las variables de entorno se encuentren seteadas.

    echo $JAVA_HOME; echo $M2_HOME

```bash
user@user:~$ echo $JAVA_HOME; echo $M2_HOME
/usr/lib/jvm/java-7-oracle
/home/{user}/Frameworks/apache-maven-3.3.9
```

- **Crear carpeta .m2**
Es donde residen todas las dependencias maven.

    mkdir ~/.m2; sudo chmod 777 ~/.m2; chown {user}:{user} ~/.m2;

```bash
user@user:~$ mkdir ~/.m2; sudo chmod 777 ~/.m2; chown {user}:{user} ~/.m2;
user@user:~$ 
```

Solicitar a QA la carpeta _repository_ y copiarla dentro del directorio creado _.m2_.


- **Configurar archivo settings.xml**

Solicitar archivo settings.xml a QA y copiarlo en la carpeta .m2.

Modificar archivo settings.xml

    sudo gedit ~/.m2/settings.xml

Modificar el valor de los tags:

**localRepository** => **/{path}/.m2/repository**, path de la carpeta repository.
**username, password**, usuario y password de autenticación al ldap.
**virgoRoot** => path donde se encuentra el virgo.
