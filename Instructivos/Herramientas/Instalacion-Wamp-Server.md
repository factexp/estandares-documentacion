### Instalación de Wamp Server

## Instalación en Windows

Herramienta que utilizaremos en Windows como servidor web.

- Descargar e instalar

     - Wamp necesita para su instalación unas librerías adicionales de Microsoft Visual C++ que debemos descardar de:
        >[VC10 SP1 vcredist_x86.exe 32 bits][4]  
        >[VC10 SP1 vcredist_x64.exe 64 bits][5]

     - Descargamos el instalador de Wamp Server desde: [Página Oficial de Descarga Wamp Server][1]
     - Instalamos Wamp Server

- Habilitamos y configuramos Módulos y extensiones para Apache y PHP

     - Habilitamos SSL en el servidor apache del wamp dándole click a:
        > Apache -> Apache Modules-> ssl_module
        
     - Habilitamos REWRITE en el servidor apache del wamp dándole click a:
        > Apache -> Apache Modules-> rewrite_module
        
     - Habilitamos openSSL en el php del wamp dándole click a:
        > PHP->PHP extensions-> php_openssl
        
     - Habilitamos curl en el php del wamp dándole click a:
        > PHP->PHP extensions-> php_curl
        
     - Habilitamos oci8 en el php del wamp dándole click a:
        > PHP->PHP extensions-> php_oci8
        
     - Abrimos una consola de Windows y nos dirigimos a:
        > cd C:\wamp\bin\apache\apache2.2.22\bin
        
     - Creamos el certificado:
        >openssl genrsa -des3 -out server.key 1024  
        
     - Nos pedirá una clave e ingresaremos:
         > serverkey
         
     - Quitamos la clave de la RSA private key( guardando un respaldo)
         > copy server.key server.key.org<br>
         > openssl rsa -in server.key.org -out server.key
         
     - Creamos una firma para el certificado:
         >openssl req -new -x509 -nodes -sha1 -days 365 -key server.key -out server.crt -config                          C:\wamp\bin\apache\apache2.2.22\conf\openssl.cnf
         
      Y nos pedirán los sgts datos:
      
         >Country Name (2 letter code) [AU]: EC  
         
         >State or Province Name (full name) [Some-State]: GUAYAS  
         
         >Locality Name (eg, city) []: GUAYAQUIL  
         
         >Organization Name (eg, company) [Internet Widgits Pty Ltd]: TELCONET  
         
         >Organizational Unit Name (eg, section) []: SISTEMAS  
         
         >Common Name (e.g. server FQDN or YOUR name) [ ]: dev-telcos-developer.telconet.ec  
         
         >Email Address [ ]: [ nuestra dirección de correo ]

     - Creamos dos carpetas en C:\wamp\bin\apache\apache2.2.22\conf :
         > ssl.key  
         > ssl.crt
         
     - Copiamos los archivos generados a las respectivas carpetas:
         >server.key -> ssl.key
         >server.crt -> ssl.crt
         
     - Abrimos el archive C:\wamp\bin\apache\apache2.2.22\conf\ httpd.conf para descomentar la linea:
         >Include conf/extra/httpd-ssl.conf
         
     - Abrimos el achivo C:\wamp\bin\apache\Apache2.2.22\conf\extra\httpd_ssl.conf y buscamos:
         > < virtualHost _default_:443 > 
         
     Y a partir de eso reemplazamos las siguientes lineas:
         > cambia la linea “DocumentRoot ...” con DocumentRoot “C:/wamp/www/”  
         
         > cambia la linea “ServerName...” con ServerName localhost:443  
         
         > cambia la linea “ErrorLog....” con Errorlog “C:/wamp/bin/apache/Apache2.2.22/logs/sslerror.log”  
         
         > cambia la linea “TransferLog ....” con TransferLog “C:/wamp/bin/apache/Apache2.2.22 /logs/sslaccess.log”  
         
         > cambia la linea “SSLCertificateFile ....” con SSLCertificateFile  “C:/wamp/bin/apache/Apache2.2.22/conf/ssl.crt/server.crt”  
         
         > cambia la linea “SSLCertificateKeyFile ....” con SSLCertificateKeyFile   “C:/wamp/bin/apache/Apache2.2.22/conf/ssl.key/server.key”  
         
         >cambia la linea “CustomLog...” con CustomLog “C:/wamp/bin/apache/Apache2.2.22/logs/ssl_request.log”
    
     Agrega las siguientes lines dentro del tag < Directory ... >...< /Directory>:
    
        >Options Indexes FollowSymLinks MultiViews  
        >AllowOverride All  
        >Order allow,deny  
        >allow from all
        
     - Reiniciamos el wamp server y  accedemos a https://localhost



[1]: http://www.wampserver.com/en/
