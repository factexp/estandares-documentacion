### Instalación de Wkhtmltopdf

## Instalación en Ubuntu

Herramienta que utilizaremos para tranformar paginas **html** en **pdf**.  

- Instalamos desde consola:  

    > **Nota:** reemplazar < bits > con i386 ó amd64 dependiendo del caso de la pc de trabajo  

        cd /home/< usuario SO >/Downloads
        
        sudo wget http://wkhtmltopdf.googlecode.com/files/wkhtmltopdf-0.9.9-static-amd64.tar.bz2  

        tar xvjf wkhtmltopdf-0.9.9-static-amd64.tar.bz2  

        sudo chown root:root wkhtmltopdf-amd64

        sudo cp wkhtmltopdf-amd64 /usr/local/bin/wkhtmltopdf  


## Instalación en Windows

- Descargar desde : [Página Oficial de descarga wkhtmltopdf][1]

- Instalar
- Mover la carpeta donde se instaló el wkhtmltopdf a la raiz del C:\  

    > move C:\Program Files(x86)\wkhtmltopdf C:\wkhtmltopdf  


[1]: http://wkhtmltopdf.org/downloads.html
