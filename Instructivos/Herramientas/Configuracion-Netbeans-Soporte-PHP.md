# NetBeans con soporte a PHP

## Configuración IDE soporte a PHP

1. Instalar NetBeans 7.4
2. Ejecutar NetBeans 7.4
3. Aceptar la licencia
4. En la pestaña "Start Page", desactivar casilla "Show On Startup" y cerrar la pestaña
5. No aceptar actualizaciones
6. Configurar soporte PHP en Tools -> Options -> PHP
    1. Activar: General -> Activate
    2. Configurar comando PHP: General -> Command Line -> PHP 5 Interpreter
    3. Configurar Debug en Debugging:
        1. Debugger Port: 9000
        2. Session ID: netbeans-xdebug
        3. Maximum Data Length: 2048
        4. Stop at First Line (desactivar)
        5. Watches and Balloon Evaluation (activar)
        6. Maximum Depth of Structures: 3
        7. Maximum Number of Children: 30
        8. Show Requested URLs (desactivar)
        9. Show Debugger Console (desactivar)
7. Configurar formato general en Tools -> Options -> Editor -> Formatting -> Language: All languages
    1. Expand Tabs to Spaces (activado)
    2. Number of Spaces per Indent: 4
    3. Tab Size: 4
    4. Right margin: 150
    5. Line Wrap: Off
8. Configurar formato PHP en Tools -> Options -> Editor -> Formatting -> Language: PHP
    1. Category: Tabs and Indents
        1. Use All Languages Settings (activar)
        2. Initial Indentation: 0
        3. Continuation Indentation: 4
        4. Array Declaration Indentation: 4
    2. Category: Alignment -> New Lines
        1. "else", "elseif" (activar)
        2. "while" (activar)
        3. "catch" (activar)
    3. Category: Braces -> Todas las opciones: New Line
    4. Category: Spaces
        1. Before Keywords -> Todas las opciones (activar)
        2. Before Parentheses -> Todas las opciones (desactivar)
        3. Around Operators -> Todas las opciones (activar) excepto "Unary Operator" y "Object Operator"
        4. Before Left Braces -> Todas las opciones (activar)
        5. Within Parentheses -> Todas las opciones (desactivar)
        6. Other -> Todas las opciones (activar) excepto "Before Comma" y "Before Semicolon"
9. Configurar formato JavaScript en Tools -> Options -> Editor -> Formatting -> Language: JavaScript
    1. Category: Tabs and Indents
        1. Use All Languages Settings (activar)
        2. Initial Indentation: 0
        3. Continuation Indentation: 4
10. Configurar Web Browser en Tools -> Options -> General: Firefox
11. Desactivar estadísticas en Tools -> Options -> General -> Help us improve the NetBeans IDE by providing anonymous usage data (desactivar)



