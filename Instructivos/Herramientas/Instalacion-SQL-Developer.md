### Instalación de SQL Developer

## Instalación en Ubuntu

Herramienta que utilizaremos para la gestión de la Base de Datos.

- Solicitar a SQA instalador:

- Instalamos el arhivo deb  desde consola :  

        sudo dpkg –i /< path-to-sqldeveloper >/sqldeveloper_4.0.0.13.30-2_all.deb

- Ejecutamos en consola lo siguiente:

        sudo sqldeveloper
    
- Nos solicitará el path del jdk 1.7, el cual si no lo tenemos debemos:
    - Remover el openjdk desde consola:   
    
            sudo apt-get purge openjdk*  

    - Instalar el siguiente paquete desde consola  :
    
            sudo apt-get install software-properties-common  
        
    - Agregar el siguiente PPA desde consola :  
    
            sudo add-apt-repository ppa:webupd8team/java  
        
    - Actualizamos nuestros repositorios desde consola :  
    
            sudo apt-get update  

    - Instaladmos Oracle JDK 7 desde consola :    
    
            sudo apt-get install oracle-java7-installer
    
- Ingresamos el full path del JDK:  

        /usr/lib/jvm/java-7-oracle/


## Instalación en Windows

Herramienta que utilizaremos para la gestión de la Base de Datos.

- Descargar SQL Developer: [Página Oficial de Descarga Oracle][1]

- Descomprimimos el Archivo ya que se descarga en .zip

- Ejecutamos el archivo sqldeveloper.exe

- Nos solicitará el path del jdk 1.6, el cual si no lo tenemos debemos:
    - Descargarlo e instalarlo: [Página Oficial de Descarga Oracle][2]

- Una vez instalado volvemos a ejecutar el sqldeveloper.exe e ingresamos el full path del jdk 1.6 ya instalado



[1]: http://www.oracle.com/technetwork/developer-tools/sql-developer/downloads/index.html
[2]: http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-javase6-419409.html#jdk-6u45-oth-JPR
