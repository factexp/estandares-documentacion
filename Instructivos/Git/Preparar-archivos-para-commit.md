Con el siguiente procedimiento se preparará los archivos a ser incluidos en un commit:

1. Si no está ubicado en la rama local de desarrollo, hacer checkout:

        git checkout [nombre rama local]

    Ejemplo:

        git checkout bugContrasena

2. Revisar lista de archivos modificados o agregados:

        git status

    Ejemplo:

        kjimenez@Dell:/var/www/telcos$ git status
        # On branch planificacion-mega
        # Changes not staged for commit:
        #   (use "git add <file>..." to update what will be committed)
        #   (use "git checkout -- <file>..." to discard changes in working directory)
        #
        #   modified:   src/telconet/comercialBundle/Controller/InfoPuntoController.php
        #   modified:   src/telconet/planificacionBundle/Resources/config/route/factibilidad.yml
        #   modified:   src/telconet/schemaBundle/Repository/InfoDetalleSolicitudRepository.php
        #   modified:   src/telconet/tecnicoBundle/Controller/InfoServicioController.php
        #
        # Untracked files:
        #   (use "git add <file>..." to include in what will be committed)
        #
        #   src/telconet/planificacionBundle/Controller/ConsultarFactibilidadInstalacionController.php
        #   src/telconet/planificacionBundle/Resources/config/routing/consultarfactibilidadinstalacion.yml
        #   src/telconet/planificacionBundle/Resources/public/js/ConsultarFactibilidad/
        #   src/telconet/planificacionBundle/Resources/views/ConsultarFactibilidad/
        #   web/public/images/add_factibilidad.png
        #   web/public/images/edit_factibilidad.png
        no changes added to commit (use "git add" and/or "git commit -a")


3. Comparar los cambios realizados con la versión anterior, para garantizar que solo se ha modificado lo pertinente al cambio requerido:

        git diff

    > **Nota:** Si se desea comparar ignorando espacios, se puede ejecutar:

        git diff -w


4. Preparar los archivos modificados para realizar el commit:

        git add [archivos o directorios separados por espacios]

    Ejemplo:

        git add src/telconet/comercialBundle/Controller/InfoPuntoController.php src/telconet/schemaBundle/Repository/InfoDetalleSolicitudRepository.php
        git add src/telconet/planificacionBundle/ src/telconet/tecnicoBundle/

    > **Nota:** Si todos los archivos mostrados por `git status` son parte del cambio a subir, se puede ejecutar:

        git add .

5. Realizar el commit con el comentario según el formato establecido
