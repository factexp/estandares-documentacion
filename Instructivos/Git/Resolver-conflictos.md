
## ¿Por qué aparecen conflictos en git al intentar hacer pull, merge ó push?
Porque varios programadores modifican las mismas líneas del mismo fichero, el segundo que intente subir el fichero, no lo tendrá actualizado y aparecerá el conflicto porque Git no puede realizar el merge de forma automática.

> **Formas de evitarlos:** Haciendo pull a menudo así mantendremos nuestro código actualizado.

## ¿Cómo lo solucionamos?

El conflicto no es un problema que nos tengamos que quitar de encima, no lo solucionaremos haciendo que el mensaje desaparezca de la consola. Sólo nos avisa de que no puede realizar el merge de forma automática y necesita que confirmemos que parte del fichero es la correcta.  

Ejemplo:

    [root@dev-telcos telcos]# git merge arsuarez
    Auto-merging src/telconet/soporteBundle/Controller/EnvioPlantillaController.php
    CONFLICT (content): Merge conflict in src/telconet/soporteBundle/Controller/EnvioPlantillaController.php
    Auto-merging src/telconet/schemaBundle/Repository/InfoServicioRepository.php
    Auto-merging src/telconet/schemaBundle/Repository/InfoElementoRepository.php
    Automatic merge failed; fix conflicts and then commit the result.

Para resolver los conflictos seguiremos los siguientes pasos:


1. Abrimos el fichero que contiene el conflicto con nuestro editor de texto.

    Git añade lo siguiente:

    ```php
    <?php

    <<<<<<< HEAD
            // código Programador A
            .
            .
    =======
            //  código Programador B
            .
            .
    >>>>>>> [Nombre programador B]
    ```

    Ejemplo:

    ```php
    <?php

    public function notificaOperacion($view,$to)
    {
        $asunto = 'Comunicado';  
        foreach($to as $t)
        {
            $message = \Swift_Message::newInstance()
            ->setSubject($asunto)
    <<<<<<< HEAD
            ->setFrom('notificaciones_telcos@telconet.ec')
            ->setTo($to)
    =======
            ->setFrom('telcos@telconet.ec')
            ->setTo($t)
    >>>>>>> arsuarez
            ->setBody($view,'text/html');
            $this->get('mailer')->send($message);
        }
    }
    ```


2. Unimos nuestros cambios con los existentes, respetando asi el trabajo de los demás. Adicional también debemos borrar las líneas que Git añade en el fichero.

    ```php
    <?php

    // código Programador A
    .
    .
    //  código Programador B
    .
    .
    ```

    Ejemplo:

    ```php
    <?php

    public function notificaOperacion($view,$to)
    {
        $asunto = 'Comunicado';  
        foreach($to as $t)
        {
            $message = \Swift_Message::newInstance()
            ->setSubject($asunto)
            ->setFrom('notificaciones_telcos@telconet.ec')
            ->setTo($t)
            ->setBody($view,'text/html');
            $this->get('mailer')->send($message);
        }
    }
    ```

    > **Nota:** Existirán casos donde no será necesario unir cambios sino dejar el código correcto como en este ejemplo.


3. Preparamos nuestros cambios:

        git add [archivos]

    Ejemplo:

        git add src/telconet/soporteBundle/Controller/EnvioPlantillaController.php


4. Realizamos el commit para confirmar nuestros cambios y dejar por acentado la resolucion del conflicto.

        git commit -m "[comentario sobre conflicto resuelto]"

    Ejemplo:

        git commit -m "Resolucion de conflicto en src/telconet/soporteBundle/Controller/EnvioPlantillaController.php"


5. Finalmente volvemos a ejecutar el comando deseado.
