Con el siguiente procedimiento se creará una nueva rama local a partir de master actualizado y se hará checkout a la rama creada inmediatamente:

1. Actualizar repositorio local en base al remoto:

        git fetch origin

2. Crear rama local en base a master actualizado y hacer checkout de inmediato:

        git checkout -b [nombre rama local] origin/master

    Ejemplo:

        git checkout -b bugContrasena origin/master
