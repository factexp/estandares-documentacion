## Instructivo para rastrear cambios en un archivo

En ciertas ocasiones se requiere buscar el commit en el cual se introdujo un código específico en un archivo versionado, de modo que se pueda determinar cuándo y quién realizó el cambio.

Para rastrear cambios en un archivo se debe seguir los siguientes pasos:


1. Actualizar repositorio local en base al remoto:

        git fetch origin

2. Listar los commits de la historia del archivo a rastrear en la rama donde existen los cambios, que por lo general será la rama master:

        git log [nombre_rama] -- [ruta/del/archivo]

    Ejemplo:

        git log origin/master -- src/telconet/someBundle/Service/SomeService.php

3. Visualizar los cambios realizados en cada commit de la historia del archivo a rastrear:

        git log -p [nombre_rama] -- [ruta/del/archivo]

    Ejemplo:

        git log -p origin/master -- src/telconet/someBundle/Service/SomeService.php

    > **Nota:** Si se desea visualizar ignorando cambios en espacios, se puede ejecutar:

        git log -w -p [nombre_rama] -- [ruta/del/archivo]

4. Buscar el código específico usando la tecla slash `/`, aprovechando que la interface de `git diff` es similar a la de `vi`:

        Para buscar: `<ESC>` + `/` + `codigoBuscado` + `<ENTER>`
        Para avanzar a la siguiente ocurrencia: `n`
        Para retroceder a la ocurrencia anterior: `SHIFT` + `n`

5. Una vez encontrado el código buscado, subir con la tecla `<RE-PAG>` hasta encontrar el commit (con su autor y fecha).

6. Una vez encontrado el commit, buscar el MR Master en SQA Board, filtrando por autor, fecha y/o título del MR (que debería coincidir con el mensaje del commit, en su defecto se puede buscar por parte del título).

