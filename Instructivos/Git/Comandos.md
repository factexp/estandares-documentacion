En este articulo aprenderemos el funcionamiento de algunos comandos básicos de GIT que utilizaremos para nuestro versionamiento :  

 a) Buscando Ayuda:

1. **git help [comando]** ó **git [comando] --help**

    Muestra la ayuda para ese comando

b) Creación de un repositorio:

2. **git init**

    Crea un repositorio en el directorio actual

3. **git clone [url repositorio remoto]**

    Clona un repositorio remoto dentro de un directorio

c) Operaciones sobre Archivos:

4. **git add [directorio ó archivo]**

    Adiciona un archivo o un directorio de manera recursiva

5. **git rm [directorio ó archivo]**

    Remueve un archivo o directorio del árbol de trabajo

    -f : Fuerza la eliminación de un archivo del repositorio

6. **git mv [origen] [destino]**

    Mueve el archivo o directorio a una nueva ruta

    -f : Sobre-escribe los archivos existentes en la ruta destino

6. **git stash save "[comentario]"**

    Guarda en una pila los cambios que han sido commiteados.

7. **git checkout -- archivo**

    Recupera un archivo desde la rama o revisión actual

    -f : Sobre-escribe los cambios locales no guardados

d) Trabajando sobre el código:

8. **git status**

    Imprime un reporte del estado actual del árbol de trabajo local

9. **git diff [ruta]**

    Muestra la diferencia entre los cambios en el árbol de trabajo local

10. **git diff HEAD [ruta]**

    Muestra las diferencias entre los cambios registrados y los no registrados

11. **git add [directorio ó archivo]**

    Selecciona el archivo ó directorio para que sea incluido en el próximo commit

12. **git reset HEAD [directorio ó archivo]**

    Marca el archivo para que no sea incluido en el próximo commit

13. **git commit**

    Realiza el commit de los archivos que han sido registrados (con git add)

    -a : Automáticamente registra todos los archivos modificados  
    -m [comentario] : Le pone un comentario al commit
    

14. **git reset --soft HEAD**

    Deshace commit & conserva los cambios en el árbol de trabajo local

15. **git reset --hard HEAD**

    Restablece el árbol de trabajo local a la versión del ultimo commit

16. **git clean**

    Elimina archivos desconocidos del árbol de trabajo local

e) Examinando el histórico:

17. **git log**

    Muestra el historial de los commit.

17. **git log [directorio ó ruta]**

    Muestra el historial de los commit de la dirección ó ruta especifica.

19. **git blame [archivo]**

    Muestra el archivo relacionado con las modificaciones realizadas

f) Repositorios remotos:

20. **git fetch [nombre del remoto]**

    Trae los cambios desde un repositorio remoto

21. **git pull [nombre del remoto] [rama remota]:[rama local]**

    Descarga y guarda los cambios realizados desde un repositorio remoto

22. **git push [nombre del remoto] [rama local]:[rama remota]**

    Guarda los cambios en un repositorio remoto

23. **git remote**

    Lista los nombre de los repositorios remotos  
    
    -v Lista con mas detalle los repositorios remotos

24. **git remote add [nombre del remoto] [url del repositorio remoto]**

    Añade un repositorio remoto a la lista de repositorios registrados

23. **git remote show [nombre del remoto]**

    Muestra los datos del repositorio remoto.

g) Ramas:

25. **git checkout [nombre rama]**

    Cambia el árbol de trabajo local a la rama indicada

    -b rama : Crea la rama antes de cambiar el árbol de trabajo local a dicha rama

26. **git branch**

    Lista las ramas locales

27. **git branch [nuevo nombre rama]**

    Crea rama local
    
    -d Elimina la rama local  
    -D Fuerza la eliminación de la rama local  

27. **git branch [nuevo nombre rama] [nombre rama remota]**

    Crea rama local con seguimiento a una rama remota
   
28. **git merge [nombre rama]**

    Une los cambios de otra rama.

j) Banderas de Estado de los Archivos:

**M (modified) :** El archivo ha sido modificado

**C (copy-edit) :** El archivo ha sido copiado y modificado

**R (rename-edit) :** El archivo ha sido renombrado y modificado

**A (added) :** El archivo ha sido añadido

**D (deleted) :** El archivo ha sido eliminado

**U (unmerged) :** El archivo presenta conflictos después de ser guardado en el servidor (merge)
