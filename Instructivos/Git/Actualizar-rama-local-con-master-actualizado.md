Con el siguiente procedimiento se actualizará la rama local con master actualizado:

1. Si no está ubicado en la rama local de desarrollo, hacer checkout:

        git checkout [nombre rama local]

    Ejemplo:

        git checkout bugContrasena

2. Actualizar repositorio local en base al remoto:

        git fetch origin

3. Aplicar sobre la rama local los cambios de master actualizado:

        git merge origin/master
