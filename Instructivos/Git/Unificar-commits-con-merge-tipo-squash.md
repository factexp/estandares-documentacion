Con el siguiente procedimiento se realizará un merge tipo squash para unificar varios commits de un desarrollo en uno solo, en base a master actualizado.

1. [Actualizar rama local original con master actualizado][1]

    Ejemplo:

        git checkout bugContrasenaOrig
        git fetch origin
        git merge origin/master

2. [Resolver conflictos][2]

3. [Crear rama local nueva a partir de master actualizado][3]

    Ejemplo:

        git checkout -b bugContrasenaNuevo origin/master

4. Hacer merge tipo squash aplicando la rama local original sobre la rama local nueva:

        git merge --squash [nombre rama local original]

    Ejemplo:

        git merge --squash bugContrasenaOrig

    > **Nota:** Luego de un merge tipo squash los archivos modificados ya quedan preparados automáticamente como si se hubiera ejecutado `git add .`

6. Comparar los cambios realizados con la versión HEAD, para garantizar que solo se ha modificado lo pertinente al cambio requerido:

        git diff HEAD

    > **Nota:** Si se desea comparar ignorando espacios, se puede ejecutar:

        git diff -w HEAD

7. Realizar el commit con el comentario según el formato establecido


[1]: Actualizar-rama-local-con-master-actualizado.md
[2]: Resolver-conflictos.md
[3]: Crear-rama-local-a-partir-de-master-actualizado-y-hacer-checkout.md