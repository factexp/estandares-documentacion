
Con el siguiente procedimiento se creará una nueva rama local a partir de una remota en sus repositorios locales:

1. Crear rama local:

        git branch [nombre rama local] [nombre rama remota]

       Ej:

        git branch bugContrasena origin/kjimenez-bug

    > **Nota:** comando para listar las ramas remotas existentes:
    >
    >       git branch -r