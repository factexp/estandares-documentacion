Muchas veces se requiere realizar otras acciones con git el cuál nos limita si esque tenemos cambios no guardados u desechos. A continuación veremos el procedimiento a seguir.

### Cambiarse de Rama

Listamos nuestras ramas :

    git branch


Ejemplo:  

    kjimenez@Dell:/var/www/telcos$ git branch
    master
    * planificacion-mega

Ejecutamos comando para cambiarnos a rama master:  

    /*Cambiarse de rama*/

    kjimenez@Dell:/var/www/telcos$ git checkout master  
    error: Your local changes to the following files would be overwritten by checkout:  
    	src/telconet/comercialBundle/Controller/InfoPuntoController.php  
    	src/telconet/tecnicoBundle/Controller/InfoServicioController.php  
    Please, commit your changes or stash them before you can switch branches.  
    Aborting  

Para poder resalizar otra accion con git sin necesidad de perder nuestros cambios seguiremos los siguientes pasos:  

1. Guardamos nuestros cambios en una pila con un comentario :  

        git stash save "[comentario]"
    
    Ejemplo:
    
        git stash save "planificacion-mega"  
        
    >  **Nota**: comentario debe describir la rama actual.
    
2. Realizamos la acción que requerímos. Ejemplo:

        /*Cambiarse de rama*/
        kjimenez@Dell:/var/www/telcos$ git checkout master
        Switched to branch 'master'
        Your branch is ahead of 'origin/master' by 28 commits.
          (use "git push" to publish your local commits)
        kjimenez@Dell:/var/www/telcos$
        
        /*Realizar un pull*/
        kjimenez@Dell:/var/www/telcos$ git pull origin
        kjimenez@dev-telcos.telconet.ec's password: 
        remote: Counting objects: 426, done.
        remote: Compressing objects: 100% (293/293), done.
        remote: Total 299 (delta 182), reused 0 (delta 0)
        Receiving objects: 100% (299/299), 159.57 KiB, done.
        Resolving deltas: 100% (182/182), completed with 49 local objects.
        From ssh://dev-telcos.telconet.ec/home/telcos/
           6765834..3ab6428  master     -> origin/master
        Already up-to-date.
        
3. Volvemos a nuestra rama de trabajo:

        git checkout [rama local donde estaban los cambios]
        
    Ejemplo: 
    
        git checkout planificacion-mega
        
4. Listamos los cambios guardados en la pila :

        git stash list  
        
    Ejemplo :

        kjimenez@Dell:/var/www/telcos$ git stash list
        stash@{0}: On planificacion-mega: planificacion-mega
        
5. Restablecemos nuestros cambios : 

        git stash apply [numero de stash]
        
    Ejemplo :
    
        kjimenez@Dell:/var/www/telcos$ git stash apply stash@{0}
        # On branch planificacion-mega
        # Changes not staged for commit:
        #   (use "git add <file>..." to update what will be committed)
        #   (use "git checkout -- <file>..." to discard changes in working directory)
        #
        #	modified:   src/telconet/comercialBundle/Controller/InfoPuntoController.php
        #	modified:   src/telconet/planificacionBundle/Resources/config/route/factibilidad.yml
        #	modified:   src/telconet/schemaBundle/Repository/InfoDetalleSolicitudRepository.php
        #	modified:   src/telconet/tecnicoBundle/Controller/InfoServicioController.php
        #
        # Untracked files:
        #   (use "git add <file>..." to include in what will be committed)
        #
        #	src/telconet/planificacionBundle/Controller/ConsultarFactibilidadInstalacionController.php
        #	src/telconet/planificacionBundle/Resources/config/routing/consultarfactibilidadinstalacion.yml
        #	src/telconet/planificacionBundle/Resources/public/js/ConsultarFactibilidad/
        #	src/telconet/planificacionBundle/Resources/views/ConsultarFactibilidad/
        #	web/public/images/add_factibilidad.png
        #	web/public/images/edit_factibilidad.png
        no changes added to commit (use "git add" and/or "git commit -a")

6. Eliminamos nuestros cambios de la pila :
    
        git stash drop [numero de stash]

    Ejemplo:
    
        kjimenez@Dell:/var/www/telcos$ git stash drop stash@{0}
        Dropped stash@{1} (71c82ec1b3ac86120794fc37b784a0f43d6cd3b3)
