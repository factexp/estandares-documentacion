### Configuración de Netbeans-XDebug

- Debe tener descargada la ultima versión de la imagen telcos del repositorio privado de Telconet.

- Abrir el aplicativo de Netbeans con el proyecto telcos.

- Ir a la opción: Tools -> Options.

- Ir a la opción: PHP -> Debugging y configuramos lo siguiente:  
Debugger Port con el puerto 9002  
Quitamos todos los check de la opción Debugging  
Guardamos los cambios con el botón Apply y OK  

- Damos clic derecho sobre el proyecto telcos y escogemos la opción properties

- En Categories escogemos Run Configuration y configuramos lo siguiente:  
Run As: Local Web Site (running on local web server)  
Project URL: http://dev-telcos-developer.telconet.ec/  

- Presionamos el botón "Advanced..." y configuramos lo siguiente:  
Debug URL: Default  
Port: 9002  
Path Mapping: Server Path -> 'Ruta del contenedor', Project Path -> 'Ruta donde esta su proyecto telcos'  
Ejemplo: Path Mapping: Server Path -> /home, Project Path -> /var/www  

- Finalmente ejecutamos el Debug del proyecto dando clic derecho y escogiendo la opción Debug sobre su proyecto telcos.

