## Instructivo de Desarrollo, Commit y Merge Request

A continuación se explica los pasos a seguir para el desarrollo de un cambio, la subida del commit a GitLab, y la creación de los Merge Requests para su Revisión de Calidad y posterior subida a Producción.

---

**1. Tipo de Cambio:** Determinar el Tipo de Cambio, que puede ser:

- **Bug:** Comportamiento no adecuado de una opción del sistema.
- **Nuevo:** Se refiere a todas las nuevas opciones o mejoras en el sistema.

---

**2. Requerimiento:** Obtener requerimiento del Usuario, que debe corresponder al Tipo de Cambio:

- **Bug:** Puede originarse mediante una de las siguientes vías:

    - **Tarea Telcos:**
        - Reportado por el Usuario en Telcos (telcos.telconet.ec)
        - La URL de la Tarea Telcos deberá incluirse en el MR Test

    - **Ticket Soporte:**
        - Reportado por el Usuario en el aplicativo de Soporte (soporte.telconet.ec)
        - La URL del Ticket Soporte deberá incluirse en el MR Test

    - **Ticket Zoho:**

        - Creado por Jefatura o Ing. de Pruebas en el aplicativo Zoho, con el siguiente formato:

            - ***Título del Problema:*** [Empresas Afectadas] : [Módulo] : Bug : [Titulo del Problema Reportado/Encontrado]

            Donde [Empresas Afectadas] son los prefijos de todas las empresas afectadas, separados por guión:

                Ejemplo: `MD : Tecnico : Bug : Activacion cliente Huawei`

            - ***Descripción del Problema***: [Descripción del Bug]

            - ***Asignar a***: [Nombre del Programador que resolverá el Bug]

        - La URL del Ticket Zoho deberá incluirse en el MR Test

    > **Nota:** Debe identificarse el commit y el MR a master que originó el Bug, [rastreando cambios en los archivos afectados][13]. Esta información deberá incluirse en el MR Test.

- **Nuevo:** Puede originarse mediante una de las siguientes vías:

    - **Solicitud:**

        - Registrado por el Usuario en el aplicativo de Solicitudes (apps.telconet.ec/solicitudes) o en el aplicativo de Requerimientos (apps.telconet.ec/requerimientos)
        - La URL de la Solicitud deberá incluirse en el MR Test

    - **Tarea Telcos:**

        - Reportado por el Usuario en Telcos (telcos.telconet.ec), con seguimiento de parte de Jefatura/Líder asignando el desarrollo al Programador
        - La URL de la Tarea Telcos deberá incluirse en el MR Test

    - **Correo Usuario:**

        - Enviado por el Usuario a Jefatura/Líder
        - El archivo PDF o EML del correo deberá incluirse en el MR Test

    - **Correo Jefatura:**

        - Enviado por Jefatura/Líder al Programador
        - El archivo PDF o EML del correo deberá incluirse en el MR Test

> **Nota:** Puede usarse los formatos de [Solicitud de Cambios][1] o [Especificación de Requisitos][2]

---

**3. Tarea:** Planificar el desarrollo de la solución en Zoho o Kanboard. La URL de la tarea planificada deberá incluirse en el MR Test.

---

**4. Análisis/Diseño:** Realizar análisis de requerimiento, matriz de afectación, diseño de solución, plan de pruebas, según sea necesario. La documentación relacionada deberá incluirse en el MR Test.

---

**5. Desarrollo:**

- [Crear rama local a partir de master actualizado y hacer checkout][3]
- Realizar el desarrollo requerido, aplicando las [Políticas de Calidad][4]

---

**6. Pruebas:** Realizar pruebas, documentarlas usando el formato de [Pruebas de Aplicación][5]

---

**7. Instructivo:** Elaborar instructivo versionado en caso de ser un desarrollo de Web Service. No Aplica para el resto de desarrollos.

---

**8. Commit:**

- [Preparar archivos para commit][6]

- Realizar el commit con el comentario según el siguiente formato:

        git commit -m "[Empresas Afectadas] : [Módulo] : [Tipo de Cambio] : [Breve descripcion del cambio realizado]"

    Donde [Empresas Afectadas] son los prefijos de todas las empresas afectadas, separados por guión:

    Ejemplos:

        git commit -m "TN : Planificacion : Bug : Coreccion en el flujo de planificar, detener y rechazar"

        git commit -m "TN-MD : Comercial : Nuevo : Generar PDF asociado a imagenes en ingreso de nuevo contrato"

- [Actualizar rama local con master actualizado][7]

- [Resolver conflictos][8] en caso de ser necesario, y luego [unificar commits con merge tipo squash][9]

---

**9. Push:** Subir la rama local a la rama remota en GitLab:

    git push -f origin [nombre rama local]:[nombre rama remota]

Donde [nombre rama remota] corresponde al login del programador y el Tipo de Cambio, y debe numerarse en caso de haber desarrollos concurrentes:

- **Nuevo:** Login del programador, más la numeración. Ejemplos:

        kjimenez
        kjimenez-2
        .
        .
        kjimenez-n

- **Bug:** Login del programador con la palabra "bug", más la numeración. Ejemplos:

        kjimenez-bug
        kjimenez-bug-2
        .
        .
        kjimenez-bug-n

Ejemplos:

    git push -f origin planificacion-mega:kjimenez
    git push -f origin bugContrasena:kjimenez-bug-2

> **Nota :** Se utiliza el -f para forzar la subida de los cambios a la rama remota correspondiente

---

**10. MR Test:** Crear el Merge Request a test en GitLab, que debe tener el siguiente formato:

- **Rama Origen:** Elegir la rama del desarrolo que se solicita hacer Revisión de Calidad:

    Ejemplo :

        kjimenez-2

- **Rama Destino:** Elegir la rama "test"

- **Asignar a:** Coordinador de Calidad de Software y Producción

- **Título:** Debe coincidir con el comentario del commit (ver punto 8):

        [Empresas Afectadas] : [Módulo] : [Tipo de Cambio] : [Breve descripcion del cambio realizado]

    Ejemplos:

        TN : Planificacion : Bug : Coreccion en el flujo de planificar, detener y rechazar

        TN-MD : Comercial : Nuevo : Generar PDF asociado a imagenes en ingreso de nuevo contrato

- **Descripción:** Debe especificar lo siguiente (se puede indicar "No Aplica" solamente en los items que así lo permitan):

    > - **Requerimiento:** [ Enlace al Requerimiento o adjuntar imagen del Requerimiento (ver punto 2). ]
    > - **Tarea:** [ Enlace a la Tarea en Zoho o Kanboard (ver punto 3). ]
    > - **Análisis/Diseño:** [ Adjuntar documentación de Análisis/Diseño (ver punto 4).  
    >   Se puede indicar "No Aplica" solamente si es Bug. ]
    > - **Doc. Pruebas:** [ Adjuntar documentación de pruebas del programador (ver punto 6). ]
    > - **Instructivo:** [ Indicar que se versiona el Instructivo en caso de ser Web Service (ver punto 7).  
    >   Se debe indicar "No Aplica" en todos los demás desarrollos. ]
    > - **Descripción:** [ Descripcion completa de las funcionalidades creadas o modificadas en el cambio a subir. ]
    > - **Sistemas Afectados:** [ Aplicativos, App Móviles, Scripts, etc., afectados por el cambio a subir. ]
    > - **Ruta Opción:** [ Pasos para llegar a las opciones creadas, modificadas o afectadas.  
    >   Se debe indicar "No Aplica" si no hay opciones afectadas. ]
    > - **Credenciales:** [ Nombres de las credenciales para la habilitación de la opción.  
    >   Se debe indicar "No Aplica" si no se requiere credenciales. ]
    > - **Crontab:** [ Líneas de crontab para la ejecucion del script, comando, etc.  
    >   Se debe indicar "No Aplica" si no se requiere crontab. ]
    > - **DB:** [ Orden de ejecución de los scripts SQL versionados.  
    >   Si es Bug se debe indicar el script de regularización de data o en su defecto se debe indicar "No requiere regularización de data".  
    >   Se debe indicar "No Aplica" si no se requiere ejecutar scripts SQL. ]
    > - **Configuración:**  [ Configuraciones adicionales necesarias para que funcione el cambio, como definición de parámetros, instalación de librerías, enlace entre sistemas, etc.  
    >   Se debe indicar "No Aplica" si no se requiere configuración adicional. ]
    > - **Commit/MR Bug:** [ Hash del commit que originó el Bug, slash `/`, y referencia con signo de admiración `!` al MR a master que originó el Bug.  
    >   Para obtener esta información se debe [rastrear cambios en los archivos afectados][13].  
    >   Se debe indicar "No Aplica" solamente si es Nuevo. ]
    > - **MR Relacionados:** [ Enlace a los MR a test que deben revisarse junto a este MR.  
    >   Se debe indicar "No Aplica" si no se requiere revisar otros MR a test. ]

    **NOTA:** No se debe adjuntar archivos en comentarios. Todos los archivos relacionados al MR deberán adjuntarse en el cuerpo de la descripción en la sección que corresponda.

    Ejemplo:

    > - **Requerimiento:** [Solicitud #9999][14]
    > - **Tarea:** https://projects.zoho.com/portal/telcosis#taskdetail/245613000000640037/245613000000670043/245613000001568011
    > - **Análisis/Diseño:** No Aplica
    > - **Doc. Pruebas:**  [Documento de Pruebas][15]
    > - **Instructivo:** No Aplica
    > - **Descripción:** Se actualiza indice de olt en la base de datos
    > - **Sistemas Afectados:** Telcos, soportelc
    > - **Ruta Opción:** Tecnico -> Elemento -> Olt
    > - **Credenciales:** Actualizar indice cliente
    > - **Crontab:** No Aplica
    > - **DB:** Ejecutar scripts versionados en el siguiente orden:  
    >   1.- DML_9999.sql  
    >   2.- DDL_9999.sql  
    >   3.- DCL_9999.sql  
    > - **Configuracion:** No Aplica
    > - **MR Bug:** No Aplica
    > - **MR Relacionados:** !699

---

**11. MR Master:** Crear el Merge Request a master en GitLab, que debe tener el siguiente formato:

- **Rama Origen:** Elegir la rama del desarrolo que se solicita subir a Producción:

    Ejemplo :

        kjimenez-2

- **Rama Destino:** Elegir la rama "master"

- **Asignar a:** Coordinador de Calidad de Software y Producción

- **Título:** Debe ser el mismo título del Merge Request a Test:

        [Empresas Afectadas] : [Módulo] : [Tipo de Cambio] : [Breve descripcion del cambio realizado]

    Ejemplos:

        TN : Planificacion : Bug : Coreccion en el flujo de planificar, detener y rechazar

        TN-MD : Comercial : Nuevo : Generar PDF asociado a imagenes en ingreso de nuevo contrato

- **Descripción:** Debe especificar lo siguiente:

    > - **MR Test:** [ Enlace del MR a Test. ]
    > - **MR Relacionados:** [ Enlace a los MR a master que deben subirse junto a este MR.  
    >   Se debe indicar "No Aplica" si no se requiere subir otros MR a master. ]

    Ejemplo:

        **MR Test:** !700
        **MR Relacionados:** No Aplica

> **Nota:** No es necesario volver a indicar las instrucciones ni los adjuntos, ya que para la Subida a Producción se usará lo especificado en el MR a Test.

---

**12. Aceptación Usuario:** En caso de ser necesario, realizar pruebas con el usuario, obtener su aceptación. Se deberá enviar esta información al Coordinador de Requerimientos de Software y Pruebas para que la incluya en el MR. Puede usarse el formato de [Pruebas de Aceptación][10].

---

**13. Revisiones de Calidad y Pruebas Funcionales:**

1. El estado de las revisiones de calidad se indicará mediante comentarios en el Merge Request, con texto `:recycle: [Estado QA o Pruebas]` (:recycle: [Estado QA o Pruebas]).
2. El Ing. de Calidad de Software y Producción realizará la **Revisión de Calidad** en base al [Control de Calidad][11].
3. Al iniciar la Revisión de Calidad, se agregará un comentario al MR indicando el estado **Checking** (:recycle: Checking).
4. Al finalizar la Revisión de Calidad, se agregará un comentario al MR indicando el resultado del Control de Calidad junto al detalle de la revisión realizada.
    - Al inicio del comentario se indicará el número de revisión y el hash del commit que se ha revisado (Por ejemplo: **REVISION DE CALIDAD # 1 (abcd1234)**)
    - Para el resultado del Control de Calidad se utilizará el siguiente código de colores:
        - :white_check_mark: Item cumplido.
        - :warning: Item para el que se pide confirmación, la cual debe ser dada por parte del programador mediante comentario al MR.
        - :o2: Item para los cuales se encontró inconsistencias (total de observaciones entre paréntesis).
    - Para el detalle de la revisión realizada se utilizará el siguiente formato solamente a partir de la segunda revisión:
        - Se ~~tachará~~ y marcará con :heavy_check_mark: las observaciones de la revisión anterior que sí hayan sido resueltas.
        - Se marcará en rojo con :o2: las observaciones de la revisión anterior que no hayan sido resueltas.
        - Se marcará en amarillo con :warning: las nuevas observaciones que surjan a raíz de los cambios realizados luego de la revisión anterior.
5. En caso de requerirse correcciones, se agregará un comentario al MR indicando el estado **Fixing** (:recycle: Fixing).
    - Se notificará al programador para que haga las correcciones solicitadas.
    - En caso de requerirse correcciones en los archivos adjuntos al Merge Request, el programador deberá editar los comentarios existentes, no agregar nuevos comentarios.
    - El programador deberá indicar que concluyó con la corrección de las observaciones detalladas en la revisión, mediante comentario en el Merge Request con texto `:recycle: Continue` (:recycle: Continue).
    - A continuación el deberá realizarse una nueva Revisión de Calidad (punto 2).
6. En caso de no requerirse correcciones, se agregará un comentario al MR indicando el estado **Pre-Testing** (:recycle: Pre-Testing).
    - En este estado ya se puede realizar las Pruebas Funcionales.
7. El Coordinador de Requerimientos de Software y Pruebas designará un Ing. de Requerimientos y Pruebas para que realice las **Pruebas Funcionales** en ambiente de pruebas, en base a lo solicitado en el Requerimiento y verificando la afectación a otras opciones del sistema. Se agregará un comentario al MR indicando la persona designada con texto `:mag: usuario` (:mag: usuario).
8. Al iniciar las Pruebas Funcionales, se agregará un comentario al MR, indicando el estado **Testing** (:recycle: Testing).
    - Se deberá verificar que el hash actual de la rama sea igual al hash de la última Revisión de Calidad, caso contrario el MR deberá pasar a estado **Fixing**.
9. Al finalizar las Pruebas Funcionales, se agregará un comentario al MR indicando el resultado.
    - Al inicio del comentario se indicará el hash del commit que se ha probado (Por ejemplo: **PRUEBAS FUNCIONALES (abcd1234)**)
10. En caso de requerirse correcciones, se agregará un comentario al MR, indicando el estado **Fixing** (:recycle: Fixing). Se deberá seguir las indicaciones del punto 5.
11. En caso de no requerirse correcciones, se agregará un comentario al MR, indicando el estado **Done** (:recycle: Done).
12. El Ing. de Calidad de Software y Producción agregará un comentario al MR, indicando el estado **Complete** (:recycle: Complete).
    - Se deberá verificar que el hash actual de la rama sea igual al de las últimas Pruebas Funcionales y al de la última Revisión de Calidad, caso contrario el MR deberá pasar a estado **Fixing**.
13. El StandBy de Calidad de Software y Producción planificará la subida a producción, y agregará un comentario al MR indicando el estado **Ready** (:recycle: Ready).
    - La subida a producción se regirá según las [Políticas de Subidas a Producción][12].
14. Una vez realizada la subida a producción, se aceptará los MR Test y Master, los cuales pasarán de estado **Opened** a **Merged**.


---

[1]: ../../Formatos/Solicitud-de-Cambios.doc
[2]: ../../Formatos/Especificacion-de-Requisitos.doc
[3]: ../Git/Crear-rama-local-a-partir-de-master-actualizado-y-hacer-checkout.md
[4]: ../../Politicas/Politicas-de-Calidad.md
[5]: ../../Formatos/Pruebas-de-Aplicacion.doc
[6]: ../Git/Preparar-archivos-para-commit.md
[7]: ../Git/Actualizar-rama-local-con-master-actualizado.md
[8]: ../Git/Resolver-conflictos.md
[9]: ../Git/Unificar-commits-con-merge-tipo-squash.md
[10]: ../../Formatos/Pruebas-de-Aceptacion.doc
[11]: ../../Documentacion/QA/Control-de-Calidad.md
[12]: ../../Politicas/Politicas-de-Subidas-a-Produccion.md
[13]: ../Git/Rastrear-cambios-archivo.md
[14]: ../../Formatos/Solicitud-de-Cambios.doc
[15]: ../../Formatos/Pruebas-de-Aplicacion.doc
