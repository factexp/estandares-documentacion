
Este manual le servirá a los programadores para instalar los programas basicos y necesarios para su ambiente de desarrollo.

Lo primero que haremos será deshabilitar el requerimiento de solicitud de password al usuario del SO cada vez que se utilice el comando **sudo**:

- Editamos el archivo sudoers:

        sudo visudo

- Buscamos la siguiente Lineas:

        %sudo ALL=(ALL) ALL

- La actualizamos con la siguiente:

        %sudo ALL=(ALL) NOPASSWD: ALL

- Guardamos y salimos

Listo ahora si reailzaremos la instalacion de los programas:

1. Habilitamos todos los repositorios disponibles en la opcion de **Software & Updates** de Ubuntu.  

2. Actualizamos los repositorios:

        sudo apt-get update && sudo apt-get upgrade  
    
3. Instalamos la interfaz de usuario Gnome 3:

        sudo apt-get install gnome-shell ubuntu-gnome-desktop  
        
    
    > **Reiniciamos el SO** y antes de iniciar sesión escogemos **Gnome**
        
4. Instalamos el editor de texto Kate y su consola:  

        sudo apt-get install kate konsole

5. Instalamos los JDK de Oracle Java:
        
        sudo apt-get purge openjdk*
        sudo add-apt-repository ppa:webupd8team/java
        sudo apt-get update
        sudo apt-get install oracle-java6-installer
        sudo apt-get install oracle-java7-installer

6. Instalamos el Netbeans( Solicitar a SQA ):

        chmod 777 netbeans-7.4-linux.sh 
        ./netbeans-7.4-linux.sh

7. Instalamos el SQL Developer( Solicitar a SQA ):

        sudo dpkg -i sqldeveloper_4.0.0.13.30-2_all.deb
        
8. Instalamos el Google Chrome( Solicitar a SQA ):

        sudo dpkg -i google-chrome-stable_current_amd64.deb

9. Instalamos Forticlient( Solicitar a SQA ) y las librerias necesarias:

        sudo apt-get install libgtk2.0-0:i386 libsm6:i386 libstdc++6:i386
    
    Accedemos a la carpeta del Forticlient y lo ejecutamos:
        
        cd forticlientsslvpn
        sudo chmod 755 forticlientsslvpn
        sudo ./forticlientsslvpn
        
    Los datos de la VPN son:  
    
    >**server:** 200.93.192.205
    >**puerto:** 10443

10. Instalamos skype:
        
        sudo apt-get install skype
        
11. Instalamos Pidgin y plugins necesarios:

        sudo apt-get install pidgin pidgin-skype pidgin-encryption
        
12. Instalamos el plugin de Flash para los navegadores:
        
        sudo apt-get install flashplugin-installer
        
13. Instalamos extensiones de compresion:

        sudo apt-get install unace unrar zip unzip p7zip-full p7zip-rar sharutils rar uudeview mpack arj cabextract file-roller

14. Instalamos Dropbox:

        sudo apt-get install nautilus-dropbox
        
15. Instalamos Gnome panel para poder crear launchers:

        sudo apt-get install --no-install-recommends gnome-panel

    >**Nota:** para crear un launcher ejecutamos el siguiente comando:
    
        sudo gnome-desktop-item-edit /usr/share/applications/ --create-new
        
16. Instalamos programa para editar configuraciones de ventanas , iconos , etc:

        sudo apt-get install gnome-tweak-tool

17. Habilitamos el HDMI:

        xrandr --output VGA1 --right-of HDMI1 --auto

18. Instalamos TrueCrypt( Solicitar a SQA ):
        
        sudo chmod 777 truecrypt-7.1a-setup-x64 
        ./truecrypt-7.1a-setup-x64 
        
19. Instalamos **KeePassX** desde el **Ubuntu Software Center**
