1.- Solicitar por medio de un correo electrónico al área de SQA (<sistemas-qa@telconet.ec>) crear el repositorio en Gitlab detallando la siguiente información:
            
- Titulo del proyecto
- Descripcion del proyecto.

2.- Una vez creado el repositorio por parte de SQA, existen 2 opciones a seguir dependiendo de lo requerido (Se debe ejecutar sólo una de las dos):

- Clonar el proyecto en un directorio local.

    >cd /directorio local/
    git clone [url del repositorio en gitlab]

    Ej:

        cd /home/kjimenez/NetBeansProjects/
        git clone git@gitlab.telconet.ec:telcos/genera-excel-facturas-md.git
        
- Agregar repositorio al proyecto.

    >cd /proyecto local/
    git init
    git remote add origin [url del repositorio en gitlab]

    Ej:

        cd /home/kjimenez/NetBeansProjects/genera-excel-facturas-md.git
        git init
        git remote add origin git@gitlab.telconet.ec:telcos/genera-excel-facturas-md.git
    
3.- Crear rama local para realizar o montar cambios.

>cd /nombre-repo/
  git checkout -b [nombre rama local]

Ej:
    
    cd genera-excel-facturas-md/
    git checkout -b nuevasColumnas

4.- Realizamos nuestros cambios, y para publicarlo seguimos el procedimiento ya establecido.